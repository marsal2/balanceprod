
function validlogin(){
	var flag=0;
	var email = $('#user').val();
	if((document.getElementById('user').value).trim()==''){	
		//alert('Please enter marks');
		$('#usermsg').html('Please enter the email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
	} 
	else if(IsEmail(email)==false){
        $('#usermsg').html('Please enter the correct email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
        }
	else{
		$('#usermsg').html('');
		$('#usermsg').removeClass('alert alert-warning')
	}	
	if((document.getElementById('pass').value).trim()==''){
		//alert('Please enter question');
		$('#passmsg').addClass('alert alert-warning')
		document.getElementById('pass').focus();
		$('#passmsg').html('Please enter the password');
		flag=1;
	}
	 else{
		$('#passmsg').html('');
		$('#passmsg').removeClass('alert alert-warning')
	}
	
if(flag==1)	
	return false;
	else return true;

}


function validRegister(){
	
	var flag=0;
	if((document.getElementById('firstname').value).trim()==''){	
		//alert('Please enter marks');
		$('#firstnamemsg').html('Please enter the firstname');
		$('#firstnamemsg').addClass('alert alert-warning')
		document.getElementById('firstname').focus();		
		flag=1;
	}	
	else{
		$('#firstnamemsg').html('');
		$('#firstnamemsg').removeClass('alert alert-warning')
	}

	if((document.getElementById('lastname').value).trim()==''){	
		//alert('Please enter marks');
		$('#lastnamemsg').html('Please enter the lastname');
		$('#lastnamemsg').addClass('alert alert-warning')
		document.getElementById('lastname').focus();		
		flag=1;
	}	
	else{
		$('#lastnamemsg').html('');
		$('#lastnamemsg').removeClass('alert alert-warning')
	}
	
	var email = $('#email').val().trim();	
	if(email==''){	
		//alert('Please enter marks');
		$('#emailmsg').html('Please enter the email id');
		$('#emailmsg').addClass('alert alert-warning')
		$('#email').focus();		
		flag=1;
	}
	else if(IsEmail(email)==false){
        $('#emailmsg').html('Please enter the correct email id');
		$('#emailmsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
    }	
	else{
		$('#emailmsg').html('');
		$('#emailmsg').removeClass('alert alert-warning')
	}
	
	
	var streetAddress = $('#streetAddress').val().trim();	
	if(streetAddress==''){	
		//alert('Please enter marks');
		$('#streetAddressmsg').html('Please enter the street address');
		$('#streetAddressmsg').addClass('alert alert-warning')
		$('#streetAddress').focus();		
		flag=1;
	}
	else{
		$('#streetAddressmsg').html('');
		$('#streetAddressmsg').removeClass('alert alert-warning')
	}
	
	
	var city = $('#city').val().trim();	
	if(city==''){	
		//alert('Please enter marks');
		$('#citymsg').html('Please enter the city');
		$('#citymsg').addClass('alert alert-warning')
		$('#city').focus();		
		flag=1;
	}
	else{
		$('#citymsg').html('');
		$('#citymsg').removeClass('alert alert-warning')
	}
	
	
	var state = $('#state').val().trim();	
	if(state==''){	
		//alert('Please enter marks');
		$('#statemsg').html('Please enter the state');
		$('#statemsg').addClass('alert alert-warning')
		$('#state').focus();		
		flag=1;
	}
	else{
		$('#statemsg').html('');
		$('#statemsg').removeClass('alert alert-warning')
	}
	
	var zip = $('#zip').val().trim();	
	if(zip==''){	
		//alert('Please enter marks');
		$('#zipmsg').html('Please enter the zip code');
		$('#zipmsg').addClass('alert alert-warning')
		$('#zip').focus();		
		flag=1;
	}
	else if (!isUSAZipCode(zip)) 
  {
    $('#zipmsg').html('Please enter correct zip code');
		$('#zipmsg').addClass('alert alert-warning')
		$('#zip').focus();		
		flag=1;
  } 
	else{
		$('#zipmsg').html('');
		$('#zipmsg').removeClass('alert alert-warning')
	}
	
	var memberNumber = $('#memberNumber').val().trim();	
	if(memberNumber==''){	
		//alert('Please enter marks');
		$('#memberNumbermsg').html('Please enter the member number');
		$('#memberNumbermsg').addClass('alert alert-warning')
		$('#memberNumber').focus();		
		flag=1;
	}
	else{
		$('#memberNumbermsg').html('');
		$('#memberNumbermsg').removeClass('alert alert-warning')
	}
	
	var ckhpassword = $('#password').val().trim();	
	
	var password_regex1=/([a-z].*[A-Z])|([A-Z].*[a-z])([0-9])+([!,%,&,@,#,$,^,*,?,_,~])/;
	var password_regex2=/([0-9])/;
	var password_regex3=/([!,%,&,@,#,$,^,*,?,_,~])/;
	if(ckhpassword==''){	
		//alert('Please enter marks');
		$('#passwordmsg').html('Please enter the password');
		$('#passwordmsg').addClass('alert alert-warning')
		$('#password').focus();		
		flag=1;
	}
	/*else if(ckhpassword.length<8 || password_regex1.test(ckhpassword)==false || password_regex2.test(ckhpassword)==false || password_regex3.test(ckhpassword)==false)*/
else if(ckhpassword.length<8) {
		$('#passwordmsg').html('Please enter strong password');
		$('#passwordmsg').addClass('alert alert-warning')
		$('#password').focus();		
		flag=1;
 }
	else{
		$('#passwordmsg').html('');
		$('#passwordmsg').removeClass('alert alert-warning')
	}
	
	
	var password_confirmation = $('#password_confirmation').val().trim();	
	if(password_confirmation==''){	
		//alert('Please enter marks');
		$('#password_confirmationmsg').html('Please enter the confirm password');
		$('#password_confirmationmsg').addClass('alert alert-warning')
		$('#password_confirmation').focus();		
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('');
		$('#password_confirmationmsg').removeClass('alert alert-warning')
	}
	
	
	if(ckhpassword!=password_confirmation){
		$('#password_confirmationmsg').html('Please enter the correct confirm password');
		$('#password_confirmationmsg').addClass('alert alert-warning')
		$('#password_confirmation').focus();		
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('');
		$('#password_confirmationmsg').removeClass('alert alert-warning')
	}
	
	var is_employee = $('#is_employee').val().trim();	
	if(is_employee==''){	
		//alert('Please enter marks');
		$('#is_employeemsg').html('Please select member or employee');
		$('#is_employeemsg').addClass('alert alert-warning')
		$('#is_employee').focus();		
		flag=1;
	}
	else{
		$('#is_employeemsg').html('');
		$('#is_employeemsg').removeClass('alert alert-warning')
	}
	
	var ckb_status = $("#counseling_and_privacy_agreement").prop('checked'); 
	if(!ckb_status){
		$('#counseling_and_privacy_agreementmsg').html('Please click on checkbox to agree with terms and conditions');
		$('#counseling_and_privacy_agreementmsg').addClass('alert alert-warning')
	}	
	else{
		
		$('#counseling_and_privacy_agreementmsg').removeClass('alert alert-warning').html('');
	}
	


if(flag==1)	
	return false;
	else return true;

}

function validforgot(){
	var flag=0;
	var email = $('#user').val();
	if((document.getElementById('user').value).trim()==''){
	
		//alert('Please enter marks');
		$('#usermsg').html('Please enter the email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();
		flag=1;
	}
	else if(IsEmail(email)==false){
        $('#usermsg').html('Please enter the correct email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
        }
	else{
		$('#usermsg').html('');
		$('#usermsg').removeClass('alert alert-warning')
	}	
	
if(flag==1)	
	return false;
	else return true;

}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}

function isUSAZipCode(str) 
{
  return /^\d{5}(-\d{4})?$/.test(str);
}

