
function validlogin(){
	var flag=0;
	var email = $('#user').val();
	if((document.getElementById('user').value).trim()==''){	
		//alert('Please enter marks');
		$('#usermsg').html('Please enter the email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
	} 
	else if(IsEmail(email)==false){
        $('#usermsg').html('Please enter the correct email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
        }
	else{
		$('#usermsg').html('');
		$('#usermsg').removeClass('alert alert-warning')
	}	
	if((document.getElementById('pass').value).trim()==''){
		//alert('Please enter question');
		$('#passmsg').addClass('alert alert-warning')
		document.getElementById('pass').focus();
		$('#passmsg').html('Please enter the password');
		flag=1;
	}
	 else{
		$('#passmsg').html('');
		$('#passmsg').removeClass('alert alert-warning')
	}
	
if(flag==1)	
	return false;
	else return true;

}


function validRegister(){
	
	var flag=0;
	//alert('dg')
	if((document.getElementById('firstname').value).trim()==''){	
		//alert('Please enter marks');
		$('#firstnamemsg').html('Please enter the firstname');
		$('#firstnamemsg').addClass('pmsg')
		
		flag=1;
	}	
	else{
		$('#firstnamemsg').html('');
		$('#firstnamemsg').removeClass('pmsg')
	}

	if((document.getElementById('lastname').value).trim()==''){	
		//alert('Please enter marks');
		$('#lastnamemsg').html('Please enter the lastname');
		$('#lastnamemsg').addClass('pmsg')
		
		flag=1;
	}	
	else{
		$('#lastnamemsg').html('');
		$('#lastnamemsg').removeClass('pmsg')
	}
	
	var email = $('#email').val().trim();	
	if(email==''){	
		$('#emailmsg').html('Please enter the email id');
		$('#emailmsg').addClass('pmsg')
		//$('#email').focus();		
		flag=1;
	}
	else if(IsEmail(email)==false){
        $('#emailmsg').html('Please enter the correct email id');
		$('#emailmsg').addClass('pmsg')
		
		flag=1;
    }	
	else{
		$('#emailmsg').html('');
		$('#emailmsg').removeClass('pmsg')
	}
	
		var zip = $('#zip').val().trim();
	//alert(zip)		
	if (zip!='' && !isUSAZipCode(zip)) 
	{
		$('#zipmsg').html('Please enter correct zip code');
		$('#zipmsg').addClass('pmsg')
		//$('#zip').focus();		
		flag=1;
	} 
	else{
		$('#zipmsg').html('');
		$('#zipmsg').removeClass('pmsg')
	}
	
	
	
	var ckhpassword = $('#password').val().trim();	
	
	var password_regex1=/([a-z].*[A-Z])|([A-Z].*[a-z])([0-9])+([!,%,&,@,#,$,^,*,?,_,~])/;
	var password_regex2=/([0-9])/;
	var password_regex3=/([!,%,&,@,#,$,^,*,?,_,~])/;
	if(ckhpassword==''){	
		//alert('Please enter marks');
		$('#passwordmsg').html('Please enter the password');
		$('#passwordmsg').addClass('pmsg')
		//$('#password').focus();		
		flag=1;
	}
	/*else if(ckhpassword.length<8 || password_regex1.test(ckhpassword)==false || password_regex2.test(ckhpassword)==false || password_regex3.test(ckhpassword)==false)*/
	else if(ckhpassword.length<8) {
		$('#passwordmsg').html('Please enter strong password');
		$('#passwordmsg').addClass('pmsg')
		//$('#password').focus();		
		flag=1;
 }
	else{
		$('#passwordmsg').html('');
		$('#passwordmsg').removeClass('pmsg')
	}
	
	
	var password_confirmation = $('#password_confirmation').val().trim();	
	if(password_confirmation==''){	
		//alert('Please enter marks');
		$('#password_confirmationmsg').html('Please enter the confirm password');
		$('#password_confirmationmsg').addClass('pmsg')
		//$('#password_confirmation').focus();		
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('');
		$('#password_confirmationmsg').removeClass('pmsg')
	}
	
	
	if(ckhpassword!=password_confirmation){
		$('#password_confirmationmsg').html('Please enter the correct confirm password');
		$('#password_confirmationmsg').addClass('pmsg')
		//$('#password_confirmation').focus();		
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('');
		$('#password_confirmationmsg').removeClass('pmsg')
	}
	
	
	
	var ckb_status = $("#counseling_and_privacy_agreement").prop('checked'); 
	//alert(ckb_status)
	if(!ckb_status){
		$('#counseling_and_privacy_agreementmsg').html('Please click on checkbox to agree with terms and conditions');
		$('#counseling_and_privacy_agreementmsg').addClass('pmsg')
	}	
	else{
		
		$('#counseling_and_privacy_agreementmsg').removeClass('pmsg').html('');
	}
	
	/*var googleResponse = $('#g-recaptcha-response').val();
	if (!googleResponse) {
		$('#grecaptchamsg').html('Please select google captcha');
		$('#grecaptchamsg').addClass('pmsg')		
		flag=1;
	} else {            
		$('#grecaptchamsg').html('');
		$('#grecaptchamsg').removeClass('pmsg')
	}*/
	


if(flag==1)	
	return false;
	else return true;

}

function validforgot(){
	var flag=0;
	var email = $('#user').val();
	if((document.getElementById('user').value).trim()==''){
	
		//alert('Please enter marks');
		$('#usermsg').html('Please enter the email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();
		flag=1;
	}
	else if(IsEmail(email)==false){
        $('#usermsg').html('Please enter the correct email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
        }
	else{
		$('#usermsg').html('');
		$('#usermsg').removeClass('alert alert-warning')
	}	
	
if(flag==1)	
	return false;
	else return true;
}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}

function isUSAZipCode(str) 
{
  return /^\d{5}(-\d{4})?$/.test(str);
}

