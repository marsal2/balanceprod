<?php
error_reporting(E_ALL);
require('vendor/autoload.php');
$html='<div class = "block report-holder background-white default-content-style">
            <section class = "report-block">
                <div class = "container">
                    <div class = "row">
                        <div class = "img-holder goofy">
                            <img src="https://www.balancepro.org/wp-content/uploads/program_BT.jpg" width="700" height="410" alt="image description" class = "height-calc">
                        </div>

                        <div class = "text-holder height-calc goofy">
                            <div class = "text">
                                <h1 class = "text-info">BalanceTrack</h1>
                                <p>Learn the basics of personal finance with the BalanceTrack educational modules. Register for a free user account or log in to get started.</p><a href="http://www.devxekera.com/quiz/programs/balance-track" class="btn btn-warning">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>';
$mpdf=new \Mpdf\Mpdf();
$mpdf->WriteHTML($html);
$file=time().'.pdf';
$mpdf->output($file,'F');
//D
//I
//F
//S
?>
