<style type="text/css">
    .form-group {
        background-color: #fff;
        padding: 10px;
    }

    .form-control {
        border-radius: 0;
        box-shadow: none;
        background-color: #fff;
        border: 1px solid #bbb;
        color: #044543;
    }

    .form-control:focus {
        box-shadow: none;
        border-color: #999;
    }

    #myModal .modal-content form label {
        font-weight: normal;
        margin-bottom: 2px;

    }

    input[type="checkbox"] {
        width: 30px;
       
        height: 30px;     

    }

    input[type="radio"] {
        width: 24px;
      
        height: 18px;
      
    }

    #myModal span {
        color: red;
        font-size: 14px;
    }

    .login-modal .modal {
        text-align: left;

    }

    #myModal .close {
        opacity: 0.4;
        position: absolute;
        background-color: #3a6466;
        right: 10px;
        top: 5px;
        z-index: 1;
    }

   
    </style>
 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <!-- Tab panes -->
                    <div id="register">
                        <div class="modal-header">
                            <h4 class="text-info" style="text-align: center;">Sign Up For Your BALANCE Account.</h4>
                            <p style="text-align: center;">Please register as a new user to access our online education programs.</p>
                        </div>
                        <div class="modal-body">
                            <form class="row" autocomplete="off" id='frm_register'>
								<input type="hidden" name="registerfrm" value="1">
                                <input type="hidden" name="action" value="do_register">
								<input type='hidden' id='mailexistsid' value='0'>                                                        
                                <div class="form-group col-sm-6">
                                    <label for="first-name">First Name</label> <span>*</span>
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name"><span id='firstnamemsg'></span>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="last-name">Last Name</label> <span>*</span>
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
									<span id='lastnamemsg'></span>
                                </div>
                                <div class="new-customer clearfix">
                                    <div class="form-group col-sm-6">
                                        <label for="email-address">Email Address</label> <span>*</span>
                                        <input type="email" class="form-control" id="emailweb" name="email" placeholder="Email" onfocusout="chkUserExits(this.value)"><span id='emailmsg'></span>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="Street Address">Street Address</label>
                                        <input type="text" class="form-control" id="streetAddress" name="streetAddress" placeholder="Street Address">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="zip-code">Zip Code</label>
                                        <input type="tel" class="form-control" id="zip" name="zip" placeholder="5 digit zip" maxlength=5 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
										<span id='zipmsg'></span>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control" id="city" name="city" placeholder="City, Town">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="state">State</label>
                                        <input type="text" class="form-control" id="state" name="state" placeholder="State">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="phone">Last four digits of account/member number</label>
                                        <input type="number" class="form-control" id="memberNumber" name="memberNumber" placeholder="4 digit number">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="Member/client">Are you a member/client or employee?</label><br>
                                        <input type="radio" id="Member/client" name="is_employee" value="0" checked>   Member/client
                                          <input type="radio" id="Employee" name="is_employee" value="1">  Employee
                                        <!-- <select name="is_employee" id="is_employee" class="form-control">
                                            <option value="member">Member/client</option>
                                            <option value="employee">Employee</option>
                                        </select> -->
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="password">Password</label> <span>*</span>
                                    <input type="password" class="form-control" name="password" id="passwordweb" placeholder="Password" onfocusout="validPass(this.value)">
									<span id='passwordmsg'></span>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="confirm-password">Confirm Password</label> <span>*</span>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" onfocusout="cvalidPass(this,'passwordweb')" placeholder="Password"><span id='password_confirmationmsg'></span>
                                </div>
                                <small class="col-xs-12 pmsg" style="font-size: 14px;font-style: italic;">Password must be a minimum of 8 characters and should contain capital letter, number and special character.</small>
                                <div class="form-group col-sm-12">
                                    <label for="counseling_and_privacy_agreement">I agree with Counseling Agreement and Privacy Policy</label> <span>*</span>
                                    <input type="checkbox" name="counseling_and_privacy_agreement" id="counseling_and_privacy_agreement">
                                    <span class="fake-input"></span><span id="counseling_and_privacy_agreementmsg">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class=" but btn btn-primary " id='frmsubmitbtn'>Create Account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		
		
	   </div>
	   
	   
	   
	   <div id="Modalsuccess" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
			<div role="document" class="modal-dialog">
				<div class="modal-content">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
					<div class="pop-up-tab visible-xs">
						<button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
					</div>
					<div class="form-content" id='log_reg_msg_id'>						
						<p style="text-align:left;" >Registration successfully done</p>						
					</div>
				</div>
			</div>
		</div>
		
		
		<div id="Modalerror" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
			<div role="document" class="modal-dialog">
				<div class="modal-content">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
					<div class="pop-up-tab visible-xs">
						<button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
					</div>
					<div class="form-content">
						
						<p style="text-align:left;" id='err_msg'>Somethis went wrong. Please try again!</p>
						
					</div>
				</div>
			</div>
		</div>


	   