<?php

//$postid = '1635';//$_SESSION['wlw_wp_post_id'];//isset($_GET['postid'])?$_GET['postid']:'1635';
$newArr=getQuizQuestions($postid);
$wlw = $_SESSION['white_label_website_id'];
//$_SESSION['correctquiz']=$newArr['correct'];

$chkFlag = chkIntervalForQuiz($postid);
//echo "<pre>";
//print_r($_SESSION['test_duration']);
//echo "</pre>";
?> <!-- login quiz code start -->
    <main id="main">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="<?=$base_url?>">Home</a></li>
                        <!-- Link to resource page -->
                        <li>Quizzes</li>
                        <li><?=$newArr['post_title']?></li>
                        
                    </ol>
                </div>
            </div>
        </div>
   <?php ///echo '<pre>',print_r($newArr);echo '</pre>';
   
   if(count($newArr)>0){
   //$copy = (trim($newArr['copy'])!='') ? $newArr['copy'] : $copy;
   
  // echo  'message_success',$newArr['message_success'];
      
   if(isset($_SESSION['message']) && $_SESSION['message']!=''){
		
	?>
			<div class="alert alert-info">
		  <span class="closebtn">&times;</span>  
		  <?php echo $_SESSION['message']?>
			</div>
		<?php
		}	
		unset($_SESSION['message']);
		if($chkFlag=='noinsert'){
			?>
			<div class="alert alert-info">
			You have recently taken this quiz. You must wait at least <?php echo $_SESSION['test_duration']?> hours to access it again.
		  
			</div>
			<?php
		}
		?>
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header">
            <div class="container">
                <div class="row">
                    <div class="">
                        <h1 class="text-info text-center">Quiz</h1>
                        <p><span style="line-height: 30px;"><?=$newArr['copy']?></span></p>
                    </div>
                </div>
            </div>
        </article>
        
        <section aria-label="quiz questions" class="quiz-questions modules-form-section">
            <div class="container">
                <div class="row">
                    <form id="quiz-form" method="post" class="form-wrap" onsubmit="return validQuiz()">
					<input type='hidden' name='action' value='submit_query'>
					<input type='hidden' name='max_tries' value="6"><?php //=$newArr['max_tries']?>
					<input type='hidden' name='cooldown' value="1"><?php //=$newArr['cooldown']?>
					<!--input type='hidden' name='success_rate' value="<?=$newArr['success_rate']?>">
					<input type='hidden' name='certificate_title' value="<?=$newArr['certificate_title']?>">
					<input type='hidden' name='email_subject' value="<?=$newArr['email_subject']?>">
					<input type='hidden' name='email_body' value="<?=$newArr['email_body']?>">
					<input type='hidden' name='message_success' value="<?=$newArr['message_success']?>">
					<input type='hidden' name='has_certificate' value="<?=$newArr['has_certificate']?>">
					<input type='hidden' name='post_title' value="<?=$newArr['post_title']?>"-->
					
                        <div class="quiz-questions">
                            <ol class="questions">
							<?php for($i=0;$i<count($newArr['question']);$i++){
								$question = $newArr['question'][$i];
								$answers = $newArr['answer'][$i];							
								
								?>
                                <li><strong class="h3"><?php echo str_replace('"','',$question); ?></strong>
                                    <ul class="choose-list">
									<?php 
									for($b=0;$b<count($answers);$b++){
									?>
                                        <li> <label > 
                                          <input type="radio" name="answer-<?=$i?>" value="<?=$b?>"> <span class="fake-input"></span><span class="fake-label "><?php echo str_replace('"','',$answers[$b]); ?> <span class=""></span></span></label>
                                        </li>                  
									<?php } ?>                                        
                                    </ul>
                                </li>
                                <?php } ?>
                            </ol>
                            
							<input type="hidden" id="quizId" name="quizid" value="<?=$postid?>"><input type="hidden" name="wlw" value="<?=$wlw?>"> 
							<input type="submit" value="Submit" class="btn but btn-warning submit-quiz">
                        </div>
                    </form>
                </div> 
            </div>
        </section>
   <?php } else {?> 
   <p>
   
   <div class="alert alert-warning">
  <span class="closebtn">&times;</span>  
  No quiz here
</div>
   </p>
   <?php } ?>
   
        </main>
<?php
if($chkFlag=='noinsert'){
	?>
	<script>
	$(':radio,:checkbox').click(function(){
    return false;
});
$('.submit-quiz').hide();

	</script>
	<?php
}
?>
<script>
$(window).load(function() {
    $('form').get(0).reset(); //clear form data on page load
});
function validQuiz(){
	var x = $("input[type = 'radio']:checked");  
	if (x.length<2) {
		alert('Please answer minimum 2 questions');
		return false;
	} 
	return true;
}
</script>
 <!-- login quiz code end -->

