<div id="modalThank" tabindex="-1" role="dialog" class="modal fade">
        <div  class="modal-dialog">
            <div class="modal-content">
                 <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">X</span></button>    <!-- &#215; -->
                <div class="alert " role="alert">                   
                    <h1 class="text-info text-center">Thank you for verifying your email address</h1>
                    <p>Thank you for completing your registration with BALANCE, in partnership with Advantage Home Plus. You’ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>
                    <p>Your registration gives you access to more BALANCE online education programs available on this website. Use your email address and BALANCE password to log in to a program when prompted.</p>
                    <p>We hope you enjoy your experience with BALANCE, in partnership with Advantage Home Plus. We are here to help you achieve your financial successes!</p>
                    <p>Best,</p>
                    <p>BALANCE in partnership with Advantage Home Plus</p>
                </div>
            </div>
        </div>
    </div>