   <main id="main">
	    <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                       
                        <li><a href="<?=$base_url?>">Home</a></li>
                       
                        <li>Past Quizzes</li>
                       
                    </ol>
                </div>
            </div>
        </div>
  
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header">
            <div class="container">
                <div class="row">
                    <div>
                        <h1 class="text-info text-center">Past Quizzes</h1>                        
                    </div>
                </div>
				<div class="row">
                    <div class="text-info" style='font-weight:bold;font-size:18px;'>
                        [Quiz results are available for <?php echo $pastquiz_days?> days]
                    </div>
                </div>
            </div>
        </article>
        <div class="container">
        	<div class="row">
<?php if(!empty($data)){?>		
		<table class="table-wor" style="margin-bottom: 30px;">
  <thead>
    <tr>
      <th>Sr.No</th>      
      <th>Quiz</th>
      <th>Date/Time</th>
      <th>Percentage</th>     
      <th>Correct Answers</th>
      <th>Certificate</th>
    </tr>
  </thead>
  <tbody>
  <?php for($i=0;$i<count($data);$i++){								
		$quizdata=getQuizQuestions($data[$i]['wp_post_id']);
		$sdata = & $data[$i];
	?>
    <tr>
      <td data-column="sr.no"><?=$i+1?></td>      
      <td data-column="Quiz" style='text-align: left;'><?php echo $quizdata['post_title']; ?></td>
      <td data-column="Date" style='text-align: left;'><?=date('d F Y, h:i A',strtotime($sdata['timestamp']))?></td>
      <td data-column="Percentage"><?php echo round($sdata['score'],2)?>%</td>
      <td data-column="Correct Answers"><?=$sdata['num_correct']?></td>    
      <td data-column="Certificate"><a href="<?=$base_url?>uploads/<?=$sdata['crtf_name']?>" title='View Certificate' target='_blank'>View</a> | <a href="<?=$base_url?>uploads/<?=$sdata['crtf_name']?>" title='Download Certificate' target='_blank' download>Download</a></td>
    </tr>
<?php } ?>  
   
  </tbody>
</table>
</div>
</div>
<?php } else{ ?>
<div class="alert">
  
  No past quizzes here.
</div>
<?php } ?>
  
</main>