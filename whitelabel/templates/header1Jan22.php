 <?php if(isset($_SESSION['user_name']) && $_SESSION['user_name']!='')
 {
	 $displaynamex=explode(' ',$_SESSION['user_name']);
	 $displayname = substr($displaynamex[1],0,8);
	 $logged_style="";
	 $nolog_style="display:none"; 
 }	 
 else{
	$logged_style="display:none"; 
	$nolog_style='';
 }
 $phone = $_SESSION['phone'];
if(empty($phone)) $phone='888-456-2227';

	//print_r($_SESSION);
	?>
 <header id="header" class="nav-fixed">
        <div class="header-t private-header-t no-chat" style="background-image: url(<?=$base_url?>assets/img/img01.png);">
            <div class="container">
                <div class="row">
                    <!-- search form -->
                    <ul class="contact-block">
					<?php if(isset($_SESSION['accesschat']) && trim($_SESSION['accesschat'])!='y'){ ?>					
                    	<li>
                        <a onclick="window.open('http://chat.balancepro.org/i3root/chat_cccs/index.html', 'Chat', 'resizable,height=500,width=500')" class="chat" href='javascript:void(0)'>
                            <span>
                                <img src="<?=$base_url?>assets/img/chat_icon.png" style="width: 23px;height: 24px;margin: 7px 2px 0 0;">
                            </span>Chat
                        </a>
						</li>
					<?php } ?>
                        <li><a href="tel:<?=$phone?>"><span class="icon-phone"></span><?=$phone?></a>
                        </li>
						
                        <li id='logged_name_hdr' style="<?=$logged_style?>"><a href="<?=$base_url?>account"><span class="icon-user"></span><?php echo $displayname?></a>
                        </li>
						<li class="hidden-xs" id='logout_hdr' style="<?=$logged_style?>"><a  href="<?=$base_url?>logout" title='Logout'><span class="icon-user"></span>LOGOUT</a>
                        </li>
						
						<!--li class="hidden-xs" id='nologin_hdr' style="<?=$nolog_style?>"><a data-toggle="modal" data-target="#Modal1" href="#Modal1"><span class="icon-user"></span>LOG IN</a>
                        </li-->
						<li class="hidden-xs" id='nologin_hdr' style="<?=$nolog_style?>"><a data-toggle="modal"  href="<?=$base_url?>login"><span class="icon-user"></span>LOG IN</a>
                        </li>
						
                    </ul>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default private-header">
            <div class="container">
                <div class="row">
                    <div class="navbar-header private-header">
                        <button aria-expanded="false" data-target="#bs-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- page logo -->
						<?php $logo = (isset($_SESSION['logo']) && $_SESSION['logo']!='') ? $_SESSION['logo'] : $base_url.'assets/img/logo.svg'?>
						<?php $title = isset($_SESSION['title']) ? $_SESSION['title'] : ''?>
                        <a href="<?=$base_url?>" class="navbar-brand navbar-promotion" title="<?=$title?>"><img src="<?=$logo?>" alt="<?=$title?>"></a>
                        <!-- promotion banner -->
						<?php if(isset($_SESSION['promo_logo']) && $_SESSION['promo_logo']!=''){?>
                        <a href="<?=$_SESSION['promotion_logo_url']?>" class="navbar-brand navbar-promotion" title="<?=$title?>"><img src="<?=$_SESSION['promo_logo']?>" alt="<?=$_SESSION['promo_logo_name']?>"></a>
						<?php } ?>
                    </div>
                    <!-- main navigation of the page -->
                    <div id="bs-navbar-collapse-1" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <?php if(isset($_SESSION['accesstoresources']) && trim($_SESSION['accesstoresources'])!=''){ ?>
							
							<li <?php if($_GET['action']=='resources'){?>class='active'<?php } ?>>
                                <a href="<?=$base_url?>index.php?action=resources" title='Resources'>Resources</a>
                            </li>
							<?php } ?>
							
							<?php if(isset($_SESSION['accesstoprograms']) && trim($_SESSION['accesstoprograms'])!=''){ ?>
                            <li <?php if($_GET['action']=='programs1'){?>class='active'<?php } ?>>
                                <a href="<?=$base_url?>programs" title='Programs'>Programs</a>
                            </li>
							<?php } ?>
							<?php if(isset($_SESSION['webinars_page_html']) && trim($_SESSION['webinars_page_html'])!=''){ ?>
							
							 <li <?php if($_GET['action']=='webinars'){?>class='active'<?php } ?>>
                                <a href="<?=$base_url?>index.php?action=webinars" title='Webinars'>Webinars</a>
                            </li>
							<?php } ?>
							<!-- <li id='li_account' style="<?=$logged_style?>">
                                <a href="<?=$base_url?>index.php?action=account" title='My Account'>Account</a>
                            </li> -->
                            <li style="<?=$logged_style?>" <?php if($_GET['action']=='account'){?>class='active'<?php } ?>><a href="<?=$base_url?>account" aria-label="header menu" class="">Account       
                            <span class="dropdown-toggle custom-caret visible-xs">
                                <img src="http://www.devxekera.com/wp-content/themes/balance-theme/images/drop-arrow.svg" width="19" height="11" alt="">
                            </span></a> 
                            

                                        <div class="slide-drop" style="position: absolute; top: -9999px; left: -9999px; width: 98px;">
                      						<ul class="custom-dropdown-menu ">
                                                  <li><a href="<?=$base_url?>pastcertificate" title='Past Certificates'>Past Certificates</a></li>                     
                                              </ul>
                    					</div>
                                                        
               				 </li>







							<!--li id='li_account' style="<?=$logged_style?>">
                                <a href="<?=$base_url?>index.php?action=loginquiz1" title='Quiz'>Quiz</a>
                            </li-->
                            <li <?php if($_GET['action']=='contact'){?>class='active'<?php } ?>>
                                <a href="<?=$base_url?>contact" class="" title='Contact Balance'>Contact Balance</a>
                            </li>
							<?php if($nolog_style==''){?>
                            <li class="visible-xs" id='nologin_hdrmob'><a href="<?=$base_url?>login" class="dropdown-toggle">Log In<span class="custom-caret visible-xs"><img src="<?=$base_url?>assets/img/drop-arrow.svg" width="19" height="11" alt=" "></span></a></li>
							<?php } else { ?>
							<li class="visible-xs">
                                <a href="<?=$base_url?>logout" title='Logout'>Logout</a>
                            </li>
							<?php }  ?>
							
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
				
            </div>
        </nav>
    </header>