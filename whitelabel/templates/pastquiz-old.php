
<style>
table { 
	width: 80%; 
	border-collapse: collapse; 
	margin:50px auto;
	}

/* Zebra striping */
tr:nth-of-type(odd) { 
	background: #eee; 
	}

th { 
	background: #73c8cb;
    color: black;
	}

td, th { 
	padding: 10px; 
	border: 1px solid #ccc; 
	text-align: center; 
	font-size: 14px;
	}

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

	table { 
	  	width: 100%; 
	}

	/* Force table to not be like tables anymore */
	table, thead, tbody, th, td, tr { 
		display: block; 
	}
	
	/* Hide table headers (but not display: none;, for accessibility) */
	thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	
	tr { border: 1px solid #ccc; }
	
	td { 
		/* Behave  like a "row" */
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
	}

	td:before { 
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
		/* Label the data */
		content: attr(data-column);

		color: #000;
		font-weight: bold;
	}

}
</style>
   

   <!-- login quiz code start -->
    <main id="main">
	
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="<?=$base_url?>">Home</a></li>
                        <!-- Link to resource page -->
                        <li>Past Quizzes</li>
                       
                    </ol>
                </div>
            </div>
        </div>
   <?php ///echo '<pre>',print_r($newArr);echo '</pre>';?>
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header">
            <div class="container">
                <div class="row">
                    <div class="">
                        <h1 class="text-info text-center">Past Quizzes</h1>                        
                    </div>
                </div>
            </div>
        </article>
<?php if(!empty($data)){?>		
		<table>
  <thead>
    <tr>
      <th>Sr.No</th>
      <!--th>Websites</th-->
      <th>Quiz</th>
      <th>Date/Time</th>
      <th>Percentage</th>     
      <th>Correct Answers</th>
      <th>Certificate</th>
    </tr>
  </thead>
  <tbody>
  <?php for($i=0;$i<count($data);$i++){								
		$quizdata=getQuizQuestions($data[$i]['wp_post_id']);
		$sdata = & $data[$i];
	?>
    <tr>
      <td data-column="sr.no"><?=$i+1?></td>
      <!--td data-column="Website"></td-->
      <td data-column="Quiz"><?php echo $quizdata['post_title']; ?></td>
      <td data-column="Date"><?=date('d F Y, h:i A',strtotime($sdata['timestamp']))?></td>
      <td data-column="Percentage"><?=$sdata['score']?>%</td>
      <td data-column="Correct Answers"><?=$sdata['num_correct']?></td>    
      <td data-column="Certificate"><a href="<?=$base_url?>uploads/<?=$sdata['crtf_name']?>" title='View Certificate' target='_blank'>View</a> | <a href="<?=$base_url?>uploads/<?=$sdata['crtf_name']?>" title='Download Certificate' target='_blank' download>Download</a></td>
    </tr>
<?php } ?>  
   
  </tbody>
</table>
<?php } else{ ?>
<div class="alert">
  
  No past quizzes here.
</div>
<?php } ?>
  
        </main>

<script>
/*
$( document ).ready(function() {
	$("#search").on('keyup', function(e) {		
	var search = this.value.trim();
	///alert(search)
	$.ajax({
			url: 'index.php?action=search_quiz&q='+search,
			type: 'GET',
			success: function(data) {
				//alert(data)

				//$('#quizdata').html('')
				$('#quizdata').html(data)
			},
			cache: false,
			contentType: false,
			processData: false
		});
	
})	
});*/
</script>
 <!-- login quiz code end -->

