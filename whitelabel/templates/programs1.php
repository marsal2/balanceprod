<main id="main" role="main">
    <style>
    .report-block img {
        height: 315px;
        width: auto;
    }

    .report-block .image-holder,
    .report-block .copy-holder {
        width: 50%;
    }

    .report-block .copy-holder {
        position: relative;
        padding: 0 38px;
        text-align: left;
    }

    @media  screen and (max-width: 1024px) {
        .report-block .image-holder {
            width: 100%;
        }

        .report-block .image-holder img {
            width: 100%;
            height: auto !important;
        }

        .report-block .copy-holder {
            width: 100%;
            padding-top: 25px;
            padding-bottom: 25px;
            top: 0;

            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            transform: translateY(0%);
        }
    }
</style>

    <div class="banner" style="background-image: url('https://arrowheadcu.balancepro.org/images/T52_header_desktop.jpg');">
    <div class="container">
        <div class="row">
            <div class="banner-block short">
                <div class="banner-text">
                    <h1 style="font-size: 44px;">Programs to guide you towards financial success</h1>
                    <p>Check out our available programs below</p>
                </div>
            </div>
        </div>
    </div>
</div>
		<?php for($i=0; $i<count($data); $i++){
				if($i%2==0) $goofy='goofy';else $goofy='';
				$img = getProgramFeaturedImage($data[$i]['ID']);
				//print_r($img);
			?>
            <div class = "block report-holder background-white default-content-style">
            <section class = "report-block">
                <div class = "container">
                    <div class = "row">
                        <div class = "img-holder <?=$goofy?>">
                            <img src="https://www.balancepro.org/wp-content/uploads/<?=$img[0]['meta_value']?>" width="700" height="410" alt="image description" class = "height-calc">
                        </div>

                        <div class = "text-holder height-calc <?=$goofy?>">
                            <div class = "text">
                                <h1 class = "text-info"><?=$data[$i]['post_title']?></h1>
                                <p><?=$data[$i]['post_excerpt']?></p>
								<!--a href="<?=$base_url?>index.php?action=programs1&article=<?=$data[$i]['post_name']?>" class="btn btn-warning" title="<?=$data[$i]['post_title']?>">Learn More</a-->
								
								<a href="<?=$base_url?>programs/<?=$data[$i]['post_name']?>" class="btn btn-warning" title="<?=$data[$i]['post_title']?>">Learn More</a>
								
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			</div>         
		<?php } ?>
            </main>