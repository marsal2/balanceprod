<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_White_Label_Websites
 * @subpackage Balance_White_Label_Websites/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Balance_White_Label_Websites
 * @subpackage Balance_White_Label_Websites/admin
 * @author     ROIDNA <devs@roidna.com>
 */
class Balance_White_Label_Websites_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string  $plugin_name The name of this plugin.
	 * @param string  $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_White_Label_Websites_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_White_Label_Websites_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/balance-white-label-websites-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_White_Label_Websites_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_White_Label_Websites_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/balance-white-label-websites-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Creates custom tables for white label websites (entity table and relational table to access groups)
	 *
	 * @since     1.0.0
	 * @uses      $wpdb, dbDelta
	 */
	public function white_label_websites_install() {
		global $wpdb, $white_label_websites_db_version;

		$prefix = $wpdb->prefix;
		$white_label_websites_table_name = $prefix . 'white_label_websites';
		$white_label_websites_access_groups_table_name = $prefix . 'white_label_website_access_group';
		$post_table_id = $prefix . 'post_id';

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $white_label_websites_table_name (
			      white_label_website_id bigint(20) NOT NULL AUTO_INCREMENT,
			      domain varchar(255) DEFAULT '' NULL,
			      title longtext DEFAULT '' NULL,
			      sylinkid longtext DEFAULT '' NULL,
			      mainemail longtext DEFAULT '' NULL,
			      phone longtext DEFAULT '' NULL,
			      description longtext DEFAULT '' NULL,
			      logo longtext DEFAULT '' NULL,
			      homepage_html longtext DEFAULT '' NULL,
			      contact_us_page_html longtext DEFAULT '' NULL,
			      resources_page_html longtext DEFAULT '' NULL,
			      webinars_page_html longtext DEFAULT '' NULL,
			      programs_protected varchar(255) DEFAULT 'all' NULL,
			      status varchar(255) DEFAULT 'auto-draft' NULL,
			      $post_table_id bigint(20) NOT NULL,
			      UNIQUE KEY white_label_website_id (white_label_website_id)
			    );
			   	CREATE TABLE $white_label_websites_access_groups_table_name (
			      white_label_website_access_group_id bigint(20) NOT NULL AUTO_INCREMENT,
			      white_label_website_id bigint(20) NOT NULL,
			      access_group_id bigint(20) NOT NULL,
			      $post_table_id bigint(20) NOT NULL,
			      UNIQUE KEY white_label_website_access_group_id (white_label_website_access_group_id)
			    ) $charset_collate ENGINE = InnoDB;";

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );

		add_option( 'white_label_websites_db_version', $white_label_websites_db_version );
	}

	/**
	 * Called on save_post to insert or update the data in the resources table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @param object  $object  The post object
	 * @return  void
	 */
	public function insert_update_white_label_website( $post_id, $object ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return $post_id; }
		if ( ! current_user_can( 'edit_post', $post_id ) ) { return $post_id; }
		if ( ! current_user_can( 'edit_page', $post_id ) ) { return $post_id; }
		if ( $object->post_type == 'white_label_website' ) { // check if correct type
			global $wpdb;

			$prefix = $wpdb->prefix;
			$white_label_websites_table_name = $prefix . 'white_label_websites';
			$white_label_websites_access_groups_table_name = $prefix . 'white_label_website_access_group';
			$white_label_websites_admins_table_name = 'whitelabel_admins';
			$post_table_id = $prefix . 'post_id';

			// get custom data where the fields are stored
			$data = get_post_custom( $post_id );
			$unserialized_data = '';

			if ( isset( $data['_page_edit_data'] ) ) {
				$unserialized_data = @unserialize( $data['_page_edit_data'][0] );
			}
			if ( $unserialized_data === false ) {
				$unserialized_data = array();
			}

			$data = $unserialized_data;

			$domain = !empty( $data['wlw_general_info_module'][0]['domain'] ) ? $data['wlw_general_info_module'][0]['domain'] : '';
			$title = !empty( $data['wlw_general_info_module'][0]['title'] ) ? $data['wlw_general_info_module'][0]['title'] : '';
			$sylinkid = !empty( $data['wlw_general_info_module'][0]['sylink_id'] ) ? $data['wlw_general_info_module'][0]['sylink_id'] : '';
			$mainemail = !empty( $data['wlw_general_info_module'][0]['main_email'] ) ? $data['wlw_general_info_module'][0]['main_email'] : '';
			$phone = !empty( $data['wlw_general_info_module'][0]['phone'] ) ? $data['wlw_general_info_module'][0]['phone'] : '';
			$description = !empty( $data['wlw_general_info_module'][0]['description'] ) ? $data['wlw_general_info_module'][0]['description'] : '';
			$logo = !empty( $data['wlw_general_info_module'][0]['logo']['image']['fullpath'] ) ? $data['wlw_general_info_module'][0]['logo']['image']['fullpath'] : '';
			$programs_protected = !empty( $data['wlw_programs_module'][0]['programs_protected'] ) ? $data['wlw_programs_module'][0]['programs_protected'] : 'all';
			$access_groups = !empty( $data['wlw_resources_module'][0]['accesstoresources'] ) ? $data['wlw_resources_module'][0]['accesstoresources'] : '';
			$access_groups = explode( ',', $access_groups );
			$admins = !empty( $data['wlw_admins_module'][0]['admin'] ) ? $data['wlw_admins_module'][0]['admin'] : '';
			$white_label_website_id = $wpdb->get_var( "SELECT white_label_website_id FROM $white_label_websites_table_name WHERE $post_table_id = '$post_id';" );

			if ( !empty( $white_label_website_id ) ) {
				$update = $wpdb->update(
					$white_label_websites_table_name, // table
					array( // columns
						'domain' => $domain,
						'title' => $title,
						'sylinkid' => $sylinkid,
						'mainemail' => $mainemail,
						'phone' => $phone,
						'description' => $description,
						'logo' => $logo,
						'homepage_html' => $this->build_white_label_website_html( 'homepage', $data ),
						'contact_us_page_html' => $this->build_white_label_website_html( 'contact_us', $data ),
						'resources_page_html' => $this->build_white_label_website_html( 'resources', $data ),
						'webinars_page_html' => $this->build_white_label_website_html( 'webinars', $data ),
						'status' => $object->post_status,
						'programs_protected' => $programs_protected
					),
					array( // where
						$post_table_id => $post_id,
					),
					array( // format of the columns
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s'
					),
					array( // where format
						'%d'
					)
				);
			} else {
				$wpdb->insert(
					$white_label_websites_table_name, // table
					array( // columns
						'domain' => $domain,
						'title' => $title,
						'description' => $description,
						'logo' => $logo,
						'homepage_html' => $this->build_white_label_website_html( 'homepage', $data ),
						'contact_us_page_html' => $this->build_white_label_website_html( 'contact_us', $data ),
						'resources_page_html' => $this->build_white_label_website_html( 'resources', $data ),
						'webinars_page_html' => $this->build_white_label_website_html( 'webinars', $data ),
						'status' => $object->post_status,
						'programs_protected' => $programs_protected,
						$post_table_id => $post_id
					),
					array( // format of the columns
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%d'
					)
				);
				$white_label_website_id = $wpdb->get_var( "SELECT white_label_website_id FROM $white_label_websites_table_name WHERE $post_table_id = '$post_id';" );
			}

			// update the relation tables
			// delete from relation tables if any previous data

			// access group table
			$wpdb->delete(
				$white_label_websites_access_groups_table_name, // table,
				array( // where
					$post_table_id => $post_id,
				),
				array(
					'%d'
				)
			);

			foreach ( $access_groups as $access_group ) {
				// insert into relation table all the data
				$wpdb->insert(
					$white_label_websites_access_groups_table_name, // table
					array( // columns
						'white_label_website_id' => $white_label_website_id,
						'access_group_id' => $access_group,
						$post_table_id => $post_id
					),
					array( // format of the columns
						'%d',
						'%d',
						'%d'
					)
				);
			}

			// admins table
			$wpdb->delete(
				$white_label_websites_admins_table_name, // table,
				array( // where
					'white_label_website_id' => $white_label_website_id,
				),
				array(
					'%d'
				)
			);

			foreach ( $admins as $admin ) {
				if ( !empty( $admin['email'] ) ) {
					// insert into relation table all the data
					$wpdb->insert(
						$white_label_websites_admins_table_name, // table
						array( // columns
							'white_label_website_id' => $white_label_website_id,
							'email' => $admin['email'],
						),
						array( // format of the columns
							'%d',
							'%s',
						)
					);
				}
			}
		}
	}

	/**
	 * Called on delete_post to cleanup the data in the resources table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @return  void
	 */
	public function delete_white_label_website( $post_id ) {
		global $wpdb;

		$prefix = $wpdb->prefix;
		$white_label_websites_table_name = $prefix . 'white_label_websites';
		$white_label_websites_access_groups_table_name = $prefix . 'white_label_website_access_group';
		$white_label_websites_admins_table_name = 'whitelabel_admins';
		$post_table_id = $prefix . 'post_id';

		$white_label_website_id = $wpdb->get_var( "SELECT white_label_website_id FROM $white_label_websites_table_name WHERE $post_table_id = '$post_id';" );

		// delete from the table
		$wpdb->delete(
			$white_label_websites_table_name,
			array( // where
				$post_table_id => $post_id
			),
			array( // where format
				'%d'
			)
		);
		// delete from relation tables
		// access groups
		$wpdb->delete(
			$white_label_websites_access_groups_table_name,
			array( // where
				$post_table_id => $post_id
			),
			array( // where format
				'%d'
			)
		);
		// admins
		$wpdb->delete(
			$white_label_websites_admins_table_name, // table,
			array( // where
				'white_label_website_id' => $white_label_website_id,
			),
			array(
				'%d'
			)
		);
	}

	private function build_white_label_website_html( $page, $data ) {
		$html = '';
		if ( !empty( $page ) && !empty( $data ) ) {
			switch ( $page ) {
			case 'homepage':
				if ( !empty( $data['m52_module'] ) && !empty( $data['m52_module'][0] ) ) {
					$html .= render_m52_hero_with_login( $data['m52_module'][0] );
				}
				if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
					// use m54 renderer as the markup is different for wlw m34 module as for main balance site
					$html .= render_m54_welcome_message( $data['m34_module'][0] );
				}
				// TO DO: m53 renderer missing as there was no design for them yet
				if ( !empty( $data['m56_module'] ) && !empty( $data['m56_module'][0] ) ) {
					$html .= render_m56_m17_three_four_messages( $data['m56_module'][0], true );
				}
				break;
			case 'contact_us':
				if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][1] ) ) {
					$html .= render_m54_welcome_message( $data['m34_module'][1] );
				}
				// TO DO: for now it outputs the placeholder and not the shortcode/gravity forms markup
				$html .= '{{contact_us_form}}';
				// TO DO: m53 renderer missing as there was no design for them yet
				break;
			case 'resources':
				if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][2] ) ) {
					// use m54 renderer as the markup is different for wlw m34 module as for main balance site
					$html .= render_m54_welcome_message( $data['m34_module'][2] );
				}
				break;
			case 'webinars':
				if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
					$html .= render_m1_hero( $data['m1_module'][0] );
				}
				if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][3] ) ) {
					// use m54 renderer as the markup is different for wlw m34 module as for main balance site
					$html .= render_m54_welcome_message( $data['m34_module'][3] );
				}
				break;
			default:
				break;
			}
		}

		return $html;
	}

	/**
	 * Creates a white label websites custom post type
	 *
	 * @since     1.0.0
	 * @uses      register_post_type()
	 */
	public function new_cpt_white_label_website() {
		$cap_type = 'post';
		$plural = 'Partner Websites';
		$single = 'Partner Website';
		$opts['can_export'] = TRUE;
		$opts['capability_type'] = $cap_type;
		$opts['description'] = '';
		$opts['exclude_from_search'] = TRUE;
		$opts['has_archive'] = FALSE;
		$opts['hierarchical'] = FALSE;
		$opts['map_meta_cap'] = TRUE;
		$opts['menu_icon'] = 'dashicons-admin-multisite';
		$opts['menu_position'] = 25;
		$opts['public'] = FALSE;
		$opts['publicly_querable'] = FALSE;
		$opts['query_var'] = TRUE;
		$opts['register_meta_box_cb'] = '';
		$opts['rewrite'] = FALSE;
		$opts['show_in_admin_bar'] = TRUE;
		$opts['show_in_menu'] = TRUE;
		$opts['show_in_nav_menu'] = FALSE;
		$opts['show_ui'] = TRUE;
		$opts['supports'] = array( 'title' );
		$opts['taxonomies'] = array();
		$opts['capabilities']['delete_others_posts'] = "delete_others_{$cap_type}s";
		$opts['capabilities']['delete_post'] = "delete_{$cap_type}";
		$opts['capabilities']['delete_posts'] = "delete_{$cap_type}s";
		$opts['capabilities']['delete_private_posts'] = "delete_private_{$cap_type}s";
		$opts['capabilities']['delete_published_posts'] = "delete_published_{$cap_type}s";
		$opts['capabilities']['edit_others_posts'] = "edit_others_{$cap_type}s";
		$opts['capabilities']['edit_post'] = "edit_{$cap_type}";
		$opts['capabilities']['edit_posts'] = "edit_{$cap_type}s";
		$opts['capabilities']['edit_private_posts'] = "edit_private_{$cap_type}s";
		$opts['capabilities']['edit_published_posts'] = "edit_published_{$cap_type}s";
		$opts['capabilities']['publish_posts'] = "publish_{$cap_type}s";
		$opts['capabilities']['read_post'] = "read_{$cap_type}";
		$opts['capabilities']['read_private_posts'] = "read_private_{$cap_type}s";
		$opts['labels']['add_new'] = __( "Add New {$single}", 'balance' );
		$opts['labels']['add_new_item'] = __( "Add New {$single}", 'balance' );
		$opts['labels']['all_items'] = __( $plural, 'balance' );
		$opts['labels']['edit_item'] = __( "Edit {$single}" , 'balance' );
		$opts['labels']['menu_name'] = __( $plural, 'balance' );
		$opts['labels']['name'] = __( $plural, 'balance' );
		$opts['labels']['name_admin_bar'] = __( $single, 'balance' );
		$opts['labels']['new_item'] = __( "New {$single}", 'balance' );
		$opts['labels']['not_found'] = __( "No {$plural} Found", 'balance' );
		$opts['labels']['not_found_in_trash'] = __( "No {$plural} Found in Trash", 'balance' );
		$opts['labels']['parent_item_colon'] = __( "Parent {$plural} :", 'balance' );
		$opts['labels']['search_items'] = __( "Search {$plural}", 'balance' );
		$opts['labels']['singular_name'] = __( $single, 'balance' );
		$opts['labels']['view_item'] = __( "View {$single}", 'balance' );
		register_post_type( 'white_label_website', $opts );

		add_filter( 'manage_edit-white_label_website_columns', array( $this, 'edit_white_label_website_columns' ) );
		add_action( 'manage_white_label_website_posts_custom_column', array( $this, 'manage_white_label_website_column' ), 10, 2 );
	} // new_cpt_white_label_website()

	/**
	 * customizes the columns on edit listing screen
	 *
	 * @since     1.0.0
	 */
	function edit_white_label_website_columns( $columns ) {
		global $post;

		$columns = array(
			'cb' => '',
			'_title' => 'Title',
			'logo' => 'Logo',
			'domain' => 'Domain',
			'page_title' => 'Website Title',
			'description' => 'Description',
			'access' => 'Access',
			'status' => 'Status',
			'date' => 'Date'
		);
		return $columns;
	} // edit_white_label_website_columns()

	/**
	 * display the data for custom columns on edit listing screen
	 *
	 * @since     1.0.0
	 */
	function manage_white_label_website_column( $column, $post_id ) {
		global $post;

		$data = get_post_custom( $post_id );
		$unserialized_data = '';

		if ( isset( $data['_page_edit_data'] ) ) {
			$unserialized_data = @unserialize( $data['_page_edit_data'][0] );
		}
		if ( $unserialized_data === false ) {
			$unserialized_data = array();
		}

		$data = $unserialized_data;

		$domain = !empty( $data['wlw_general_info_module'][0]['domain'] ) ? '<a href="' . ( strpos( $data['wlw_general_info_module'][0]['domain'], "//" ) === false ? "//" : "" ) . $data['wlw_general_info_module'][0]['domain'] . '" target="_blank">%s</a>' : '—';
		$title = !empty( $data['wlw_general_info_module'][0]['title'] ) ? $data['wlw_general_info_module'][0]['title'] : '—';
		$description = !empty( $data['wlw_general_info_module'][0]['description'] ) ? $data['wlw_general_info_module'][0]['description'] : '—';
		$logo = !empty( $data['wlw_general_info_module'][0]['logo']['image']['attachment_id'] ) ? '<img style="width:50px;"  src="' . wp_get_attachment_thumb_url( $data['wlw_general_info_module'][0]['logo']['image']['attachment_id'] ) . '">' : '—';
		$access_groups = !empty( $data['wlw_resources_module'][0]['accesstoresources'] ) ? $data['wlw_resources_module'][0]['accesstoresources'] : '—';

		switch ( $column ) {
		case '_title' :
			$output = '';
			$output.= '<strong>';
			$output.= '	<a class="row-title" href="'. get_edit_post_link( $post->ID )   . '" title="Edit “' . $post->post_title . '”">' . $post->post_title . '</a>';
			$output.= '</strong>';
			$output.= '<div class="row-actions">';
			switch ( $post->post_status ) {
			case 'trash':
				$output.= '	<span class="restore">';
				$output.= '		<a title="Restore this item from the Trash" href="' . wp_nonce_url( admin_url( 'post.php?post=' . $post->ID . '&amp;action=untrash' ), 'untrash-post_'. $post->ID ) . '">Restore</a> | ';
				$output.= '	</span>';
				$output.= '	<span class="delete">';
				$output.= '		<a class="submitdelete" title="Delete this item permanently" href="' . get_delete_post_link( $post->ID, '', true ) . '">Delete Permanently</a>';
				$output.= '	</span>';
				break;
			default:
				$output.= '	<span class="edit">';
				$output.= '		<a href="'. get_edit_post_link( $post->ID )  . '" title="Edit this item">Edit</a> | ';
				$output.= '	</span>';
				if ( $domain != '—' ) {
					$output.= '	<span class="edit visit">';
					$output.= sprintf( $domain, 'Visit site' ) . ' | ';
					$output.= '	</span>';
				}
				$output.= '	<span class="trash">';
				$output.= '		<a class="submitdelete" title="Move this item to the Trash" href="' . get_delete_post_link( $post->ID ) . '">Trash</a>';
				$output.= '	</span>';
				break;
			}
			$output.= '</div>';
			echo $output;
			break;
		case 'logo' :
			echo  $logo;
			break;
		case 'domain' :
			if ( $domain != '—' ) {
				echo  sprintf( $domain, $data['wlw_general_info_module'][0]['domain'] );
			} else {
				echo $domain;
			}
			break;
		case 'page_title' :
			echo  $title;
			break;
		case 'description' :
			echo  $description;
			break;
		case 'access' :
			if ( $access_groups != '—' ) {
				$access_groups = explode( ',', $access_groups );
				$access_groups_links = '';
				foreach ( $access_groups as $access_group ) {
					if ( !empty( $access_group ) ) {
						$access_groups_links .= '<a href="' . get_edit_post_link( $access_group ) . '">' . get_the_title( $access_group ) . '</a>, ';
					}
				}
				echo substr( $access_groups_links, 0 , -2 );
			} else {
				echo $access_groups;
			}
			break;
		case 'status' :
			switch ( $post->post_status ) {
			case  'trash':
				echo '<span style="color:red" class="dashicons dashicons-no" title="' . ucfirst( $post->post_status ) . 'ed"></span>';
				break;
			case 'auto-draft':
			case 'draft':
				echo '<span style="color:orange" class="dashicons dashicons-update" title="' . ucfirst( $post->post_status ) . 'ed"></span>';
				break;
			case 'publish':
				echo '<span style="color:green" class="dashicons dashicons-yes" title="' . ucfirst( $post->post_status ) . 'ed"></span>';
				break;
			}
			break;
		}
	} // manage_cpts_resources_column()
}
