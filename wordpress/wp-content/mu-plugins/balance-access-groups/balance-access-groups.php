<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://roidna.com
 * @since             1.0.0
 * @package           Balance_Access_Groups
 *
 * @wordpress-plugin
 * Plugin Name:       Balance Access Groups
 * Plugin URI:        #
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            ROIDNA
 * Author URI:        http://roidna.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       balance-access-groups
 * Domain Path:       /languages
 */



// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-balance-access-groups-activator.php
 */
function activate_balance_access_groups() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-balance-access-groups-activator.php';
	Balance_Access_Groups_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-balance-access-groups-deactivator.php
 */
function deactivate_balance_access_groups() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-balance-access-groups-deactivator.php';
	Balance_Access_Groups_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_balance_access_groups' );
register_deactivation_hook( __FILE__, 'deactivate_balance_access_groups' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-balance-access-groups.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
 

	
function run_balance_access_groups() {
	$plugin = new Balance_Access_Groups();
	$plugin->run();
}

run_balance_access_groups();

