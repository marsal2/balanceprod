<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/includes
 * @author     ROIDNA <devs@roidna.com>
 */
class Balance_Access_Groups_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
