


  KJE.parameters.set("GROSS_EARNINGS",1000);
  KJE.parameters.set("RETIRE_PLAN_PERCENT",5.0);
  KJE.parameters.set("STATE_TAX",6);
  KJE.parameters.set("WITHHOLD_COUNT",2);

KJE.parameters.set("MSG_CATEGORIES1","FSA Deduction");
KJE.parameters.set("MSG_CATEGORIES2","Non deductible");
KJE.parameters.set("MSG_CATEGORIES3","Post-tax reimbursements");
KJE.parameters.set("MSG_CATEGORIES4","FICA OASDI");
KJE.parameters.set("MSG_CATEGORIES5","FICA Medicare");
KJE.parameters.set("MSG_CATEGORIES6","Federal tax");
KJE.parameters.set("MSG_CATEGORIES7","State & local tax");
KJE.parameters.set("MSG_CATEGORIES8","Net income");
KJE.parameters.set("MSG_CATEGORIES9","401(k)/403(b) plan");




/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


