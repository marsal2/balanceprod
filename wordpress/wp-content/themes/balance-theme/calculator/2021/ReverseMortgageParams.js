


KJE.parameters.set("STARTING_AMOUNT",KJE.Default.MortgageAmt);
KJE.parameters.set("RATE_OF_RETURN",KJE.Default.RateFix30);
KJE.parameters.set("ADDITIONAL_CONTRIBUTIONS",0);
KJE.parameters.set("YEARS",10);
KJE.parameters.set("COMPOUND_INTEREST",KJE.CompoundSavingsCalc.COMPOUND_MONTHLY);
KJE.parameters.set("DEPOSIT_FREQUENCY",KJE.CompoundSavingsCalc.DEPOSIT_MONTHLY);


/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


