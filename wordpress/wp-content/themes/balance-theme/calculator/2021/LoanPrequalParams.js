


  KJE.parameters.set("CREDIT_CARD_LIMIT",10000);
  KJE.parameters.set("DEPENDANTS",0);
  KJE.parameters.set("INCOME_AMOUNT",50000);
  KJE.parameters.set("INCOME_SOURCES",1);
  KJE.parameters.set("LIABILITY",3000);
  KJE.parameters.set("NEW_LOAN_BALANCE",200000);
  KJE.parameters.set("NEW_LOAN_RATE",KJE.Default.RatePersonal);
  KJE.parameters.set("NEW_LOAN_TERM",10);
  KJE.parameters.set("REAL_ESTATE_SECURITY",250000);
  KJE.parameters.set("GRAPH_RESULT_COLOR1","#EB3013");
  KJE.parameters.set("GRAPH_RESULT_COLOR2","#EB3013");
  KJE.parameters.set("GRAPH_RESULT_COLOR3","#EB3013");
  KJE.parameters.set("GRAPH_RESULT_COLOR4","#FFC957");
  KJE.parameters.set("GRAPH_RESULT_COLOR5","#FFC957");
  KJE.parameters.set("GRAPH_RESULT_COLOR6","#2BD07E");
  KJE.parameters.set("GRAPH_RESULT_COLOR7","#2BD07E");



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


