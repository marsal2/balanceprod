<?php
/*
Modules: {"m56b[0]":{"name":"M56b: Program Settings"},"m1[0]":{"name":"M1: Hero"},"m38[0]":{"name":"M38: Section","params":{"media_buttons":true,"quicktags":true}}}
*/
global $additional_body_class, $post, $data;
$additional_body_class = 'single-program';

get_custom_data();

$visible = get_post_meta($data['post_ID'])['meta-box-checkbox'][0];

if( !empty($visible) && !current_user_can('edit_post') ){ 
  require('404.php');
} else if( !empty($data['m56b_module']) && !empty($data['m56b_module'][0]) && !empty($data['m56b_module'][0]['program_type']) ){
  if($data['m56b_module'][0]['program_type'] == 'link_to_old_site') {
    header('Location: '.$data['m56b_module'][0]['link_to_old_site']);
    exit();
  }else{
    get_header();
    if( $data['m56b_module'][0]['program_type'] == 'resource_as_program' ) {
      get_template_part('partials/single-page-program');
    }else{
      if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
        echo render_m1_hero( $data['m1_module'][0] );
      }
      get_template_part('partials/program');
    }
    get_footer();
  }
}else{
  require('404.php');
}