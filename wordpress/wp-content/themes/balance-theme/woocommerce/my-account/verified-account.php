<?php
$my_account_page_permalink = get_the_permalink ( get_option( 'woocommerce_myaccount_page_id' ) );
?>
<ul role="tablist" class="nav nav-tabs">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="icon-user"></span><?php _e( 'Account Verification', 'balance' ); ?></a></li>
</ul>
<div class="tab-content">
	<?php  if ( isset( $_GET['status'] ) && $_GET['status'] == 'success' ) { ?>
	<div class="woocommerce-message woocommerce-message-success"><?php echo __( 'Your account has been verified and activated. You will be redirected to login screen in... ', 'woocommerce' ) . '<b><span class="counter">4</span>s</b>'; ?></div>
	<script>
		jQuery(document).ready(function() {
			counter = 3;
			setInterval(function() {
				jQuery('.counter').html(counter);
				counter--;
				if (counter == 0) {
					window.location = '<?php echo $my_account_page_permalink; ?>?action=login';
				}
			}, 1000);
		});
	</script>
	<?php } else { ?>
	<script>
		window.location = '<?php echo $my_account_page_permalink; ?>?action=login';
	</script>
	<?php } ?>
	<br>
</div>
