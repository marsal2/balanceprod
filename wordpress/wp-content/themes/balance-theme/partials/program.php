<?php
global $data, $post;

if ( !empty( $data['m38_module'] ) && !empty( $data['m38_module'][0] ) ) {
  echo render_m38_section_copy( $data['m38_module'][0] );
}
//If on program
if ( !empty( $data['m56b_module'] ) && !empty( $data['m56b_module'][0] ) && !empty( $data['m56b_module'][0]['program_resources']) ) {
  echo render_m56b_program_sections( $data['m56b_module'][0]['program_resources'] );
}
//If on toolkit
if ( !empty( $data['resourcesections_module'] ) && !empty( $data['resourcesections_module'][0] ) && !empty( $data['resourcesections_module'][0]['sections']) ) {
  echo render_m56b_program_sections( $data['resourcesections_module'][0]['sections'] );
}