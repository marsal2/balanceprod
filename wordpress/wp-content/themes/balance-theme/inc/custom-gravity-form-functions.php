<?php

// customize the whole form object before the markup logic is applied to it
add_filter( 'gform_pre_render', 'custom_gravity_form_class' );
function custom_gravity_form_class( $form ) {
  // do not fire in administration
  if ( is_admin() ) {
    return $form;
  }
  // add additional form class for Balance in form
  // used separately becuase the Gravity Forms do not have any class on default (cannot use str_replace directly)
  $form['cssClass'] = 'form-wrap' . ( !empty($form['cssClass']) ? $form['cssClass'] : '' );
  return $form;
}

// customize the whole form string before is outputted
add_filter( 'gform_get_form_filter', 'custom_gravity_form_contents_classess', 10, 2 );
function custom_gravity_form_contents_classess( $form_string, $form ) {
  // do not fire in administration
  if ( is_admin() ) {
    return $form_string;
  }
  // add additional classess specific for Balance in forms
  $form_string = str_replace('gform_fields', 'gform_fields input-group', $form_string);
  $form_string = str_replace('gform_body', 'gform_body input-group-frame', $form_string);
  return $form_string;
}

// customize the wrapper of the fields (default is <li> element) in gravity forms (apply custom HTML for Balance)
add_filter( 'gform_field_container', 'my_field_container', 10, 6 );
function my_field_container( $field_container, $field, $form, $css_class, $style, $field_content ) {
  // do not fire in administration
  if ( is_admin() ) {
    return $field_container;
  }
  switch ($field->type) {
    case 'section':
      // create a form section by closing the current unordered list (1), adding section content (2) and opening new form section after it (3)
      $field_container = '</ul>'; // (1)
      if ( count( $form['pagination']['pages'] ) > 0 ) {
        $field_container .= '</div>';
      }
      $field_container .= '{FIELD_CONTENT}'; // (2)
      if ( count( $form['pagination']['pages'] ) > 0 ) {
        $field_container .= '<div class="gform_page_fields">';
      }
      $field_container .= '<ul class="gform_fields input-group">'; // (3)
      break;
    case 'list':
    case 'table':
      $field_container = '<li id="field_' . $form['id'] . '_' . $field->id . '" class="list-wrap">{FIELD_CONTENT}</li>';
      break;
    default:
      break;
  }
  return $field_container;
}

// customize the contents of the fields in gravity forms (apply custom HTML for Balance)
// add_filter( 'gform_field_input', 'tracker', 10, 5 );
// function tracker( $input, $field, $value, $lead_id, $form_id ) {
//   // because this will fire for every form/field, only do it when it is the specific form and field
//   if ( $form_id == 23 && $field->id == 11 ) {
//       $input = '<input type="hidden" id="hidTracker" name="hidTracker" value="test">';
//   }
//   return $input;
// }

// customize the contents of the fields in gravity forms (apply custom HTML for Balance)
add_filter( 'gform_field_content', 'custom_gform_fields_contents', 10, 5 );
function custom_gform_fields_contents( $content, $field, $value, $lead_id, $form_id ) {
  // do not fire in administration
  if ( is_admin() && $field->type != 'table' ) {
    return $content;
  }

  switch ($field->type) {
    case 'text':
    case 'phone':
    case 'website':
    case 'number':
    case 'textarea':
    case 'date':
      $content = render_m70_m84_form_text_field( $field, $form_id, $value );
      break;
    case 'select':
      $content = render_m81_form_select_field( $field, $form_id, $value );
      break;
    case 'email':
      $content = render_m85_form_email_field( $field, $form_id, $value );
      break;
    case 'radio':
    case 'checkbox':
      $content = render_m82_m83_form_checkboxes_radiobuttons_list_field( $field, $form_id, $value );
      break;
    case 'section':
      // read the field data and add them to the module object (used in renderer)
      $module_data = array(
        'title' => !empty( $field['label'] ) ? $field['label'] : '',
        'copy' => !empty( $field['description'] ) ? $field['description'] : '',
      );
      $section_id = 'field_' . $form_id . '_' . $field['id'];
      // reuse M34 renderer to output the section in the form
      $css = !empty( $field->cssClass ) ? ' ' . $field->cssClass : '';
      $content = render_m34_article_content( $module_data, $section_id, true, 'article default-content-style' . $css, 'text-center');
      break;
    case 'fileupload':
      $content = render_m89_form_upload_field( $field, $form_id, $value );
      break;
    case 'captcha':
      $content = render_m90_form_captcha_field( $field, $form_id, $value );
      break;
    case 'table':
      if ( $field->is_form_editor() ) { // fire on the form editor screen

        $form = GFAPI::get_form( $form_id );

        $admin_buttons = $field->get_admin_buttons();
        $field_label = $field->get_field_label( $form, $value );
        $required_div = sprintf( "<span class='gfield_required'>%s</span>", $field->isRequired ? '*' : '' );
        $description = $field->get_description( $field->description, 'gfield_description' );
        $validation_message = ( $field->failed_validation && ! empty( $field->validation_message ) ) ? sprintf( "<div class='gfield_description validation_message'>%s</div>", $field->validation_message ) : '';

        // output the preview of the table from the editor
        $field_admin_content = '<table class="gfield_table gfield_table_container gfield_list gfield_list_container">';
        $field_admin_content .= '  <thead>';

        $field_admin_content .= '    <tr>';
        $field_admin_content .= '      <th>&nbsp;</th>';
        if ( !empty( $field['column_choices'] ) ) {
          foreach ($field['column_choices'] as $column_choice) {
            $field_admin_content .= '    <th>' . $column_choice['text'] . '</th>';
          }
        } else {
          $field_admin_content .= '      <th>' . __( 'Column 1', 'balance' ) . '</th>';
          $field_admin_content .= '      <th>' . __( 'Column 2', 'balance' ) . '</th>';
          $field_admin_content .= '      <th>' . __( 'Column 3', 'balance' ) . '</th>';
        }

        $field_admin_content .= '    </tr>';
        $field_admin_content .= '  </thead>';
        $field_admin_content .= '  <tbody>';

        if ( !empty( $field['column_choices'] ) ) {
          foreach ($field['row_choices'] as $row_choice) {
            $field_admin_content .= '    <tr>';
            $field_admin_content .= '      <td>' . $row_choice['text'] . '</td>';
            foreach ($field['column_choices'] as $column_choice) {
              $field_admin_content .= '      <td><input type="text" disabled="disabled" placeholder="' . ( !empty( $column_choice['price'] ) ? $column_choice['price'] : '' ) . '"></td>';
            }
            $field_admin_content .= '      </tr>';
          }
        } else {
          $field_admin_content .= '    <tr>';
          $field_admin_content .= '      <td>' . __( 'Row 1', 'balance' ) . '</td>';
          $field_admin_content .= '      <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '      <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '      <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '      </tr>';
          $field_admin_content .= '      <tr>';
          $field_admin_content .= '        <td>' . __( 'Row 2', 'balance' ) . '</td>';
          $field_admin_content .= '        <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '        <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '        <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '      </tr>';
          $field_admin_content .= '      <tr>';
          $field_admin_content .= '        <td>' . __( 'Row 3', 'balance' ) . '</td>';
          $field_admin_content .= '        <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '        <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '        <td><input type="text" disabled="disabled"></td>';
          $field_admin_content .= '      </tr>';
        }
        $field_admin_content .= '  </tbody>';
        $field_admin_content .= '</table>';

        $field_content = sprintf( "%s<label class='gfield_label'>%s%s</label><div class='ginput_container ginput_container_table ginput_table'>%s</div>%s%s", $admin_buttons, esc_html( $field_label ), $required_div, $field_admin_content, $description, $validation_message );

        $content = $field_content;
      } else if ( $field->is_entry_detail() ) {
        $mode = empty( $_POST['screen_mode'] ) ? 'view' : $_POST['screen_mode'];
        if ( $mode == 'view' ) {
          $content = $content;
        } else {
          $content = build_table_entry_detail_markup( $value, $field, 'edit' );
        }
      } else {
        $content = render_m73_m74_m75_m77_m78_form_table_field( $field, $form_id, $value );
      }
      break;
    default:
      break;
  }
  return $content;
}

// customize the available fields in administration for gravity forms, rmeove post and pricing fields
add_filter( 'gform_add_field_buttons', 'remove_and_add_gravity_forms_fields' );
function remove_and_add_gravity_forms_fields( $field_groups ) {
  $index = 0;
  $standard_field_index = -1;
  $advanced_field_index = -1;
  $post_field_index = -1;
  $pricing_field_index = -1;

  //finding group indexes
  foreach ( $field_groups as $group ) {
    if ( $group['name'] == 'post_fields' ) {
        $post_field_index = $index;
    } elseif ( $group['name'] == 'pricing_fields' ) {
        $pricing_field_index = $index;
    } elseif ( $group['name'] == 'advanced_fields' ) {
        $advanced_field_index = $index;
    } elseif ( $group['name'] == 'standard_fields' ) {
        $standard_field_index = $index;
    }

    $index ++;
  }

  //remove entire post field and pricing field group
  if ( $post_field_index >= 0 ) {
    unset( $field_groups[ $post_field_index ] );
  }
  if ( $pricing_field_index >= 0 ) {
    unset( $field_groups[ $pricing_field_index ] );
  }

  //remove time, Address and Name field
  if ( $advanced_field_index >= 0 ) {
    $time_index = -1;
    $address_index = -1;
    $name_index = -1;

    $index = 0;
    foreach ( $field_groups[ $advanced_field_index ]['fields'] as $advanced_field ) {
      if ( $advanced_field['value'] == 'Time' ) {
          $time_index = $index;
      } else if ( $advanced_field['value'] == 'Name' ) {
        $name_index = $index;
      } else if ( $advanced_field['value'] == 'Address' ) {
        $address_index = $index;
      }
      $index ++;
    }

    unset( $field_groups[ $advanced_field_index ]['fields'][ $time_index ] );
    unset( $field_groups[ $advanced_field_index ]['fields'][ $name_index ] );
    unset( $field_groups[ $advanced_field_index ]['fields'][ $address_index ] );
  }

  //remove HTML and multiselect field
  if ( $standard_field_index >= 0 ) {
    $HTML_index = -1;
    $multiselect_index = -1;

    $index = 0;
    foreach ( $field_groups[ $standard_field_index ]['fields'] as $standard_field ) {
      if ( $standard_field['value'] == 'Multi Select' ) {
        $multiselect_index = $index;
      }
      $index ++;
    }
    unset( $field_groups[ $standard_field_index ]['fields'][ $multiselect_index ] );
  }

  // add Table Custom Field to Gravity Forms
  foreach( $field_groups as &$group ){
    if( $group['name'] == 'advanced_fields' ) { // to add to the Advanced Fields
      $group['fields'][] = array(
        'class' => 'button',
        'data-type' => 'table',
        'value' => __('Table', 'balance'),
        'label' => __('Table', 'balance'),
        "onclick" => "StartAddField('table');"
      );
    }
  }
  return $field_groups;
}


add_action( 'admin_head', 'enqueue_form_editor_style' );
function enqueue_form_editor_style(){
  if ( RGForms::is_gravity_page() ) {
    //enqueueing admin custom styles on gravity form pages
    wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/css/admin/admin.css', false, '0.1', 'all' );
  }
}

add_filter( 'gform_noconflict_styles', 'register_style' );
function register_style( $styles ) {
  //registering my style with Gravity Forms so that it gets enqueued when running on no-conflict mode
  $styles[] = 'admin-styles';
  return $styles;
}

// remove conflicting css styles for datepicker field
add_action( 'gform_enqueue_scripts', 'dequeue_gf_stylesheets_add_custom_js', 11 );
function dequeue_gf_stylesheets_add_custom_js() {
  wp_dequeue_style( 'gforms_datepicker_css' );
  ?>
  <script>jQuery(document).ready(function() { jQuery("h1:visible:gt(0)").addClass("not-first-title"); });</script>
  <?php
}

// run the cpatcha verification through API (replacement of captcha for recaptcha)
add_filter( 'gform_field_validation', 'gravity_forms_captcha_validate', 10, 4 );
function gravity_forms_captcha_validate( $result, $value, $form, $field ) {
  if ( $field['type'] == 'captcha' ) {
    // Check to ensure POST challenge is included
    if ( isset( $_POST['g-recaptcha-response'] ) && ! empty( $_POST['g-recaptcha-response'] ) ) {
      $recaptcha_response = urlencode( $_POST['g-recaptcha-response'] );
      $user_ip            = $_SERVER['REMOTE_ADDR'];
      $verify_url         = 'https://www.google.com/recaptcha/api/siteverify?secret=6LdqjBkTAAAAAHi_-ec2PiVJrJC_gqC8rNOMGcoz&response=' . $recaptcha_response . '&remoteip=' . $user_ip;
      $json_response      = file_get_contents( $verify_url );
      if( ! empty( $json_response ) && $result = json_decode( $json_response, true ) ) {
        if( isset( $result['success'] ) && $result['success'] == true ) {
          $result['is_valid'] = true;
          return $result;
        }
      }
    }
    $result['message']  = __( 'CAPTCHA failed. Please try again!', 'gf-no-captcha-recaptcha' );
    $result['is_valid'] = false;
  }
  return $result;
}

// Adds title to GF custom field
add_filter( 'gform_field_type_title' , 'gravity_forms_custom_field_table_title', 999, 2 );
function gravity_forms_custom_field_table_title( $title, $type ) {
  if ( $type == 'table' ) {
    $title = 'Table';
  }
  return $title;
}

// additional javascript for the table custom field (for the form editor screen in administration)
add_action( "gform_editor_js", "gravity_forms_table_custom_field_editor_js" );
function gravity_forms_table_custom_field_editor_js(){
?>
  <script type='text/javascript'>
    jQuery(document).ready(function($) {
      // from forms.js; can add custom setting as well
      fieldSettings["table"] = ".label_setting, .description_setting, .table_columns_setting, .table_rows_setting, .rules_setting, .admin_label_setting, .error_message_setting, .css_class_setting"; //this will show all the fields of the Paragraph Text field minus a couple that I didn't want to appear.
      // attach the sorting to the table field columns and rows setting
      jQuery('#table_columns, #table_rows').sortable({
        axis: 'y',
        handle: '.field-choice-handle',
        update: function(event, ui){
          var fromIndex = ui.item.data("index");
          var toIndex = ui.item.index();
          var tableFieldType = ui.item.data("table_field_type");
          MoveTableFieldChoice(fromIndex, toIndex, tableFieldType);
        }
      });

      //binding to the load field settings event to initialize the table field
      $(document).bind("gform_load_field_settings", function(event, field, form) {
        field = GetSelectedField();
        if ( field.type == 'table' ) {
          LoadTableFieldChoices( field );
          UpdateTableFieldChoices();
        }
      });
      // on settings input changes update the whole table field and preview in the editor
      jQuery('.table_columns_setting, .table_rows_setting')
        .on('input propertychange', '.field-choice-column-input, .field-choice-row-input', function(e){
          var $this = jQuery(this);
          var tableFieldType = '';
          if ( $this.attr('class').indexOf('column') > -1 ) {
            tableFieldType = 'column';
          } else {
            tableFieldType = 'row';
          }
          var li = $this.closest('li.field-choice-row');
          var inputType = li.data('input_type');
          var i = li.data('index');
          SetTableFieldChoice( inputType, i, tableFieldType);

        });

    });
    //
    function LoadTableFieldChoices( field ) {
      jQuery("#table_columns").html(GetTableFieldChoices(field,'column'));
      jQuery("#table_rows").html(GetTableFieldChoices(field,'row'));
    }

    function UpdateTableFieldChoices(){
      field = GetSelectedField();
      field.inputs = new Array();
      var choices = '';
      var selector = '';

      var skip = 0;
      columns = field["column_choices"];
      rows = field["row_choices"];
      var class_attr = "";
        choices += "<thead><tr><th>&nbsp;</th>";
        for(var i=0; i<columns.length; i++){
            choices += "<th>" + columns[i]["text"] + "</th>";
        }
        choices += "</tr></thead>";

      choices += "<tbody>";
      var count_inputs = 0;
      for(var z=0; z<rows.length; z++){
        choices += "<tr>";
        choices += "<td>" + rows[z]["text"] + "</td>";
        for(var j=0; j<columns.length; j++){
          placeholder = (columns[j]["price"] != '') ? columns[j]["price"] : '';
          choices += "<td " + class_attr +"><input type='text' disabled='disabled' placeholder='" + placeholder + "' /></td>";
          field.inputs.push(new Input(field.id + '.' + count_inputs, ''));
          count_inputs++;
        }
        choices +="</tr>";
        if (rows[z]["price"] != '') {
          choices += "<tr>";
          choices += "<td colspan='" + (columns.length+1) +"'>" + rows[z]["price"] + "</td>";
          choices +="</tr>";
        }
      }
      choices += "</tbody>";


      selector = '.gfield_table';
      jQuery(".field_selected " + selector).html(choices);
    }

    function GetTableFieldChoices(field, tableFieldType) {
      if ( typeof field[tableFieldType + '_choices'] == 'undefined' ) {
        return "";
      }

      var str = "";
      for (var i = 0; i < field[tableFieldType + '_choices'].length; i++) {
        var inputType = GetInputType(field);
        var text = (typeof field[tableFieldType + '_choices'][i].text !== 'undefined' ) ? field[tableFieldType + '_choices'][i].text.replace(/"/g, "&quot;") : '';
        var description = (typeof field[tableFieldType + '_choices'][i].price !== 'undefined' ) ? field[tableFieldType + '_choices'][i].price.replace(/"/g, "&quot;") : '';
        var placeholder = (typeof field[tableFieldType + '_choices'][i].price !== 'undefined' ) ? field[tableFieldType + '_choices'][i].price.replace(/"/g, "&quot;") : '';

        str += "<li class='field-choice-row' data-table_field_type='" + tableFieldType + "' data-input_type='" + inputType + "' data-index='" + i + "'>";
        str += "<i class='fa fa-sort field-choice-handle'></i> ";
        str += "<input type='text' id='" + inputType + "_choice_text_" + i + "_" + tableFieldType + "' value=\"" + text + "\" class='field-choice-input field-choice-" + tableFieldType + "-input field-choice-text' />";

        if (window["gform_append_field_choice_option_" + field.type]) {
          str += window["gform_append_field_choice_option_" + field.type](field, i);
        }

        str += gform.applyFilters('gform_append_field_choice_option', '', field, i);

        str += "<a class='gf_insert_field_choice' onclick=\"InsertTableFieldChoice(" + (i + 1) + ",'" + tableFieldType + "');\"><i class='gficon-add'></i></a>";

        if (field[tableFieldType + '_choices'].length > 1){
          str += "<a class='gf_delete_field_choice' onclick=\"DeleteTableFieldChoice(" + i + ",'" + tableFieldType + "');\"><i class='gficon-subtract'></i></a>";
        }
        if ( tableFieldType == 'row' ) {
          str += "<textarea id='" + inputType + "_choice_description_" + i + "_" + tableFieldType + "' class='field-choice-input field-choice-" + tableFieldType + "-input field-choice-text' style='display:block;width:96%;margin-top:5px;margin-left:12px;' placeholder='<?php _e( 'Description', 'balance' ); ?>'>" + description + "</textarea>";
        } else {
          str += "<input type='text' id='" + inputType + "_choice_placeholder_" + i + "_" + tableFieldType + "' value=\"" + placeholder + "\" class='field-choice-input field-choice-" + tableFieldType + "-input field-choice-text' style='display:block;margin-top:5px;margin-left:12px;' placeholder='<?php _e( 'Placeholder', 'balance' ); ?>' />";
        }
        str += "</li>";

      }
      return str;
    }

    function MoveTableFieldChoice( fromIndex, toIndex, tableFieldType ){
      field = GetSelectedField();
      var choice = field[tableFieldType + '_choices'][fromIndex];

      //deleting from old position
      field[tableFieldType + '_choices'].splice(fromIndex, 1);

      //inserting into new position
      field[tableFieldType + '_choices'].splice(toIndex, 0, choice);

      LoadTableFieldChoices(field);
      UpdateTableFieldChoices();
    }

    function SetTableFieldChoice(inputType, index, tableFieldType){
      field = GetSelectedField();
      var text = jQuery("#" + inputType + "_choice_text_" + index + '_' + tableFieldType).val();
      field[tableFieldType + '_choices'][index].text = text;
      field[tableFieldType + '_choices'][index].value = text;
      if ( tableFieldType == 'row' ) {
        var description = jQuery("#" + inputType + "_choice_description_" + index + '_' + tableFieldType).val();
        field[tableFieldType + '_choices'][index].price = description;
      } else {
        var placeholder = jQuery("#" + inputType + "_choice_placeholder_" + index + '_' + tableFieldType).val();
        field[tableFieldType + '_choices'][index].price = placeholder;
      }
      UpdateTableFieldChoices();
    }

    function InsertTableFieldChoice(index, tableFieldType){
      field = GetSelectedField();
      var new_choice = new Choice("<?php _e('Untitled', 'balance'); ?>", "", "");
      field[tableFieldType + '_choices'].splice(index, 0, new_choice);
      LoadTableFieldChoices(field);
      UpdateTableFieldChoices();
    }

    function DeleteTableFieldChoice(index, tableFieldType){
      field = GetSelectedField();
      field[tableFieldType + '_choices'].splice(index, 1);
      LoadTableFieldChoices(field);
      UpdateTableFieldChoices();
    }
  </script>
<?php
}

// hook into the field attributes setter and add the case for the custom table field
add_action( 'gform_editor_js_set_default_values', 'gravityforms_table_custom_field_set_defaults' );
function gravityforms_table_custom_field_set_defaults() {
  ?>
  case "table" :
    field.label = <?php echo json_encode( esc_html__( 'Table', 'balance' ) ); ?>;
    field.column_choices = new Array(new Choice(<?php echo json_encode( esc_html__( 'Column 1', 'balance' ) ); ?>), new Choice(<?php echo json_encode( esc_html__( 'Column 2', 'balance' ) ); ?>), new Choice(<?php echo json_encode( esc_html__( 'Column 3', 'balance' ) ); ?>));
    field.row_choices = new Array(new Choice(<?php echo json_encode( esc_html__( 'Row 1', 'balance' ) ); ?>), new Choice(<?php echo json_encode( esc_html__( 'Row 2', 'balance' ) ); ?>), new Choice(<?php echo json_encode( esc_html__( 'Row 3', 'balance' ) ); ?>));
    field.inputs = null;
  break;
  <?php
}

// Add a custom setting to the table advanced field (2, for table field rows and columns)
add_action( "gform_field_standard_settings" , "gravityforms_table_custom_field_custom_settings" , 10, 2 );
function gravityforms_table_custom_field_custom_settings( $position, $form_id ){
  if ( $position == 25 ) {
  ?>
    <li class="table_columns_setting field_setting">
      <label>
        <?php esc_html_e( 'Columns', 'balance' ); ?>
        <?php gform_tooltip( 'form_field_table_columns' ) ?>
      </label>
      <div id="gfield_settings_table_columns_container">
        <ul id="table_columns"></ul>
      </div>
    </li>
    <li class="table_rows_setting field_setting">
      <label>
        <?php esc_html_e( 'Rows', 'balance' ); ?>
        <?php gform_tooltip( 'form_field_table_rows' ) ?>
      </label>
      <div id="gfield_settings_table_rows_container">
        <ul id="table_rows"></ul>
      </div>
    </li>
  <?php
  }
}

// set tooltip value for the columns and rows custom field settings
add_filter( 'gform_tooltips', 'gravityforms_table_custom_field_custom_settings_columns_rows_tooltips' );
function gravityforms_table_custom_field_custom_settings_columns_rows_tooltips( $tooltips ) {
  $tooltips['form_field_table_columns'] = __( '<h6>Columns</h6>Add/reorder desired number of columns. Additionally to column title you can set placeholder text for each input in that column.', 'balance' );
  $tooltips['form_field_table_rows'] = __( '<h6>Rows</h6>Add/reorder desired number of rows. Additionally to row title you can set description for each row.', 'balance' );
  return $tooltips;
}
// for local testing purposes only, disables the sending of emails
// add_filter( 'gform_disable_notification', 'disable_notification', 10, 4 );
// function disable_notification( $is_disabled, $notification, $form, $entry ) {
//     return true;
// }

// modify the table field entry screen content (display array of data as table)
add_filter( 'gform_entry_field_value', 'gravityforms_table_custom_field_entry_field_value', 10, 4 );
function gravityforms_table_custom_field_entry_field_value( $value, $field, $lead, $form ) {
  if ( $field['type'] == 'table' ) {
    $new_value = '';
    $new_value = build_table_entry_detail_markup( $value, $field, 'view' );

    return $new_value;
  }
  return $value;
}

function build_table_entry_detail_markup( $value, $field, $mode ) {
  $output = '<table class="gfield_table gfield_table_container gfield_list gfield_list_container">';
  $output .= '  <thead>';
  if ( $mode == 'edit' ) {
    $output .= '    <tr><td colspan="' . (count($field['column_choices'])+1) . '">' . $field['label'] . '</td></tr>';
  }
  $output .= '    <tr>';
  $output .= '      <th>&nbsp;</th>';
  if ( !empty( $field['column_choices'] ) ) {
    foreach ($field['column_choices'] as $column_choice) {
      $output .= '    <th>' . $column_choice['text'] . '</th>';
    }
  }

  $output .= '    </tr>';
  $output .= '  </thead>';
  $output .= '  <tbody>';
  $count_inputs = 0;
  if ( !empty( $field['column_choices'] ) ) {
    foreach ($field['row_choices'] as $row_choice) {
      $output .= '    <tr>';
      $output .= '      <td>' . $row_choice['text'] . '</td>';
      foreach ($field['column_choices'] as $column_choice) {
        if ( $count_inputs == 0 && !empty( $value[$field['id']] ) ) {
          $curr_input_value = $value[$field['id']];
        } else {
          $curr_input_value = !empty( $value[$field['id'].'.'.$count_inputs] ) ? $value[$field['id'].'.'.$count_inputs] : '&nbsp;';
        }
        $output .= '    <td' . ( ( $curr_input_value[0] == '$' ) ? ' style="text-align:center;"' : '' ) . '>' . ( $mode == 'edit' ? '<input type="text" disabled="disabled" value="' . $curr_input_value . '">' : $curr_input_value ) . '</td>';
        $count_inputs++;
      }
      $output .= '    </tr>';
    }
  }
  $output .= '  </tbody>';
  $output .= '</table>';
  return $output;
}

class GWPreviewConfirmation {
    private static $lead;
    public static function init() {
        add_filter( 'gform_pre_render', array( __class__, 'replace_merge_tags' ) );
    }
    public static function replace_merge_tags( $form ) {
        $current_page = isset(GFFormDisplay::$submission[$form['id']]) ? GFFormDisplay::$submission[$form['id']]['page_number'] : 1;
        $fields = array();
        // get all HTML fields on the current page
        foreach($form['fields'] as &$field) {
            // skip all fields on the first page
            if(rgar($field, 'pageNumber') <= 1)
                continue;
            $default_value = rgar($field, 'defaultValue');
            preg_match_all('/{.+}/', $default_value, $matches, PREG_SET_ORDER);
            if(!empty($matches)) {
                // if default value needs to be replaced but is not on current page, wait until on the current page to replace it
                if(rgar($field, 'pageNumber') != $current_page) {
                    $field['defaultValue'] = '';
                } else {
                    $field['defaultValue'] = self::preview_replace_variables($default_value, $form);
                }
            }
            // only run 'content' filter for fields on the current page
            if(rgar($field, 'pageNumber') != $current_page)
                continue;
            $html_content = rgar($field, 'content');
            preg_match_all('/{.+}/', $html_content, $matches, PREG_SET_ORDER);
            if(!empty($matches)) {
                $field['content'] = self::preview_replace_variables($html_content, $form);
            }
        }
        return $form;
    }
    /**
    * Adds special support for file upload, post image and multi input merge tags.
    */
    public static function preview_special_merge_tags($value, $input_id, $merge_tag, $field) {

        // added to prevent overriding :noadmin filter (and other filters that remove fields)
        if( ! $value )
            return $value;

        $input_type = RGFormsModel::get_input_type($field);

        $is_upload_field = in_array( $input_type, array('post_image', 'fileupload') );
        $is_multi_input = is_array( rgar($field, 'inputs') );
        $is_input = intval( $input_id ) != $input_id;

        if( !$is_upload_field && !$is_multi_input && $input_type != 'table' )
            return $value;
        // if is individual input of multi-input field, return just that input value
        if( $is_input )
            return $value;

        $form = RGFormsModel::get_form_meta($field['formId']);
        $lead = self::create_lead($form);
        $currency = GFCommon::get_currency();
        if(is_array(rgar($field, 'inputs'))) {
            $value = RGFormsModel::get_lead_field_value($lead, $field);
            if ( $input_type == 'table' ) {
              return build_table_entry_detail_markup( $value, $field, 'preview' );
            } else {
              return GFCommon::get_lead_field_display($field, $value, $currency);
            }
        }
        switch($input_type) {
          case 'fileupload':
            $value = self::preview_image_value("input_{$field['id']}", $field, $form, $lead);
            $value = self::preview_image_display($field, $form, $value);
            break;
          default:
            $value = self::preview_image_value("input_{$field['id']}", $field, $form, $lead);
            $value = GFCommon::get_lead_field_display($field, $value, $currency);
            break;
        }
        return $value;
    }
    public static function preview_image_value($input_name, $field, $form, $lead) {
        $field_id = $field['id'];
        $file_info = RGFormsModel::get_temp_filename($form['id'], $input_name);
        $source = RGFormsModel::get_upload_url($form['id']) . "/tmp/" . $file_info["temp_filename"];
        if(!$file_info)
            return '';
        switch(RGFormsModel::get_input_type($field)){
            case "post_image":
                list(,$image_title, $image_caption, $image_description) = explode("|:|", $lead[$field['id']]);
                $value = !empty($source) ? $source . "|:|" . $image_title . "|:|" . $image_caption . "|:|" . $image_description : "";
                break;
            case "fileupload" :
                $value = $source;
                break;
        }
        return $value;
    }
    public static function preview_image_display($field, $form, $value) {
        // need to get the tmp $file_info to retrieve real uploaded filename, otherwise will display ugly tmp name
        $input_name = "input_" . str_replace('.', '_', $field['id']);
        $file_info = RGFormsModel::get_temp_filename($form['id'], $input_name);
        $file_path = $value;
        if(!empty($file_path)){
            $file_path = esc_attr(str_replace(" ", "%20", $file_path));
            $value = "<a href='$file_path' target='_blank' title='" . __("Click to view", "gravityforms") . "'>" . $file_info['uploaded_filename'] . "</a>";
        }
        return $value;
    }
    /**
    * Retrieves $lead object from class if it has already been created; otherwise creates a new $lead object.
    */
    public static function create_lead( $form ) {

        if( empty( self::$lead ) ) {
            self::$lead = GFFormsModel::create_lead( $form );
            self::clear_field_value_cache( $form );
        }

        return self::$lead;
    }
    public static function preview_replace_variables( $content, $form ) {
        $lead = self::create_lead($form);
        // add filter that will handle getting temporary URLs for file uploads and post image fields (removed below)
        // beware, the RGFormsModel::create_lead() function also triggers the gform_merge_tag_filter at some point and will
        // result in an infinite loop if not called first above
        add_filter('gform_merge_tag_filter', array('GWPreviewConfirmation', 'preview_special_merge_tags'), 10, 4);
        $content = GFCommon::replace_variables($content, $form, $lead, false, false, false);
        // remove filter so this function is not applied after preview functionality is complete
        remove_filter('gform_merge_tag_filter', array('GWPreviewConfirmation', 'preview_special_merge_tags'));
        return $content;
    }

    public static function clear_field_value_cache( $form ) {

        if( ! class_exists( 'GFCache' ) )
            return;

        foreach( $form['fields'] as &$field ) {
            if( GFFormsModel::get_input_type( $field ) == 'total' )
                GFCache::delete( 'GFFormsModel::get_lead_field_value__' . $field['id'] );
        }

    }
}
GWPreviewConfirmation::init();

function get_field_conditional_logic( $form_id, $field, $event ) {
  $logic_event = '';
  switch ($event) {
    case 'keyup':
      if ( count( $field['conditionalLogicFields'] ) > 0 ) {
        $logic_event = "onchange='gf_apply_rules(" . $form_id . ',' . GFCommon::json_encode( $field['conditionalLogicFields'] ) . ");' onkeyup='clearTimeout(__gf_timeout_handle); __gf_timeout_handle = setTimeout(\"gf_apply_rules(" . $form_id . ',' . GFCommon::json_encode( $field['conditionalLogicFields'] ) . ")\", 300);'";
      }
      break;
    case 'change':
      if ( count( $field['conditionalLogicFields'] ) > 0 ) {
        $logic_event = 'onchange="gf_apply_rules(' . $form_id . ',' . GFCommon::json_encode( $field['conditionalLogicFields'] ) . ');"';
      }
      break;
    case 'click':
      if ( count( $field['conditionalLogicFields'] ) > 0 ) {
        $logic_event = 'onclick="gf_apply_rules(' . $form_id . ',' . GFCommon::json_encode( $field['conditionalLogicFields'] ) . ');"';
      }
      break;
    default:
      break;
  }
  return $logic_event;
}


add_filter( 'gform_replace_merge_tags', 'balance_pathways_budget_summary', 10, 7 );
function balance_pathways_budget_summary( $text, $form, $entry, $url_encode, $esc_html, $nl2br, $format ) {
  $custom_merge_tag = '{calculations}';

  if ( strpos( $text, $custom_merge_tag ) === false ) {
    return $text;
  }

  $form_id = RGFormsModel::get_form_id($form['title']);

  // If form is Pathways Budget (id: 5)
	if ( $form_id == 5 ) {
    $income = $expenses = 0;
  	$table = false;
  	$new_content = "";

		foreach( $form['fields'] as &$field ) {
      // monthly income on page 1
			if ( $field->type == 'table' && $field->pageNumber == 1 ) {
				$table = $field;
			}
      // all other fields i.e. expenses
			else if ( $field->type == 'number' ) {
				$expenses += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id ) );
			}
		}
    // sum of net income (column 2)
		for ( $i=0; $i < count( $table['inputs'] ); $i++ ) {
			$index = 1 + count( $table['column_choices'] ) * $i;
			$income += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $table->id . '_' . $index ) );
		}

		$diff = $income - $expenses;

		$new_content .= '<p style="margin-top:50px;">Monthly Living Expenses = <strong>$' . number_format( $expenses, 2 ) . '</strong><br />';
		$new_content .= "Monthly Net Income = <strong>$" . number_format( $income, 2 ) . "</strong><br />";
		$new_content .= "Income after expense = <strong>$" . number_format( $diff, 2 ) . "</strong><br />";

		if ( $diff < 0 ) {
			$new_content .= "<p>" . rgar( $form, 'summary_message_shortfall' ) . "</p>";
		} else {
			$new_content .= "<p>" . rgar( $form, 'summary_message_surplus' ) . "</p>";
		}
    $text = str_replace( $custom_merge_tag, $new_content, $text );
	}

  else if ( rgar( $form, 'title' ) == "Client Worksheet" ) {
    $income_gross = $income_net = $coapp_gross = $coapp_net = 0;
    $assets = array( 'present' => 0, 'owed' => 0, 'payment' => 0 );
    $ess_expenses = array_fill_keys( array( 'housing', 'investments', 'food', 'medical', 'transportation', 'child_care', 'taxes', 'miscellaneous' ), 0 );
    $var_expenses = array_fill_keys( array( 'personal', 'entertainment', 'miscellaneous' ), 0 );
    $debt = 0; // list field!

  	$new_content = $log = $output = '';

		foreach( $form['fields'] as &$field ) {
      if ( $field->type == 'table' ) {
        switch( $field->pageNumber ) {
          // monthly income
    			case 2:
            $cols = count( $field['column_choices'] );
            // applicant; $income_gross = column 1 values sum, $income_net = column 2 values sum
            if ( strcasecmp( $field->label, 'Monthly Income' ) == 0 ) {
              //$log .= 'found monthly income. inputs: '.count($field['inputs']);
              for ( $i=0; $i < count( $field['inputs'] ); $i++ ) {
          			$index1 = 0 + $cols * $i;
                $index2 = 1 + $cols * $i;
                //$log .= "adding ".rgpost( 'input_' . $field->id . '_' . $index1 )." to gross";
          			$income_gross += preg_replace( "/[^0-9.]/", "", rgpost( 'input_' . $field->id . '_' . $index1 ) );
                //$log .=  "(gross now ".$income_gross."). ";
                $income_net += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index2 ) );
          		}
            }
            // co-applicant; $income_gross = column 1 values sum, $income_net = column 2 values sum
            else if ( strcasecmp( $field->label, 'Co-Applicant' ) == 0 ) {
              //$log .= '<br />found co-app monthly income. inputs: '.count($field['inputs']);
              for ( $i=0; $i < count( $field['inputs'] ); $i++ ) {
          			$index1 = 0 + $cols * $i;
                $index2 = 1 + $cols * $i;
          			$coapp_gross += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index1 ) );
                $coapp_net += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index2 ) );
          		}
            }
            break;
    			// assets & liabilities; $assets['present'] = column 1, $assets['owed'] = column 2, $assets['payment'] = column 3
    			case 3:
            //$log .= '<br />found assets & liabilities. inputs: '.count($field['inputs']);
            $cols = count( $field['column_choices'] );
            for ( $i=0; $i < count( $field['inputs'] ); $i++ ) {
              $index1 = 0 + $cols * $i;
              $index2 = 1 + $cols * $i;
              $index3 = 2 + $cols * $i;
              $assets['present'] += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index1 ) );
              $assets['owed'] += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index2 ) );
              $assets['payment'] += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index3 ) );
            }
            break;
          // essential expenses; $ess_expenses['label'] = column 1 for each of the table fields
          case 4:
            $cols = count( $field['column_choices'] );

            $label = preg_replace( "/[^a-z0-9]/", '_', strtolower( $field->label ) );
            //$log .= "label: ".$field->label . ", temp label: ".$label."<br/>";

            if ( ! array_key_exists( $label, $ess_expenses ) || empty( $ess_expenses[$label] ) ) {
              $ess_expenses[$label] = 0;
            }

            for ( $i=0; $i < count( $field['inputs'] ); $i++ ) {
              $index = 0 + $cols * $i;
              $ess_expenses[$label] += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index ) );
            }
            break;
          // variable expenses; $var_expenses['label'] = column 1 for each of the table fields
          case 5:
            $cols = count( $field['column_choices'] );

            $label = preg_replace( "/[^a-z0-9]/", '_', strtolower( $field->label ) );
            //$log .= "label: ".$field->label . ", temp label: ".$label."<br/>";

            if ( ! array_key_exists( $label, $ess_expenses ) || empty( $ess_expenses[$label] ) ) {
              $var_expenses[$label] = 0;
            }

            for ( $i=0; $i < count( $field['inputs'] ); $i++ ) {
              $index = 0 + $cols * $i;
              $var_expenses[$label] += preg_replace( "/[^0-9.]/", '', rgpost( 'input_' . $field->id . '_' . $index ) );
            }
            break;
        }
      }
      // unsecured debt: list; $debt = sum of column 4 amounts
      else if ( $field->type == 'list' ) {
        $cols = count( $field->choices );
        $data = rgpost( 'input_' . $field->id );
        for ( $i=0; $i < count( $data ); $i++ ) {
          $index = 4 + $cols * $i;
          if ( isset( $data[$index] ) ) {
            $debt += preg_replace( "/[^0-9.]/", '', $data[$index] );
          }
        }
      }
		}

    $sum_ess = array_sum( $ess_expenses );
    $sum_var = array_sum( $var_expenses );
    $sum_expenses = $sum_ess + $sum_var;
    $diff = $income_net - $sum_expenses - $debt;

    $output .= '<p style="padding-top:20px;font-size:26px;color:#53b2af;margin-bottom:20px">Summary</p>';
    $output .= '<table class="gf_summary_table">';
    $output .= '    <tr>';
    $output .= '      <th colspan="4">';
    $output .= '        Monthly Income:';
    $output .= '      </th>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>&nbsp;</td>';
    $output .= '      <td>Gross</td>';
    $output .= '      <td>Net</td>';
    $output .= '      <td>&nbsp;</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Monthly Income</td>';
    $output .= '      <td>' . number_format( $income_gross, 2 ) . '</td>';
    $output .= '      <td>' . number_format( $income_net, 2 ) . '</td>';
    $output .= '      <td>&nbsp;</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>&nbsp;</td>';
    $output .= '      <td>Present Value</td>';
    $output .= '      <td>Amount Owed</td>';
    $output .= '      <td>Payment</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Assets & Liabilities</td>';
    $output .= '      <td>' . number_format( $assets['present'], 2 ) . '</td>';
    $output .= '      <td>' . number_format( $assets['owed'], 2 ) . '</td>';
    $output .= '      <td>' . number_format( $assets['payment'], 2 ) . '</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td colspan="4">&nbsp;</td>';
    $output .= '    </tr';
    $output .= '    <tr>';
    $output .= '      <th colspan="4">';
    $output .= '        Monthly Essential Expenses:';
    $output .= '      </th>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Housing</td>';
    $output .= '      <td>' . number_format( $ess_expenses['housing'], 2 ) . '</td>';
    $output .= '      <td>Transportation</td>';
    $output .= '      <td>' . number_format( $ess_expenses['transportation'], 2 ) . '</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Investments</td>';
    $output .= '      <td>' . number_format( $ess_expenses['investments'], 2 ) . '</td>';
    $output .= '      <td>Child Care</td>';
    $output .= '      <td>' . number_format( $ess_expenses['child_care'], 2 ) . '</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Food</td>';
    $output .= '      <td>' . number_format( $ess_expenses['food'], 2 ) . '</td>';
    $output .= '      <td>Taxes</td>';
    $output .= '      <td>' . number_format( $ess_expenses['taxes'], 2 ) . '</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Medical</td>';
    $output .= '      <td>' . number_format( $ess_expenses['medical'], 2 ) . '</td>';
    $output .= '      <td>Miscellaneous</td>';
    $output .= '      <td>' . number_format( $ess_expenses['miscellaneous'], 2 ) . '</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td colspan="4" class="left">Your total monthly essential expense is ' . number_format( $sum_ess, 2 ) . '.</td>';
    $output .= '    </tr';
    $output .= '    <tr>';
    $output .= '      <td colspan="4">&nbsp;</td>';
    $output .= '    </tr';
    $output .= '    <tr>';
    $output .= '      <th colspan="4">';
    $output .= '        Monthly Variable Expenses:';
    $output .= '      </th>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Personal</td>';
    $output .= '      <td>' . number_format( $var_expenses['personal'], 2 ) . '</td>';
    $output .= '      <td>Entertainment</td>';
    $output .= '      <td>' . number_format( $var_expenses['entertainment'], 2 ) . '</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td>Miscellaneous</td>';
    $output .= '      <td>' . number_format( $var_expenses['miscellaneous'], 2 ) . '</td>';
    $output .= '      <td>&nbsp;</td>';
    $output .= '      <td>&nbsp;</td>';
    $output .= '    </tr>';
    $output .= '    <tr>';
    $output .= '      <td colspan="4" class="left">Your total monthly discretionary expense is ' . number_format( $sum_var, 2 ) . '.</td>';
    $output .= '    </tr>';
    $output .= '</table>';

    $output .= '<p>Monthly Unsecured Debt Payment = $' . number_format( $debt, 2 ) . '<br />';
    $output .= 'Monthly Living Expenses = $' . number_format( $sum_expenses, 2 ) . '<br />';
    $output .= 'Monthly Net Income = $' . number_format( $income_net, 2 ) . '<br />';
    $output .= 'Income after expense = $' . number_format( $diff, 2 ) . '</p>';

    $new_content .= $output;
    $new_content .= "<script>
      jQuery(document).ready(function() {
        jQuery('.gfield_list:not(.gfield_table_container), .gfield_list:not(.gfield_table_container) td, .gfield_list:not(.gfield_table_container) th').attr('style', '');
      });
      </script>";

    $text = str_replace( $custom_merge_tag, $new_content, $text );

  }

  return $text;
}

class FormatCurrency {
  var $_field_values = array();
  function __construct() {
      add_filter( 'gform_pre_render', array( $this, 'format_currency_js' ) );
      add_filter( 'gform_pre_validation', array( $this, 'clean_value_for_number_fields' ) );
      add_filter( 'gform_validation', array( $this, 'restore_value_for_number_fields' ) );
      add_action( 'gform_get_field_value', array( $this, 'format_currency' ), 10, 3 );
  }
  function format_currency_js($form) {
      $this->enqueue_gravityforms_js();
      $script = '(function($){' .
          '$("input.gf_currency, .gf_currency input").each( function() { ' .
              '$(this).val( gformFormatMoney( $(this).val() ) ); ' .
          '}).change( function( event ) { ' .
              '$(this).val( gformFormatMoney( $(this).val() ) ); ' .
          '}).keypress(function(e) { filter_non_numeric_keys(e) });' .
          'gform.addFilter( "gform_calculation_format_result", "gwMaybeFormatMoney" ); ' .
          'window.gwMaybeFormatMoney = function( returnVal, origResult, formulaField, formId ) { ' .
              'var field = jQuery("#field_" + formId + "_" + formulaField.field_id); ' .
              'return field.hasClass( "gf_currency" ) ? gformFormatMoney( origResult ) : returnVal; ' .
          '} ' .
      '})(jQuery);';
      GFFormDisplay::add_init_script( $form['id'], 'format_currency', GFFormDisplay::ON_PAGE_RENDER, $script );
      return $form;
  }
  function format_currency( $value, $lead, $field ) {
      $css_classes = explode( ' ', rgar( $field, 'cssClass' ) );
      if( ! in_array( 'gf_currency', $css_classes ) )
          return $value;
      require_once( GFCommon::get_base_path() . '/currency.php' );
      $currency = new RGCurrency( GFCommon::get_currency() );
      $value = $currency->to_money( $value );
      return $value;
  }
  function clean_value_for_number_fields( $form ) {
    foreach( $form['fields'] as $field ) {
      $is_number_field = GFFormsModel::get_input_type( $field ) == 'number';
      $has_gf_currency_class = in_array( 'gf_currency', explode( ' ', rgar( $field, 'cssClass' ) ) );
      if( ! $is_number_field || ! $has_gf_currency_class )
          continue;
      $value = $_POST["input_{$field['id']}"];
      if( ! $value )
          continue;
      // save original value to be restored later
      $this->_field_values[$field['id']] = $value;
      require_once( GFCommon::get_base_path() . '/currency.php' );
      $currency = new RGCurrency( GFCommon::get_currency() );
      $_POST["input_{$field['id']}"] = $currency->to_number( $value );
    }
    return $form;
  }
  function restore_value_for_number_fields( $validation_result ) {
    foreach( $validation_result['form']['fields'] as $field ) {
      if( ! isset( $this->_field_values[ $field['id'] ] ) ) {
        continue;
      }
      $value = $this->_field_values[ $field['id'] ];
      $_POST["input_{$field['id']}"] = $value;
    }
    return $validation_result;
  }
  function enqueue_gravityforms_js() {
    $has_proper_support = version_compare( GFCommon::$version, '1.8.dev4', '>=' );
    $enqueue_script = $has_proper_support ? 'gform_gravityforms' : 'gforms_gravityforms';
    if( ! wp_script_is( $enqueue_script ) ) {
      wp_enqueue_script( $enqueue_script );
    }
  }
}
new FormatCurrency();

add_filter( "gform_email_background_color_label", "set_email_label_color", 10, 3 );
function set_email_label_color( $color, $field, $lead ) {
  return "#FFF";
}
add_filter( "gform_email_background_color_data", "set_email_data_color", 10, 3 );
function set_email_data_color( $color, $field, $lead ) {
  return "#FFF";
}
