<?php
/*
 * Balance Custom post type: News
 *
 */

 /*
 * custom post type registration
 */

function cpt_news() {
  $labels = array(
    'name'               => __( 'News', 'balance' ),
    'singular_name'      => __( 'News', 'balance' ),
    'add_new'            => __( 'Add New', 'balance' ),
    'add_new_item'       => __( 'Add New News', 'balance' ),
    'edit_item'          => __( 'Edit News', 'balance' ),
    'new_item'           => __( 'New News', 'balance' ),
    'all_items'          => __( 'News', 'balance' ),
    'view_item'          => __( 'View News', 'balance' ),
    'search_items'       => __( 'Search News', 'balance' ),
    'not_found'          => __( 'No News found', 'balance' ),
    'not_found_in_trash' => __( 'No News found in the Trash', 'balance' ),
    'parent_item_colon'  => '',
    'menu_name'          => __( 'News', 'balance' )
  );
  $args = array(
    'labels'        => $labels,
    'description'   => __( 'Data about News', 'balance' ),
    'public'        => true,
    'supports'      => array( 'title', 'thumbnail' ),
    'has_archive'   => false,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 28,
    'menu_icon' => 'dashicons-media-document',
    'capability_type' => 'post',
    'hierarchical' => false,
  );
  register_post_type( 'news', $args );
}
add_action( 'init', 'cpt_news' );

/*
 * custom post type update messages
 */
function news_updated_messages( $messages ) {
    global $post, $post_ID;
    $messages['news'] = array(
        0 => '',
        1 => __( 'News updated.', 'balance' ),
        2 => __( 'Custom field updated.', 'balance' ),
        3 => __( 'Custom field deleted.', 'balance' ),
        4 => __( 'News post updated.', 'balance' ),
        5 => isset($_GET['revision']) ? sprintf( __('News post restored to revision from %s', 'balance' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6 => __( 'News saved.', 'balance' ),
        7 => __( 'News saved.', 'balance' ),
        8 => '',
        9 => '',
        10 => '',
    );
    return $messages;
}
add_filter( 'post_updated_messages', 'news_updated_messages' );

?>
