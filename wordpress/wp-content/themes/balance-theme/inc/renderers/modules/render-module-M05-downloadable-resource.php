<?php

function render_m05_section_title_copy_button( $module_data ) {
  $output = '';
  if ( !empty( $module_data['title'] ) || !empty( $module_data['copy'] ) ) {
    $output .= '<article name="module_title_" class="text-block article" aria-label="article module ' .str_replace(array("'",'"'),'', $module_data["title"]). '">';
    $output .=   '<div class="container">';
    $output .=     !empty( $module_data['title'] ) ? '<h1 class="h2 text-info text-center">'.$module_data['title'].'</h1>' : '';
    $output .=     '<div class="row">';
    $output .=       !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
    $output .=       '<div class="btn-holder">';
    if ( !empty( $module_data['cta'] ) && !empty( $module_data['resource']) ) {
      $output .= !empty( $module_data['linkfield'] ) ? sprintf(
        build_link( $module_data['linkfield'] ),
        'btn btn-warning',
        $module_data['buttontext']
      ) : '';
    }
    $output .= '</div>';
    $output .=     '</div>';
    $output .=   '</div>';
    $output .=  '</article>';
  }

  return stripslashes( $output );
}
