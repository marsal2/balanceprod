<?php

function render_m54_welcome_message( $module_data ) {
	$output = '';
	if ( !empty( $module_data['title'] ) || !empty( $module_data['copy'] ) ) {
		$output .= '<!-- M54: WELCOME MESSAGE -->';
		$output .= '<article class="text-block article default-content-style" aria-label="article module ' .str_replace(array("'",'"'),'', $module_data["title"]). '">';
		$output .= ' <div class="container">';
		$output .= '   <div class="row">';
		$output .= !empty( $module_data['title'] ) ? '<h1 class="text-info text-center">' . $module_data['title'] . '</h1>' : '';
		$output .= !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
		$output .= '    </div>';
		$output .= '  </div>';
		$output .= '</article>';
		$output .= '<!-- end M54: WELCOME MESSAGE -->';
	}

	return stripslashes( $output );
}
