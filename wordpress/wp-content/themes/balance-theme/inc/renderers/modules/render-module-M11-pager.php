<?php

function render_m11_pager($url_base, $total_pages, $current_page = 1) {
  $output = '';
  //Fix pages that are out of bounds
  if( $current_page < 1 ) {
    $current_page = 1;
  }
  //Fix pages that are out of bounds
  if( $current_page > $total_pages ) {
    $current_page = $total_pages;
  }
  $url_base .= 'page/';
  //Calculate starting end ending position for the pager
  $after = 5;
  $before = 5;

  $previous = $current_page == 1 ? 1 : $current_page - 1;
  $next = $current_page == $total_pages ? $total_pages : $current_page + 1;

  if( $current_page - $before < 1 ) {
    $start = 1;
    $after += $before - $current_page;
  }else {
    $start = $current_page - $before;
  }

  if( $current_page + $after > $total_pages) {
    $end = $total_pages;
    $start -= (($current_page + $after) - $total_pages);
    if($start < 1) {
      $start = 1;
    }
  }else {
    $end = $current_page + $after;
  }

  for( $i = ($start-1); $i < $end; $i++ ) {
    $active = ($i+1) == $current_page ?  'class="active"' : '';
    $output.= '<li '.$active.'><a href="'.$url_base.($i+1).'">'.($i+1).'</a></li>';
  }

  $output = '
    <nav aria-label="paging holder m11" class="paging-holder">
      <div class="container">
        <div class="row">
          <ul class="pagination">
            <li>
              <a href="'.$url_base.$previous.'" aria-label="'.__('Previous').'">
                <span class="btn-prev"></span>
                <span class="hidden-xs">'.__('Previous').'</span>
              </a>
            </li>
            '.$output.'
            <li>
              <a href="'.$url_base.$next.'" aria-label="'.__('Next').'">
                <span class="hidden-xs">'.__('Next').'</span>
                <span class="btn-next"></span>
              </a>
            </li>
          </ul>
          <p>of '.$total_pages.' '.__('pages').'</p>
        </div>
      </div>
    </nav>';
  return $output;
}