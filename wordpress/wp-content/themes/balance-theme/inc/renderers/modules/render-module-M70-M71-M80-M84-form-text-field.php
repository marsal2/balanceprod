<?php

function render_m70_m84_form_text_field( $field, $form_id = '', $value = '' ) {
  $logic_event = '';
  // assign correct field html type
  switch ($field['type']) {
    case 'phone':
      $logic_event = get_field_conditional_logic( $form_id, $field, 'keyup' );
      $field['html_type'] = 'text';
      break;
    case 'website':
      $logic_event = get_field_conditional_logic( $form_id, $field, 'keyup' );
      $field['html_type'] = 'text';
      break;
    case 'number':
      $logic_event = get_field_conditional_logic( $form_id, $field, 'keyup' );
      // We use <input type="number"> for most numeric fields. However, the 'gf_currency'
      // custom validation class applied to currency fields requires <input type="text">.
      $field['html_type'] = $field["numberFormat"] == "currency" ? 'text' : 'number';
      break;
    case 'date':
      $field['html_type'] = 'text';
      break;
    case 'text':
    case 'textarea':
      $logic_event = get_field_conditional_logic( $form_id, $field, 'keyup' );
      $field['html_type'] = $field['type'];
      break;
  }

  $output_field = '';
  if ( $field['type'] == 'textarea' ) {
    $output_field .= '  <textarea id="input_' . $form_id . '_' . $field['id'] . '" name="input_' . $field['id'] . '" type="' . $field['html_type'] . '" placeholder="' . $field['placeholder'] . '" ' . $logic_event . '>' . $value . '</textarea>';
  } else {
    $output_field .= '  <input id="input_' . $form_id . '_' . $field['id'] . '" name="input_' . $field['id'] . '" type="' . $field['html_type'] . '" placeholder="' . $field['placeholder'] . '" value="' . $value . '" ' . ( $field['type'] == 'date' ? ' data-datepicker="{&quot;dateFormat&quot;: &quot;' . getDateFormatById( $field['dateFormat'] ) . '&quot;,&quot;showWeek&quot;: true, &quot;changeMonth&quot;: true, &quot;changeYear&quot;: true, &quot;showOn&quot;: &quot;both&quot;, &quot;maxDate&quot;: &quot;+0d&quot;, &quot;yearRange&quot;: &quot;-100:+0&quot;}" "' : '') . ' ' . $logic_event . '>';
  }
  $output = '';
  $output = sprintf( get_form_field_wrap( $field, $form_id ), $output_field );
  return stripslashes( $output );
}

function getDateFormatById( $date_format_id = 'mdy' ) {
  $date_format_string = '';
  switch ( $date_format_id ) {
    case 'mdy':
      $date_format_string = 'mm/dd/yy';
      break;
    case 'dmy':
      $date_format_string = 'dd/mm/yy';
      break;
    case 'dmy_dash':
      $date_format_string = 'dd-mm-yy';
      break;
    case 'dmy_dot':
      $date_format_string = 'dd.mm.yy';
      break;
    case 'ymd_slash':
      $date_format_string = 'yy/mm/dd';
      break;
    case 'ymd_dash':
      $date_format_string = 'yy-mm-dd';
      break;
    case 'ymd_dash':
      $date_format_string = 'yy.mm.dd';
      break;
    default:
      $date_format_string = 'mm/dd/yy';
      break;
  }
  return $date_format_string;
}
