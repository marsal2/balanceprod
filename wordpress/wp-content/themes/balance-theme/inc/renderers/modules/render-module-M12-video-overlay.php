<?php

function render_m12_video_overlay( $module_data ) {
	$output = '';
	if ( !empty( $module_data['video'] ) ) {
		$video_url = wp_oembed_get( $module_data['video'] );
		if ( $video_url ) {
			$output .= '  <section aria-label="video holder" class="background-white">';
			$output .= '    <div class="container">';
			$output .= '      <div class="row">';
			$output .= '        <div class="col-xs-12 player-wrapper">';
			$output .= '          <div class="video-holder">';
			$output .=             $video_url;
			$output .= '          </div>';
			$output .= '        </div>';
			$output .= '      </div>';
			$output .= '    </div>';
			$output .= '  </section>';
		}
	}


	return stripslashes( $output );
}
