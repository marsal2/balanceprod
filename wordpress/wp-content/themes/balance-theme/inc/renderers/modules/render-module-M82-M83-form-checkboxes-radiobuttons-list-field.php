<?php

function render_m82_m83_form_checkboxes_radiobuttons_list_field( $field, $form_id = '', $value = '' ) {
  $main_wrapper_class = $field['type'];
  $inner_wrapper_class = $field['type'];
  // assign correct wrapper class
  switch ($field['type']) {
    case 'radio':
      $main_wrapper_class = 'radio-wrap';
      $inner_wrapper_class = 'radio';
      break;
    case 'checkbox':
      $main_wrapper_class = 'checkbox';
      $inner_wrapper_class = 'check';
      break;
    default:
      $main_wrapper_class = $field['type'];
      $inner_wrapper_class = $field['type'];
      break;
  }
  $logic_event = get_field_conditional_logic( $form_id, $field, 'click' );
  $output = '';
  $output .= '<div class="' . $main_wrapper_class . ' ginput_container ginput_container_' . $field['type'] . '">';
  
  $output .= '  <span class="label label-title">';
  $output .= '    <label>' . $field['label'] . ( $field['isRequired'] ? ' <span class="gfield_required">*</span>' : '' ) . '</label>';
  $output .= '  </span>';

  $output .= '  <ul class="choose-list" id="input_' . $form_id . '_' . $field['id'] . '">';
      
  $choice_number = 1;

  // add 'other' choice to choices if enabled
  if ( !empty( $field['enableOtherChoice'] ) && $field['enableOtherChoice'] ) {
    $other_default_value = GFCommon::get_other_choice_value();
    // $field['choices'][] = array( 'text' => $other_default_value, 'value' => 'gf_other_choice', 'isSelected' => false, 'isOtherChoice' => true );
  }
  $count = 0;
  $count_of_choices = count( $field['choices'] );
  foreach ($field['choices'] as $choice) {
    $count++;

    // if the field is radio and is the fourth one open up a new li element
    if ( $field['type'] == 'radio' ) {
      if ( $count % 3 == 1 ) {
        $output .= '<li>';
      }
    } else { // if the field is checkbox open up li on every iteration
      $output .= '<li>';
    }

    if ( $choice_number % 10 == 0 ) { //hack to skip numbers ending in 0. so that 5.1 doesn't conflict with 5.10
      $choice_number ++;
    }

    $input_id = $field['id'] . '.' . $choice_number;
    $id = $form_id . '_' . $field['id'] . '_' . $choice_number ++;

    $name = 'input_' . $input_id;
    $input_focus  = '';

    if ( ! isset( $_GET['gf_token'] ) && empty( $_POST ) && rgar( $choice, 'isSelected' ) ) {
      $checked = "checked='checked'";
    } elseif ( is_array( $value ) && RGFormsModel::choice_value_match( $field, $choice, rgget( $input_id, $value ) ) ) {
      $checked = "checked='checked'";
    } elseif ( ! is_array( $value ) && RGFormsModel::choice_value_match( $field, $choice, $value ) ) {
      $checked = "checked='checked'";
    } else {
      $checked = '';
    }

    if ( $field['type'] == 'radio' ) {
      $name = 'input_' . $field['id'];
      $input_focus  = "onfocus=\"jQuery(this).parent().parent().find('input[type=text]').focus();\"";
      $checked = RGFormsModel::choice_value_match( $field, $choice, $value ) ? "checked='checked'" : '';
    }

    // if the radio button list contains other option display a text field for input / set field appropriately
    if ( !empty( $choice['isOtherChoice'] ) && $choice['isOtherChoice'] ) {
      $onfocus = "onfocus=\"jQuery(this).parent().find('input').click(); if(jQuery(this).val() == '" . $other_default_value . "') { jQuery(this).val(''); }\"";
      $onblur  = "onblur=\"if(jQuery(this).val().replace(' ', '') == '') { jQuery(this).val('" . $other_default_value . "'); }\"";

      $value_exists = RGFormsModel::choices_value_match( $field, $field['choices'], $value );

      if ( $value == 'gf_other_choice' && rgpost( "input_" . $field['id'] . "_other" ) ) {
        $other_value = rgpost( "input_" . $field['id'] . "_other" );
      } elseif ( ! $value_exists && ! empty( $value ) ) {
        $other_value = $value;
        $value       = 'gf_other_choice';
        $checked     = "checked='checked'";
      } else {
        $other_value = $other_default_value;
      }
    }

    $output .= '     <div class="' . $inner_wrapper_class . '">';
    $output .= '       <label for="choice_' . $id . '" id="label_' . $id . '">';
    $output .= '         <input id="choice_' . $id .'" name="' . $name . '" value="' . $choice['value'] . '" ' . $checked . ' type="' . $field['type'] . '" ' . $input_focus . ' ' . $logic_event . '>';
    $output .= '         <span class="fake-input"></span>';
    $output .= '         <span class="fake-label">' . $choice['text'] . '</span>';
    $output .= '       </label>';

    if ( !empty( $choice['isOtherChoice'] ) && $choice['isOtherChoice'] ) {
      $output .= '     <input id="input_' . $form_id . '_' . $field['id'] . '_other" name="input_' . $field['id'] . '_other" type="text" value="' . esc_attr( $other_value ) . '" aria-label="' . __( 'Other', 'balance' ) . '" ' . $onfocus . ' ' . $onblur . ' class="other" />';
    }

    $output .= '     </div>';

    // if the field is radio and is the third one close up a li element
    if ( $field['type'] == 'radio' ) {
      if ( $count % 3 == 0 ) {
        $output .= '</li>';
      }
    } else { // if the field is checkbox close li on every iteration
      $output .= '</li>';
    }

  }

  // this is to ensure there is no open li if the number of elements in choices is not a multiple of 4, for radio type only
  if ( $field['type'] == 'radio' ) {
    if ( $count % 3 == 0 ) {
      $output .= '</li>';
    }
  }

  $output .= '  </ul>';
  $output .= '</div>';

  if ( !empty( $field['description'] ) ) {
    $output .= '<div class="note">';
    $output .= '  <p>' . $field['description'] . '</p>';
    $output .= '</div>';
  }
  if ( $field['failed_validation'] && $field['validation_message'] != '' ) {
    $output .= '<div class="validation_message note">';
    $output .= $field['validation_message'];
    $output .= '</div>';
  }

  return stripslashes( $output );
}
