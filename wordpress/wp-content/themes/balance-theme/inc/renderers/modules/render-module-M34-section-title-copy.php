<?php

function render_m34_section_title_copy( $module_data, $secondary_markup = true ) {
	$output = '';

	if ( !empty( $module_data['title'] ) || !empty( $module_data['copy'] ) ) {
		if ( !$secondary_markup ) {
			$output .= '<!-- M34: SECTION -->';
			$output .= '<div class="block">';
			$output .=   '<div class="container">';
			$output .=     '<div class="row">';
			$output .=       '<div class="article">';
			$output .=         !empty( $module_data['title'] ) ? '<h1 class="text-info text-center">'.$module_data['title'].'</h1>' : '';
			$output .=         !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
			$output .=       '</div>';
			$output .=     '</div>';
			$output .=   '</div>';
			$output .=  '</div>';
			$output .= '<!-- end M34: SECTION -->';
		} else {
			$output .= '<!-- M34/M31: SECTION -->';
			$output .= '<article name="" class="text-block article default-content-style" aria-label="module for article '.str_replace(array("'",'"'),'', $module_data['title']).'">';
			$output .= ' <div class="container">';
			$output .= '    <div class="row">';
			$output .=         !empty( $module_data['title'] ) ? '<h1 class="text-info text-center">'.$module_data['title'].'</h1>' : '';
			$output .=         !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
			$output .= '    </div>';
			$output .= '  </div>';
			$output .= '</article>';
			$output .= '<!-- end M34/M31: SECTION -->';

		}

	}

	return stripslashes( $output );
}
