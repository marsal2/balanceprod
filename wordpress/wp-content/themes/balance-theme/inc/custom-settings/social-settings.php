<?php

/**
 * Google maps settings page
 */
add_action( 'init', 'social_settings_init' );
function social_settings_init() {

  $settings = get_option( "social_settings" );
    if ( empty( $settings ) ) {
      $settings = array(
        'facebook' => '',
        'twitter' => '',
        'google' => '',
        'youtube' => '',
      );
    add_option( "social_settings", $settings, '', 'yes' );
  }
}

function social_setting_page_init() {

  $settings_page = add_options_page( 'Social Links', 'Social Links', 'manage_options', 'social_settings', 'social_settings_page' );
  add_action( "load-{$settings_page}", 'social_settings_load_settings_page' );
}
add_action( 'admin_menu', 'social_setting_page_init' );

function social_settings_load_settings_page() {
  if ( isset($_POST["social_settings_submit"]) && $_POST["social_settings_submit"] == 'Y' ) {
    social_settings_save_settings();
    wp_redirect(admin_url('options-general.php?page=social_settings'));
    exit;
  }
}

function social_settings_page() {
  global $pagenow;
    $settings = get_option( "social_settings" );
  ?>

  <div class="wrap">
    <h2>Social links</h2>
    <div id="poststuff" class="custom-settings">
      <form method="post" action="<?php admin_url( 'options-general.php?page=social_settings' ); ?>">
                <p>
                  <label><?php _e( 'Facebook', 'balance' ); ?>:</label><br/>
                  <?php echo text_field( $settings['facebook'], 'facebook', '', 'full-width' ); ?>
                </p>
                <p>
                  <label><?php _e( 'Twitter', 'balance' ); ?>:</label><br/>
                  <?php echo text_field( $settings['twitter'], 'twitter', '', 'full-width' ); ?>
                </p>
                <p>
                  <label><?php _e( 'Google+', 'balance' ); ?>:</label><br/>
                  <?php echo text_field( $settings['google'], 'google', '', 'full-width' ); ?>
                </p>
                <p>
                  <label><?php _e( 'Youtube', 'balance' ); ?>:</label><br/>
                  <?php echo text_field( $settings['youtube'], 'youtube', '', 'full-width' ); ?>
                </p>
        <p class="submit" style="clear: both;">
          <input type="submit" name="Submit"  class="button-primary" value="Update Settings" />
          <input type="hidden" name="social_settings_submit" value="Y" />
        </p>
      </form>
    </div>
  </div>
<?php
}

function social_settings_save_settings(){
  global $pagenow;
  if ( $pagenow == 'options-general.php' && $_GET['page'] == 'social_settings' )
  {
      $settings = array();
      $settings = get_option( "social_settings" );
      $settings['facebook'] = $_POST['facebook'];
      $settings['twitter'] = $_POST['twitter'];
      $settings['google'] = $_POST['google'];
      $settings['youtube'] = $_POST['youtube'];
      $updated = update_option( "social_settings", $settings );
  }
}
