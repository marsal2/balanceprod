<?php

// This is main file in which you include all additions to default wp

/**
 * FUNCTIONS
 */
// custom helper functions which are global (backend/frontend)
require_once get_template_directory() . '/inc/custom-functions.php';
require_once get_template_directory() . '/inc/navigation-functions/menu-functions.php';
require_once get_template_directory() . '/inc/navigation-functions/breadcrumb-functions.php';

require_once get_template_directory() . '/inc/controllers/controller-M24.php';


/**
 * MODULE EDIT SCREENS
 */
require_once get_template_directory() . '/inc/edit/edit.php';
// fields which we need for custom edit modules
require_once get_template_directory() . '/inc/edit/fields.php';
// modules which we display inside edit
$dir = get_theme_root() . '/balance-theme/inc/edit/modules/*.php';
foreach ( glob( "$dir" ) as $filename ) {
	include $filename;
}

/**
 * MODULE RENDERERS
 */
// helpers for modules, shared modules functionality
require_once get_template_directory() . '/inc/renderers/helpers.php';
$dir = get_theme_root() . '/balance-theme/inc/renderers/modules/*.php';
foreach ( glob( "$dir" ) as $filename ) {
	require_once $filename;
}

/**
 * CUSTOM SETTING PAGES
 */
$dir = get_theme_root() . '/balance-theme/inc/custom-settings/*.php';
foreach ( glob( "$dir" ) as $filename ) {
	require_once $filename;
}

/**
 * CUSTOM POST TYPES
 */
$dir = get_theme_root() . '/balance-theme/inc/post-types/*.php';
foreach ( glob( "$dir" ) as $filename ) {
	require_once $filename;
}

/**
 * CUSTOM TAXONOMIES
 */
$dir = get_theme_root() . '/balance-theme/inc/taxonomies/*.php';
foreach ( glob( "$dir" ) as $filename ) {
	require_once $filename;
}

/**
 * CUSTOM META BOXES
 */
$dir = get_theme_root() . '/balance-theme/inc/meta-boxes/*.php';
foreach ( glob( "$dir" ) as $filename ) {
	require_once $filename;
}

/**
 * CUSTOM WOOCOMMERCE FUNCTIONS
 */
$dir = get_theme_root() . '/balance-theme/woocommerce/wc-custom-functions.php';
foreach ( glob( "$dir" ) as $filename ) {
	require_once $filename;
}

/**
 * CUSTOM GRAVITY FORMS FUNCTIONS
 */
require_once get_template_directory() . '/inc/custom-gravity-form-functions.php';
?>
