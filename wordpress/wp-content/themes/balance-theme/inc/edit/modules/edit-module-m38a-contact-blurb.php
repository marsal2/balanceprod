<?php
/*
 *  WP Edit module: M38a
 *  Description: Module section: M38a - Contact details
 */

function m38a_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if ( empty( $custom_settings ) ) {
    $custom_settings = array('media_buttons' => false, 'quicktags' => false );
  }

  $output = simple_edit_module($key, 'm38a', array(
    array(
      'type' => 'text',
      'name' => 'title',
      'default_value' => '',
      'label' => 'Title'
    ),
    array(
      'type' => 'textarea',
      'name' => 'address',
      'label' => 'Address',
      'default_value' => '',
      'additional_params' => array(
        false,
        3,
        '',
        '',
        $custom_settings
      )
    ),
    array(
      'type' => 'text',
      'name' => 'phone',
      'default_value' => '',
      'label' => 'Phone'
    ),
    array(
      'type' => 'text',
      'name' => 'fax',
      'default_value' => '',
      'label' => 'Fax'
    ),
  ), $data, $module_title, $visible_on);

  return $output;
}