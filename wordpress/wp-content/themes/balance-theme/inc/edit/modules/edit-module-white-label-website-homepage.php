<?php
/*
 *  WP Edit module: White Label Websites Homepage
 */
function white_label_websites_homepage_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
  global $data;

  if( !empty( $data['wlw_homepage_module'] ) ){
    $data['wlw_homepage_module'][ $key ] = array(
      'title' => '',
    );
  }

  $output = '';
  $output .= '<a name="wlw-homepage-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper wlw-homepage-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom wlw-homepage-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside">';
  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}
?>
