<?php
/*
 *  WP Edit module: M38
 *  Description: Module section: M38 - Copy
 */

function m38_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if ( empty( $custom_settings ) ) {
    $custom_settings = array('media_buttons' => false, 'quicktags' => true );
  }

  $output = simple_edit_module($key, 'm38', array(
    array(
      'type' => 'textarea',
      'name' => 'copy',
      'label' => 'Copy',
      'default_value' => '',
      'additional_params' => array(
        true,
        20,
        '',
        '',
        $custom_settings
      )
    )
  ), $data, $module_title, $visible_on);

  return $output;
}