<?php
/*
 *  WP Edit module: M24
 *  Description: Quiz question
 */

function m24_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  $output = simple_edit_module($key, 'quiz', array(
    array(
          'type' => 'repeater',
          'name' => 'quiz',
          'default_value' => array(),
          'label' => 'Quiz question',
          'fields' => array(
            array(
              'type' => 'text',
              'name' => 'question',
              'label' => 'Question',
              'default_value' => ''
            ),
            array(
              'type' => 'repeater',
              'name' => 'answers',
              'default_value' => array(),
              'label' => 'Answer',
              'fields' => array(
                array(
                  'type' => 'text',
                  'name' => 'answer',
                  'label' => 'Answer',
                  'default_value' => ''
                ),
                array(
                  'type' => 'checkbox',
                  'name' => 'is_correct',
                  'label' => 'This is the correct answer',
                  'default_value' => ''
                )
              )
            )
          )
        )
  ), $data, $module_title, $visible_on);

  return $output;

}

?>
