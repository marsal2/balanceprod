<?php
/*
 *  WP Edit module: M4
 *  Description: Module with downloadable content. M04 – Downloadable content 1; M05 – Downloadable content 2
 */

function m4_module_form( $key, $visible_on = 'all', $module_title = '') {
  global $data;

  if( empty( $data['m4_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm4_module');
    $data['m4_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
      'resource' => '',
      'image' => array(
        'attachment_id' => '',
        'filename' => '',
        'fullpath' => ''
      ),
    );
  }

  $autocomplete_args_pages = array(
    'post_type' => 'attachment',
    'post_mime_type' => array(
      'application/pdf',
    ),
    'posts_per_page' => -1
  );

  $output = '';
  $output .= '<a name="m4-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m4-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m4-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '        <p>';
  $output .= '          <label>'. __( 'Title', 'balance' ) .':</label>';
  $output .=              text_field( $data['m4_module'][ $key ]['title'], 'm4_module['.$key.'][title]');
  $output .= '        </p>';

  $output .= '            <p>';
  $output .= '              <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/>';
  $output .=                textarea_field( $data['m4_module'][ $key ]['copy'], 'm4_module['.$key.'][copy]', false, 0, '', '', array('media_buttons' => false, 'quicktags' => false ) );
  $output .= '            </p>';

  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'File', 'balance' ) .':</b></label>';
  $output .=              multi_autocomplete_field( $data['m4_module'][ $key ]['resource'], 'm4_module['.$key.'][resource]', $autocomplete_args_pages, 1);
  $output .= '        </p>';

  $output .= '        <p>';
  $output .= '           <label><b>'. __( 'Image', 'balance' ) .':</b></label><br/>';
  $output .=             upload_field( $data['m4_module'][ $key ]['image'], 'm4_module['.$key.'][image]' );
  $output .= '        </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
