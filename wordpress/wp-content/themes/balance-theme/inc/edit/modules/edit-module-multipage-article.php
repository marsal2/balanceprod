<?php
/*
 *  WP Edit module: multipage
 *  Description: Multi page article module
 */

function multipage_module_form( $key, $visible_on = 'all', $module_title = '') {
  global $data;
  global $resources_cpts;

  $output = simple_edit_module($key, 'multipage_module', array(
    array(
      'type' => 'repeater',
      'name' => 'pages',
      'default_value' => array(),
      'label' => 'Page',
      'fields' => array(
        array(
          'type' => 'text',
          'name' => 'title',
          'label' => 'Page title',
          'default_value' => ''
        ),
        array(
          'type' => 'textarea',
          'name' => 'copy',
          'label' => 'Copy',
          'default_value' => '',
          'additional_params' => array(
            true,
            0,
            '',
            '',
            array('media_buttons' => true, 'quicktags' => true )
          )
        ),
        array(
          'type' => 'text',
          'name' => 'side_related_resources_title',
          'label' => 'Related floater title',
          'default_value' => ''
        ),
        array(
          'type' => 'checkbox',
          'name' => 'related_quiz_link',
          'label' => 'Link related quiz with related resources',
          'default_value' => ''
        ),
        array(
          'type' => 'repeater',
          'name' => 'side_related_resources',
          'label' => 'Related resource',
          'default_value' => array(),
          'fields' => array(
            array(
              'type' => 'text',
              'name' => 'link_text',
              'label' => 'Related resource link text',
              'default_value' => ''
            ),
            array(
              'type' => 'multi_autocomplete',
              'name' => 'side_related_resources',
              'label' => 'Related resource',
              'default_value' => '',
              'additional_params' => array(
                array(
                  'post_type' =>  $resources_cpts,
                  'posts_per_page' => -1,
                  'post_status' => 'publish'
                ),
                1
              )
            ),
          )
        ),
        array(
          'type' => 'repeater',
          'name' => 'quiz',
          'default_value' => array(),
          'label' => 'End of page quiz question',
          'max' => 1,
          'fields' => array(
            array(
              'type' => 'text',
              'name' => 'question',
              'label' => 'Question',
              'default_value' => ''
            ),
            array(
              'type' => 'repeater',
              'name' => 'answers',
              'default_value' => array(),
              'label' => 'Answer',
              'fields' => array(
                array(
                  'type' => 'text',
                  'name' => 'answer',
                  'label' => 'Answer',
                  'default_value' => ''
                ),
                array(
                  'type' => 'checkbox',
                  'name' => 'is_correct',
                  'label' => 'This is the correct answer',
                  'default_value' => ''
                )
              )
            )
          )
        ),
        array(
          'type' => 'text',
          'name' => 'quiz_success_message',
          'label' => 'Quiz correct message',
          'default_value' => ''
        ),
        array(
          'type' => 'text',
          'name' => 'quiz_fail_message',
          'label' => 'Quiz fail message',
          'default_value' => ''
        )
      ),
    ),
    array(
      'type' => 'spacer',
    ),
    array(
      'type' => 'multi_autocomplete',
      'name' => 'last_page_quiz',
      'label' => 'Optional quiz for the last page',
      'default_value' => '',
      'additional_params' => array(
        array(
          'post_type' =>  'quiz',
          'posts_per_page' => -1,
          'post_status' => 'publish'
        ),
        1
      )
    )
  ), $data, $module_title, $visible_on);
  return $output;
}
