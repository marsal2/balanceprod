<?php
/*
 *  WP Edit module: White Label Websites Programs
 */
function white_label_websites_programs_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	if ( empty( $data['wlw_programs_module'] ) ) {
		$data = init_array_on_var_and_array_key($data, 'wlw_programs_module');
		$data['wlw_programs_module'][ $key ] = array(
			'programs' => '',
			'programs_protected' => 'individually'
		);
	}

	$autocomplete_args_programs = array(
		'post_type' => 'program',
		'posts_per_page' => -1,
		'post_status' => 'publish'
	);

	//Left right image choices
	$programs_protected_choices = array(
		'individually'  => __( 'Programs Individually Protected', 'balance' ),
		'all' => __( 'All Programs Protected', 'balance' ),
	);

	if ( empty( $data['wlw_programs_module'][ $key ]['programs_protected'] ) ) {
		$data['wlw_programs_module'][ $key ]['programs_protected'] = 'individually';
	}



/*START OF SETTINGS*/

  if ( empty($data['test_times'][ $key ]) ) {
    $data = init_array_on_var_and_array_key($data, 'test_times');
    $data['test_times'] = array(
      'type' => 'life_stage'
    );
  }


  $array_of_types = array(
    1 => array(
      'id' => '1',
      'title' => '1',
    ),
    2 => array(
      'id' => '2',
      'title' => '2'
    ),
    3 => array(
      'id' => '3',
      'title' => '3'
    ),
    4 => array(
      'id' => '4',
      'title' => '4'
    ),
    5 => array(
      'id' => '5',
      'title' => '5'
    ),
    6 => array(
      'id' => '6',
      'title' => '6'
    ),
    7 => array(
      'id' => '7',
      'title' => '7'
    ),
    8 => array(
      'id' => '8',
      'title' => '8'
    ),
    9 => array(
      'id' => '9',
      'title' => '9'
    ),
    10 => array(
      'id' => '10',
      'title' => '10'
    ),
    11 => array(
      'id' => '11',
      'title' => '11'
    ),
    12 => array(
      'id' => '12',
      'title' => '12'
    ),
    13 => array(
      'id' => '13',
      'title' => '13'
    ),
    14 => array(
      'id' => '14',
      'title' => '14'
    ),
    15 => array(
      'id' => '15',
      'title' => '15'
    ),
    16 => array(
      'id' => '16',
      'title' => '16'
    ),
    17 => array(
      'id' => '17',
      'title' => '17'
    ),
    18 => array(
      'id' => '18',
      'title' => '18'
    ),
    19 => array(
      'id' => '19',
      'title' => '19'
    ),
    20 => array(
      'id' => '20',
      'title' => '20'
    ),
    21 => array(
      'id' => '21',
      'title' => '21'
    ),
    22 => array(
      'id' => '22',
      'title' => '22'
    ),
    23 => array(
      'id' => '23',
      'title' => '23'
    ),
    24 => array(
      'id' => '24',
      'title' => '24'
    ),
    25 => array(
      'id' => '25',
      'title' => '25'
    ),
    26 => array(
      'id' => '26',
      'title' => '26'
    ),
    27 => array(
      'id' => '27',
      'title' => '27'
    ),
    28 => array(
      'id' => '28',
      'title' => '28'
    ),
    29 => array(
      'id' => '29',
      'title' => '29'
    ),
    30 => array(
      'id' => '30',
      'title' => '30'
    )
  );

  $passing_percent = array(
    0 => array(
      'id' => '',
      'title' => 'Select',
    ),
    1 => array(
      'id' => '50',
      'title' => '50',
    ),
    2 => array(
      'id' => '55',
      'title' => '55',
    ),
    3 => array(
      'id' => '60',
      'title' => '60'
    ),
    4 => array(
      'id' => '65',
      'title' => '65'
    ),
    5 => array(
      'id' => '70',
      'title' => '70'
    ),
    6 => array(
      'id' => '75',
      'title' => '75'
    ),
    7 => array(
      'id' => '80',
      'title' => '80'
    ),
    8 => array(
      'id' => '85',
      'title' => '85'
    ),
    9 => array(
      'id' => '90',
      'title' => '90'
    ),
    10 => array(
      'id' => '95',
      'title' => '95'
    ),
    11 => array(
      'id' => '100',
      'title' => '100'
    )
  );


  if ( empty( $data['test_duration'] ) ) {
    $data = init_array_on_var_and_array_key($data, 'test_duration');
    $data['test_duration'] = array(
      'yes_no' => ''
    );
  }

  if ( empty( $data['test_duration'] ) ) {
    $data = init_array_on_var_and_array_key($data, 'test_duration');
    $data['test_duration'] = array(
      'duration_test' => ''
    );
  }

/*END OF SETTINGS*/
	$output = '';
	$output .= '<a name="wlw-programs-module-wrapper-'. $key .'"></a>';
	$output .= '<div class="module-wrapper wlw-programs-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom wlw-programs-module-list-wrapper-'. $key .'">';
	$output .= '    <h3>'. $module_title . ( intval( $key ) > 0 ? ' #'.( intval( $key )+1 ) : '' ) .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
	$output .= '    <div class="inside">';

	$output .= '     <p>';
	$output .= '        <label><b>'. __( 'Programs Protection', 'balance' ) .':</b></label><br>';
	$output .=          radiobuttonlist_field( $data['wlw_programs_module'][ $key ]['programs_protected'], 'wlw_programs_module['.$key.'][programs_protected]', $programs_protected_choices, 'individually', true );
	$output .= '      </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Programs linked to Partner website', 'balance' ) .':</b></label>';
	$output .=              multi_autocomplete_field( $data['wlw_programs_module'][ $key ]['programs'], 'wlw_programs_module['.$key.'][programs]', $autocomplete_args_programs );
	$output .= '        </p>';

	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';


/*START OF SETTINGS*/

  $output .= '<style>
	.box-div-settings{border: solid 3px #cccccc;padding: 10px 20px 20px 20px;margin-bottom: 10px;}
	</style>';
 
  $output .= '<div class="module-wrapper wlw-programs-module-wrapper-0 hidden" data-visible-on="tab-7">';

  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '<h3>Settings<a class="description fright section-expander is-expanded" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';
  
  $output .= '  <b>Email settings</b>';
 
  $output .= '     <div class="box-div-settings"> ';
  $output .= '          <br><label><b>'. __( 'Send Email to Siteadmin and Superadmin', 'balance' ) .':</b></label>&nbsp;&nbsp;&nbsp;';
  //$output .=              radiobuttonlist_field( $data['siteadmin_superadmin_mail'], 'siteadmin_superadmin_mail', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'y', true);
  
  $siteadmin_superadmin_mail = $data['siteadmin_superadmin_mail'];
  if(isset($siteadmin_superadmin_mail) && $siteadmin_superadmin_mail=='on')$monstatus='checked';else $monstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="siteadmin_superadmin_mail" '.$monstatus.' style="margin-top:10px !important">'; 
 
  
  $output .= '      </div>';
  
  
  $output .= '  <b>Quiz settings</b>';
  $output .= '     <div class="box-div-settings"> ';
  $output .= '          <label><b>'. __( 'Quiz Duration', 'balance' ) .':</b></label><br>';
  $output .=              radiobuttonlist_field( $data['test_duration'], 'test_duration', array( '24' => __( '24 hours', 'balance' ), '48' => __( '48 hours', 'balance' ) ), 'y', true);
  $output .= '    </p>';

  $output .= '  
  <script>
  function disableTestTimes(el){
    var contact_page_status = document.getElementsByName(el.name);
    //alert(el.checked);
    if(el.checked == false){ 
    jQuery("#test_times").attr("disabled","true");
} else {
    jQuery("#test_times").removeAttr("disabled");
}
  }
    </script>';


  $output .= '    <p>';
  $output .= '      <label>';
  $output .= '        <span class="text">'. __( 'Limit ') .'</span>';
  $output .= '      </label>';
  $no_limit = $data['no_limit'];
  if(isset($no_limit) && $no_limit=='on')$no_limitstatus='checked';else $no_limitstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="no_limit" '.$no_limitstatus.' onclick="disableTestTimes(this)" style="padding-top:10px !important">';
  $output .= '      <label>';
  $output .= '      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
  $output .= '        <span class="text">'. __( 'Number of attempts: ') .'</span>';
  $output .= '      </label>';
  if(isset($data['test_duration']) && !isset($data['no_limit']) && $data['no_limit']!='on' || $data['no_limit']=='off') {
  $output .= '       <select id="test_times" class="single-drp " name="test_times" disabled><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option></select>';
} else {

  $output .=        dropdown_field( $data['test_times'], 'test_times', '', $array_of_types, '', false );
}
 // $output .= '    </div>';
 $output .= '    <br><br><hr><br>';

   //$output .= '     <div class="box-div-settings"> ';
  $output .= '          <label><b>'. __( 'Passing Percentage Criteria', 'balance' ) .':</b></label><br>';
  $output .= '      <label>';
 // $output .= '      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
  $output .= '        <span class="text">'. __( 'Passing Percentage: ') .'</span>';
  $output .= '      </label>';
  $output .=        dropdown_field( $data['passing_percent'], 'passing_percent', '', $passing_percent, '', false );

 $output .= '    <br><br><hr><br>';

  $output .= '     <p>';
  $output .= '          <label><b>'. __( 'Do you want to show Question and Answer after Quiz attempt', 'balance' ) .':</b></label>&nbsp;&nbsp;&nbsp;';
  //$output .=              radiobuttonlist_field( $data['quiz_q_n_a'], 'quiz_q_n_a', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance', 'checked' ) ), 'y',  true);
  $quiz_q_n_a = $data['quiz_q_n_a'];
  if(isset($quiz_q_n_a) && $quiz_q_n_a=='on')$monstatus='checked';else $monstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="quiz_q_n_a" '.$monstatus.' style="margin-top:10px !important">'; 
  
  
  $output .= '      </p>';
  $output .= '    </div>';
 /*
  $output .= '  <b>Contact settings</b>';
  $output .= '     <div class="box-div-settings"> ';
  $output .= '          <label><b>'. __( 'Do you want to show contact page in your website', 'balance' ) .':</b></label><br>';
  $output .=              radiobuttonlist_field( $data['contact_page'], 'contact_page', array( 'Yes' => __( 'Yes', 'balance' ), 'No' => __( 'No', 'balance' ) ), 'y', true);

$output .= '      <br>';
   $output .= '      </div>';
*/  
   

  $output .= '  </div>';
  $output .= '</div>';
  $output .= '</div>';

  /*END OF SETTINGS*/

/*START OF MESSAGES*/
  $output .= '<div class="module-wrapper wlw-programs-module-wrapper-0 hidden" data-visible-on="tab-9">';
$output .= '     <style>
                  input.toggleCheck {
                  -webkit-appearance: none !important;
                  appearance: none !important;
                  padding: 16px 30px !important;
                  border-radius: 16px !important;
                  background: radial-gradient(circle 12px, white 100%, transparent calc(100% + 1px)) #ccc -14px !important;
                  transition: 0.3s ease-in-out !important;
                }

                input.toggleCheck:checked {
                  background-color: dodgerBlue !important;
                  background-position: 14px !important;
				  
				 
                }
				 #messagetext_ifr,#thankyoutext_ifr{
					  height:400px !important;
				  }
				  #certificatetext_ifr,#certificateadmintext_ifr{
					  height:200px !important;
				  }
				   
                </style>
';

//START OF Registration MESSAGE
  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  
  $output .= '<h3>Registration Mail Message<a class="description fright section-expander is-expanded" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';  
  $output .= '     <p>';
  $output .= '          <label><b>Subject</b></label><br>';
  if(isset($data['register_subject']) && trim($data['register_subject'])!='') $register_subject = $data['register_subject'];
  else $register_subject = 'BALANCE Account Email Verification!';
  $output .= '             <input type="text" class="" name="register_subject" value="'.$register_subject.'">';
  $output .= '            <span><b>[Note: %username%,%site_title% are variables used for user name, your website title]</b>';
  $output .= '      </p>';

 
				
 
  $output .= '     <p>';
  $output .= '          <label><b>Enable Dynamic Message</b></label>&nbsp;&nbsp;&nbsp;';
  if(isset($data['register_message_status']) && $data['register_message_status']=='on'){
  $output .= '             <input type="checkbox" class="toggleCheck" name="register_message_status" checked>';
  } 
  else {
  $output .= '             <input type="checkbox" class="toggleCheck" name="register_message_status">';
  }
  $output .= '      </p>';
    //Textarea field
	
	$default_register_message_text = '<div><p>Thank you %username% for creating your new profile with BALANCE, in partnership with %site_title%. Please click the link below to verify your ownership of %user_email%.</p>

		<p>CLICK THIS LINK TO VERIFY: <BR><a href="%link%">%link%</a> <BR>
		<BR><BR>
		Best,<BR>

		BALANCE in partnership with %site_title%</p></div>';
	
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Message Text', 'balance' ) .':</b></label><br/>';
    $output .=                textarea_field( $data['register_message_text'], 'register_message_text', false, 10, $default_register_message_text, '', array('media_buttons' => false, 'quicktags' => true ) );
    $output .= '            <span><b>[Note: %username%,%user_email%,%site_title%,%site_url%,%link% are variables used for user name, user email, your website title, website url and verification link]</b>';
    $output .= '            </p>';    
  $output .= '  </div>';
  $output .= '</div>';
  //END OF Registration Mail Message

//START OF THANK YOU MESSAGE
  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '<h3>Thank You Pop up Message [Not email message]<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';  

  $output .= '     <p>';
  $output .= '          <label><b>Enable Dynamic Message</b></label>&nbsp;&nbsp;&nbsp;';
  if(isset($data['thankYou_status']) && $data['thankYou_status']=='on'){
  $output .= '             <input type="checkbox" class="toggleCheck" name="thankYou_status" checked>';
  }
  else {
  $output .= '             <input type="checkbox" class="toggleCheck" name="thankYou_status">';
  }
  $output .= '      </p>';
    //Textarea field
	
	$default_thankYou_text='<h1 class="text-info text-center">Thank you for verifying your email address</h1>
				<p>Thank you for completing your registration with BALANCE, in partnership with <span class="text-info"><b>%site_title%</b></span>. You’ve taken a step to financial fitness by being an active participant in your own financial wellness.</p>
				<p>Your registration gives you access to more BALANCE online education programs available on this website. Use your email address and BALANCE password to log in to a program when prompted.</p>
				<p>We hope you enjoy your experience with BALANCE, in partnership with <span class="text-info"><b>%site_title%</b></span>. We are here to help you achieve your financial success.</p>
				<p>Best,</p>
				<p>BALANCE in partnership with <span class="text-info"><b>%site_title%</b></span></p>';
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Message Text', 'balance' ) .':</b></label><br/>';
    $output .=                textarea_field( $data['thankYou_text'], 'thankYou_text', false, 15, $default_thankYou_text, '', array('media_buttons' => false, 'quicktags' => true ) );
	$output .= '            <span><b>[Note: %site_title%, %site_url% are variables used for your website title and website url .]</b>';
    $output .= '            </p>';
  $output .= '  </div>';
  $output .= '</div>';
  //END OF THANK YOU MESSAGE



//START OF WELCOME MESSAGE
  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '<h3>Welcome Message<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';  
  $output .= '     <p>';
  $output .= '          <label><b>Subject</b></label><br>';
  if(isset($data['message_subject']) && trim($data['message_subject'])!='') $message_subject = $data['message_subject'];
  else $message_subject = 'Welcome to BALANCE, in Partnership with %site_title%';
  $output .= '             <input type="text" class="" name="message_subject" value="'.$message_subject.'">';
  $output .= '            <span><b>[Note: %site_title%,%username% are variables used for your website title and User name]</b></span>';
  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '          <label><b>Enable Dynamic Message</b></label>&nbsp;&nbsp;&nbsp;';
  if(isset($data['message_status']) && $data['message_status']=='on'){
  $output .= '             <input type="checkbox" class="toggleCheck" name="message_status" checked>';
  } 
  else {
  $output .= '             <input type="checkbox" class="toggleCheck" name="message_status">';
  }
  $output .= '      </p>';
    //Textarea field
	
	$default_message_text = '<div><h1>Welcome to BALANCE, in partnership with %site_title%</h1>
					<p>Welcome and thank you for completing your registration with BALANCE, in partnership with %site_title%.</p>
					<p>
					You have taken a step to financial fitness by being an active participant in your own financial wellness.
					</p>
					<p>
					Your registration gives you access to more online education programs available on <a href="%site_url%">%site_url%</a> .
					</p>
					<p>
					Use your email address and BALANCE password to log in to a program when prompted.
					</p>
		<p>
					We hope you enjoy your experience with BALANCE, in partnership with %site_title% We are here to help you achieve your financial success.
				</p>
					<p>Start by visiting us at <a href="%site_url%">%site_url% </a> .
					</p>
				<p>	Best,</p>
				<p>	
					BALANCE, in partnership with %site_title%
					</p>
					</div>';
	
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Message Text', 'balance' ) .':</b></label><br/>';
    $output .=                textarea_field( $data['message_text'], 'message_text', false, 20, $default_message_text, '', array('media_buttons' => false, 'quicktags' => true ) );
    $output .= '            <span><b>[Note: %site_title%,%site_url% are variables used for your website title and website url]</b></span>';
    $output .= '            </p>';    
  $output .= '  </div>';
  $output .= '</div>';
  //END OF WELCOME MESSAGE


//START OF CERTIFICATE MESSAGE
  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '<h3>Certificate Message<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  //START OF CUSTOMER 
  $output .= '  <div class="inside" style="display: inline-block;width: 45.5%;">';
  $output .= '     <p>';
  $output .= '          <label><b>Customer</b></label><br><br>'; 
  $output .= '      </p>';
  $output .= '     <p>';
  $output .= '          <label><b>Subject</b></label><br>';
  if(isset($data['certificate_subject']) && trim($data['certificate_subject'])!='') $certificate_subject = $data['certificate_subject'];
  else $certificate_subject = '%quiztitle%';
  
  $output .= '             <input type="text" class="" name="certificate_subject" value="'.$certificate_subject.'">';
  $output .= '            <span><b>[Note: %quiztitle% variable used for Quiz Title.]</b></span><br><br>';	
  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '          <label><b>Enable Dynamic Message</b></label>&nbsp;&nbsp;&nbsp;';
  if(isset($data['certificate_status']) && $data['certificate_status']=='on'){
  $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_status" checked>';
  }
  else {
  $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_status">';
  }
  $output .= '      </p>';
    //Textarea field
	$default_certificate_text = '<div><p>Congratulations, you successfully completed this module! Your score was %score% (%correct_val%/%total_questions%)</p></div>';
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Message Text', 'balance' ) .':</b></label><br/>';
    $output .=                textarea_field( $data['certificate_text'], 'certificate_text', false, 10, $default_certificate_text, '', array('media_buttons' => false, 'quicktags' => true ) );
	$output .= '            <span><br><br><b>[Note: %score%, %correct_val%, %total_questions% variable used for your quiz score,correct answers and total questions.]</b></span>';
    $output .= '            </p>';
  $output .= '  </div>';
  //END OF CUSTOMER

  //START OF ADMIN/SUPERADMIN 
  $output .= '  <div class="inside" style="display: inline-block;width: 45.5%;">'; 
  $output .= '     <p>';
  $output .= '          <label><b>Admin / Superadmin</b></label><br><br>';
 $output .= '     </p>';
 $output .= '     <p>';
  $output .= '          <label><b>Subject</b></label><br>';
  if(isset($data['certificate_admin_subject']) && trim($data['certificate_admin_subject'])!='') $certificate_admin_subject = $data['certificate_admin_subject'];
  else $certificate_admin_subject = '%quiztitle%';
  $output .= '             <input type="text" class="" name="certificate_admin_subject" value="'.$certificate_admin_subject.'">';
  $output .= '            <span><b>[Note: %quiztitle% variable used for Quiz Title.]</b></span>';	
  $output .= '      </p>';

  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '          <label><b>Enable Dynamic Message</b></label>&nbsp;&nbsp;&nbsp;';
  if(isset($data['certificate_admin_status']) && $data['certificate_admin_status']=='on'){
  $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_admin_status" checked>';
  }
  else {
  $output .= '             <input type="checkbox" class="toggleCheck" name="certificate_admin_status">';
  }
  $output .= '      </p>';
    //Textarea field
	$default_certificate_admin_text = '<div><p>%user% has completed the quiz successfully.</p><BR>
				The score was %score% (%correct_val%/%total_questions%)
				<BR><BR>Website : %site_title%<BR>
				<BR><BR>Thanks<BR>
				Balancepro.org<BR></div>';
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Message Text', 'balance' ) .':</b></label><br/>';
    $output .=                textarea_field( $data['certificate_admin_text'], 'certificate_admin_text', false, 10, $default_certificate_admin_text, '', array('media_buttons' => false, 'quicktags' => true ) );
	
	$output .= '        <span><b>[Note: %user%, %site_title%, %score%, %correct_val%, %total_questions% variable used for your quiz user name, website title, score,correct answers and total questions.]</b></span>';
    $output .= '            </p>';
  $output .= '  </div>';
  //END OF ADMIN/SUPERADMIN


  $output .= '</div>';
  //END OF CERTIFICATE MESSAGE

$output .= '      </div>';
		  $output .= '  </div>';
 $output .= '</div>';
		
  $output .= '</div>';

  /*END OF MESSAGES*/


/*START OF CHAT*/

  $output .= '<style>
	.box-div-settings{border: solid 3px #cccccc;padding: 10px 20px 20px 20px;margin-bottom: 10px;}
	</style>';
 
  $output .= '<div class="module-wrapper wlw-programs-module-wrapper-0 hidden" data-visible-on="tab-10">';
  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '<h3>Chat<a class="description fright section-expander is-expanded" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';


  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  $output .= '        <br><p style="padding-left: 10px;">';
  $output .= '          <label><b>'. __( 'Do you want to Enable chat', 'balance' ) .':</b></label>&nbsp;&nbsp;&nbsp;';
if($data['wlw_general_info_module'][ $key ]['disable_chat'] == "on" || $data['wlw_general_info_module'][ $key ]['disable_chat'] == "Yes") {
$output .= '<input type="checkbox" class="toggleCheck" name="wlw_general_info_module['.$key.'][disable_chat]" checked="checked" style="padding-top:10px !important">';
  } else {
$output .= '<input type="checkbox" class="toggleCheck" name="wlw_general_info_module['.$key.'][disable_chat]" style="padding-top:10px !important">';
  }
  //$output .=              radiobuttonlist_field( $data['wlw_general_info_module'][ $key ]['disable_chat'], 'wlw_general_info_module['.$key.'][disable_chat]', array( 'y' => __( 'Yes', 'balance' ), 'n' => __( 'No', 'balance' ) ), 'n', true);
  
  $output .= '        </p>';
  $output .= '</div>';


$output .= '  <b>Chat time settings</b>';
  $output .= '     <div class="box-div-settings"> ';
  $array_of_times = array();
  for($i=1;$i<=12;$i++){
	  if($i<10) $v='0'.$i.':00'; else $v=$i.':00';
	  $array_of_times[] = array('id'=>$v,'title'=>$v);
	  if($i<10) $k='0'.$i.':30'; else $k=$i.':30';
	  $array_of_times[] = array('id'=>$k,'title'=>$k);
  }

 $time_am_pm = array(
    0 => array(
      'id' => 'AM',
      'title' => 'AM',
    ),
    1 => array(
      'id' => 'PM',
      'title' => 'PM',
    ));
  
  $output .= '     <BR>Our normal business hours are in (UTC-08:00) Pacific Time (US)<BR>';
  
  $output .= '      <BR><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monday:	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>';
  if(isset($data['timesettings']['mon_status']) && $data['timesettings']['mon_status']=='on')$monstatus='checked';else $monstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[mon_status]" '.$monstatus.' style="padding-top:10px !important">'; 
  $output .= dropdown_field( $data['timesettings']['mon_start'], 'timesettings[mon_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['mon_start_ap'], 'timesettings[mon_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['mon_end'], 'timesettings[mon_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['mon_end_ap'], 'timesettings[mon_end_ap]', '', $time_am_pm, '', false );
  
  $output .= '      <BR><BR> <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuesday:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>';
  if(isset($data['timesettings']['tue_status']) && $data['timesettings']['tue_status']=='on')$tuestatus='checked';else $tuestatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[tue_status]" '.$tuestatus.' style="padding-top:10px !important;
}">'; 
  $output .=dropdown_field( $data['timesettings']['tue_start'], 'timesettings[tue_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['tue_start_ap'], 'timesettings[tue_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['tue_end'], 'timesettings[tue_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['tue_end_ap'], 'timesettings[tue_end_ap]', '', $time_am_pm, '', false );
 
  $output .= '      <BR><BR> <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wednesday: &nbsp;&nbsp;&nbsp;&nbsp;</b>';
  if(isset($data['timesettings']['wed_status']) && $data['timesettings']['wed_status']=='on')$wedstatus='checked';else $wedstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[wed_status]" '.$wedstatus.' style="padding-top:10px !important">'; 
  $output .=dropdown_field( $data['timesettings']['wed_start'], 'timesettings[wed_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['wed_start_ap'], 'timesettings[wed_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['wed_end'], 'timesettings[wed_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['wed_end_ap'], 'timesettings[wed_end_ap]', '', $time_am_pm, '', false );
  
  $output .= '      <BR><BR> <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thursday:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> ';
  if(isset($data['timesettings']['thu_status']) && $data['timesettings']['thu_status']=='on')$thustatus='checked';else $thustatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[thu_status]" '.$thustatus.' style="padding-top:10px !important">'; 
  $output .= dropdown_field( $data['timesettings']['thu_start'], 'timesettings[thu_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['thu_start_ap'], 'timesettings[thu_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['thu_end'], 'timesettings[thu_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['thu_end_ap'], 'timesettings[thu_end_ap]', '', $time_am_pm, '', false );
  
  $output .= '      <BR><BR> <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Friday:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> ';
  if(isset($data['timesettings']['fri_status']) && $data['timesettings']['fri_status']=='on')$fristatus='checked';else $fristatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[fri_status]" '.$fristatus.' style="padding-top:10px !important">'; 
  $output .= dropdown_field( $data['timesettings']['fri_start'], 'timesettings[fri_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['fri_start_ap'], 'timesettings[fri_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['fri_end'], 'timesettings[fri_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['fri_end_ap'], 'timesettings[fri_end_ap]', '', $time_am_pm, '', false );
  
  $output .= '      <BR><BR> <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saturday:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> ';
  if(isset($data['timesettings']['sat_status']) && $data['timesettings']['sat_status']=='on')$satstatus='checked';else $satstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[sat_status]" '.$satstatus.' style="padding-top:10px !important">'; 
  $output .= dropdown_field( $data['timesettings']['sat_start'], 'timesettings[sat_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['sat_start_ap'], 'timesettings[sat_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['sat_end'], 'timesettings[sat_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['sat_end_ap'], 'timesettings[sat_end_ap]', '', $time_am_pm, '', false );
  
  $output .= '<BR><BR> <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sunday:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b> ';
  if(isset($data['timesettings']['sun_status']) && $data['timesettings']['sun_status']=='on')$sunstatus='checked';else $sunstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="timesettings[sun_status]" '.$sunstatus.' style="padding-top:10px !important">'; 
  $output .=dropdown_field( $data['timesettings']['sun_start'], 'timesettings[sun_start]', '', $array_of_times, '', false ).''.dropdown_field( $data['timesettings']['sun_start_ap'], 'timesettings[sun_start_ap]', '', $time_am_pm, '', false ).' to '.dropdown_field( $data['timesettings']['sun_end'], 'timesettings[sun_end]', '', $array_of_times, '', false ).dropdown_field( $data['timesettings']['sun_end_ap'], 'timesettings[sun_end_ap]', '', $time_am_pm, '', false );  
$output .= '</div>';
  
  
  //START OF CHAT MESSAGE
  $output .= '<div class="postbox postbox-custom types-selector-wrapper wlw-programs-module-list-wrapper-0">';
  //$output .= '<h3>Chat Message<a class="description fright section-expander" data-toggle-title="Expand" href="javascript:;">Collapse</a></h3> ';
  $output .= '  <div class="inside">';  
  $output .= '     <p>';
  $tooltip_text=$data['tooltip_text'];
  $output .= '<label><b>Tooltip Text</b></label><br>';
  $output .= '             <input type="text" class="" disabled=disabled name="tooltip_text" value="'.$tooltip_text.'">';
  $output .= '  <span><b>[Note: This text will appear as tooltip(title) after Available/Unavailable.]</b></span>';
  $output .= '      </p>';
  
  $output .= '     <p>';
  $output .= '<label><b>Message Text:</b></label><br>';
   if(isset($data['chat_message']) && trim($data['chat_message'])!='') $chat_message = $data['chat_message'];
   else $chat_message = '<p>BALANCE chat is currently unavailable. Our normal business hours are in Pacific Time (US):<br> %timesettings%</p>';
  
  $output .=  textarea_field( $data['chat_message'], 'chat_message', false, 10, $chat_message, '', array('media_buttons' => false, 'quicktags' => true ) );
  $output .= '  <span><b>[Note: This text message will appear in new window when chat is unavailable. %sitetitle%  and %timesettings% as specified in settings tab.]</b></span>';
  $output .= '  </p>';  
  $output .= '  </div>';
  $output .= '</div>';
  //END OF CHAT MESSAGE

  

  $output .= '  </div>';
  $output .= '</div>';
  $output .= '</div>';

  /*END OF SETTINGS*/


	return $output;
}
?>
