<?php
/*
 *  WP Edit module: M41b
 *  Description: Module section: M41b - repater of M31 module
 */

function m41b_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	$output = simple_edit_module( $key, 'm41b', array(
			array(
				'type' => 'repeater',
				'name' => 'sections',
				'label' => 'Section',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'text',
						'name' => 'title',
						'label' => 'Title',
						'default_value' => ''
					),
					array(
						'type' => 'textarea',
						'name' => 'copy',
						'label' => 'Copy',
						'default_value' => '',
						'additional_params' => array(
							true,
							0,
							'',
							'',
							array( 'media_buttons' => false, 'quicktags' => true )
						)
					),
					array(
						'type' => 'radiobuttonlist',
						'name' => 'leftright',
						'label' => 'Link text',
						'default_value' => array(),
						'additional_params' => array(
							array(
								'left' => __( 'Left', 'balance' ),
								'right'  => __( 'Right', 'balance' ),
							),
							'left',
							true
						)
					),
					array(
						'type' => 'upload',
						'name' => 'image',
						'label' => 'Image',
						'default_value' => '',
						'additional_params' => array(
							'',
							'Image',
						)
					),
					array(
						'type' => 'text',
						'name' => 'buttontext',
						'label' => 'Button text',
						'default_value' => ''
					),
					array(
						'type' => 'links_to',
						'name' => 'linkfield',
						'label' => 'Button link',
						'default_value' => array(),
						'additional_params' => array(
							'',
							'Page',
						)
					),
				)
			)
		), $data, $module_title, $visible_on );

	return $output;
}

?>
