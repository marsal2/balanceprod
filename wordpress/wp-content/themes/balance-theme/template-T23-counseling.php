<?php
/*
Template Name: T23: Counseling
Modules: {"m34[0]":{"name":"M34: Section"},"m18[0]":{"name":"M18: Simple form"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'counseling';
get_custom_data();

get_header();

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}

/* CONTACT FORM */
if (!empty($data['m18_module']) && !empty($data['m18_module'][0])) {
    echo do_shortcode($data['m18_module'][0]['formhtml']);
}

get_footer();
?>
