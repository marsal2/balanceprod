<?php
/**
 * Edit user administration panel.
 *
 * @package WordPress
 * @subpackage Administration
 */

/** WordPress Administration Bootstrap */
require_once __DIR__ . '/admin.php';
global $wpdb;
	require_once dirname( __DIR__ ) . '/wp-load.php';

	$wp_chat_data = $wpdb->get_results( "SELECT * FROM wp_chat_setting" );
/*print_r($wp_chat_data);*/
$chat_value_data = unserialize($wp_chat_data[0]->chat_value);
/*echo "<pre>";
print_r($chat_value_data);
echo "</pre>";*/
/*
if ( current_user_can( 'edit_users' ) && ! is_user_admin() ) {
	$parent_file = 'users.php';
} else {
	$parent_file = 'profile.php';
}*/

$profile_help = '<p>' . __( 'Your profile contains information about you (your &#8220;account&#8221;) as well as some personal options related to using WordPress.' ) . '</p>' .
	'<p>' . __( 'You can change your password, turn on keyboard shortcuts, change the color scheme of your WordPress administration screens, and turn off the WYSIWYG (Visual) editor, among other things. You can hide the Toolbar (formerly called the Admin Bar) from the front end of your site, however it cannot be disabled on the admin screens.' ) . '</p>' .
	'<p>' . __( 'You can select the language you wish to use while using the WordPress administration screen without affecting the language site visitors see.' ) . '</p>' .
	'<p>' . __( 'Your username cannot be changed, but you can use other fields to enter your real name or a nickname, and change which name to display on your posts.' ) . '</p>' .
	'<p>' . __( 'You can log out of other devices, such as your phone or a public computer, by clicking the Log Out Everywhere Else button.' ) . '</p>' .
	'<p>' . __( 'Required fields are indicated; the rest are optional. Profile information will only be displayed if your theme is set up to do so.' ) . '</p>' .
	'<p>' . __( 'Remember to click the Update Profile button when you are finished.' ) . '</p>';

get_current_screen()->add_help_tab(
	array(
		'id'      => 'overview',
		'title'   => __( 'Chat' ),
		'content' => $profile_help,
	)
);

get_current_screen()->set_help_sidebar(
	'<p><strong>' . __( 'For more information:' ) . '</strong></p>' .
	'<p>' . __( '<a href="https://wordpress.org/support/article/users-your-profile-screen/">Documentation on User Profiles</a>' ) . '</p>' .
	'<p>' . __( '<a href="https://wordpress.org/support/">Support</a>' ) . '</p>'
);

$user_can_edit = current_user_can( 'edit_posts' ) || current_user_can( 'edit_pages' );

/**
 * Filters whether to allow administrators on Multisite to edit every user.
 *
 * Enabling the user editing form via this filter also hinges on the user holding
 * the 'manage_network_users' cap, and the logged-in user not matching the user
 * profile open for editing.
 *
 * The filter was introduced to replace the EDIT_ANY_USER constant.
 *
 * @since 3.0.0
 *
 * @param bool $allow Whether to allow editing of any user. Default true.
 */
if ( is_multisite()
	&& ! current_user_can( 'manage_network_users' )
	&& $user_id !== $current_user->ID
	&& ! apply_filters( 'enable_edit_any_user_configuration', true )
) {
	wp_die( __( 'Sorry, you are not allowed to edit this user.' ) );
}
		require_once ABSPATH . 'wp-admin/admin-header.php';
		?>


		<?php if ( isset( $_GET['updated'] ) ) : ?>
			<div id="message" class="updated notice is-dismissible">
				<?php if ( IS_PROFILE_PAGE ) : ?>
					<p><strong><?php _e( 'Profile updated.' ); ?></strong></p>
				<?php else : ?>
					<p><strong><?php _e( 'User updated.' ); ?></strong></p>
				<?php endif; ?>
				<?php if ( $wp_http_referer && false === strpos( $wp_http_referer, 'user-new.php' ) && ! IS_PROFILE_PAGE ) : ?>
					<p><a href="<?php echo esc_url( wp_validate_redirect( esc_url_raw( $wp_http_referer ), self_admin_url( 'users.php' ) ) ); ?>"><?php _e( '&larr; Go to Users' ); ?></a></p>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( isset( $_GET['error'] ) ) : ?>
			<div class="notice notice-error">
			<?php if ( 'new-email' === $_GET['error'] ) : ?>
				<p><?php _e( 'Error while saving the new email address. Please try again.' ); ?></p>
			<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( isset( $errors ) && is_wp_error( $errors ) ) : ?>
			<div class="error">
				<p><?php echo implode( "</p>\n<p>", $errors->get_error_messages() ); ?></p>
			</div>
		<?php endif; ?>

				<h2><?php _e( 'Chat & Contact' ); ?></h2>
<style type="text/css">
input.toggleCheck {
	    -webkit-appearance: none !important;
    appearance: none !important;
    padding: 16px 30px !important;
    border-radius: 16px !important;
    background: radial-gradient(circle 12px, white 100%, transparent calc(100% + 1px)) #ccc -14px !important;
    transition: 0.3s ease-in-out !important;
}

input.toggleCheck:checked {
  background-color: dodgerBlue !important;
  background-position: 14px !important;
}
input[type=text], textarea {
	width: 80%;
}
</style>
<script>
  function disableContactForms(el){
  	var contact_page_status = document.getElementsByName(el.name);
  //	alert(contact_page_status);
  	//alert(el.checked);
  	if(el.checked == false){
  		//alert("1");
    //jQuery("contact_form").checked = "";  
    jQuery("#contact_forms").removeAttr("checked"); 
    jQuery("#contact_forms").attr("disabled","true"); 
    jQuery("#contact_addresses").removeAttr("checked"); 
    jQuery("#contact_addresses").attr("disabled","true");
} else {
	//alert("2"); 
    jQuery("#contact_forms").removeAttr("disabled");
    jQuery("#contact_addresses").removeAttr("disabled");
}
  /*if (el.checked == "false") {
    //document.getElementsByName("contact_form").removeAttr("disabled");
    document.getElementsByName("contact_form")[0].disabled = true;
  } else {
    document.getElementsByName("contact_form")[0].disabled = false;
    //document.getElementsByName("contact_form").removeAttr("checked");
  }*/
  }
  	</script>
			<form action="addchat.php" method="post">
				<table class="form-table" role="presentation">
					<tr class="user-nickname-wrap">
						<th colspan="2"><label for="nickname" style="font-size: 18px;"><?php _e( 'Contact Setting' ); ?></label></th>
					</tr>
					<tr class="user-nickname-wrap">
						<th style="padding:10px 10px 0px 0px;" colspan="2"><label for="nickname" style="font-size: 15px;"><?php _e( 'Do you want to enable Contact Page' ); ?></label></th>
					</tr>
					<tr>
						<td>
							<?php
								if($chat_value_data['contact_page'] == "on") {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_page" checked="checked" style="padding-top:10px !important" onclick="disableContactForms(this)">
							<?php
								} else {
							?>
								  <input type="checkbox" class="toggleCheck" name="contact_page" style="padding-top:10px !important" onclick="disableContactForms(this)">
							<?php } ?>
						</td>
					</tr>
					<tr class="user-nickname-wrap">
						<th style="padding:10px 10px 0px 0px;" colspan="2"><label for="nickname" style="font-size: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php _e( 'Do you want to enable Contact Address' ); ?></label></th>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php
								if(isset($chat_value_data['contact_page']) && isset($chat_value_data['contact_address'])) {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_address" id="contact_addresses" checked="checked" style="padding-top:10px !important">
							<?php
								} else if(isset($chat_value_data['contact_page'])=='' && isset($chat_value_data['contact_address'])=='1') {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_address" id="contact_addresses" disabled="disabled" style="padding-top:10px !important">
							<?php
								} else if(isset($chat_value_data['contact_page'])=='' && isset($chat_value_data['contact_address'])=='') {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_address" id="contact_addresses" disabled="disabled" style="padding-top:10px !important">
							<?php
								} else {
							?>
								  <input type="checkbox" class="toggleCheck" name="contact_address" id="contact_addresses" style="padding-top:10px !important">
							<?php } ?>
						</td>
					</tr>
					<tr class="user-nickname-wrap">
						<th style="padding:10px 10px 0px 0px;" colspan="2"><label for="nickname" style="font-size: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php _e( 'Do you want to enable Contact Form' ); ?></label></th>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php
								if(isset($chat_value_data['contact_page']) && isset($chat_value_data['contact_form'])) {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_form" id="contact_forms" checked="checked" style="padding-top:10px !important">
							<?php
								} else if(isset($chat_value_data['contact_page'])=='' && isset($chat_value_data['contact_form'])=='1') {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_form" id="contact_forms" disabled="disabled" style="padding-top:10px !important">
							<?php
								} else if(isset($chat_value_data['contact_page'])=='' && isset($chat_value_data['contact_form'])=='') {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_form" id="contact_forms" disabled="disabled" style="padding-top:10px !important">
							<?php
								} else {
							?>
								  <input type="checkbox" class="toggleCheck" name="contact_form" id="contact_forms" style="padding-top:10px !important">
							<?php } ?>
						</td>
					</tr>
					<tr class="user-nickname-wrap">
						<th style="padding:10px 10px 0px 0px;" colspan="2"><label for="nickname" style="font-size: 15px;"><?php _e( 'Do you want to enable Contact Number' ); ?></label></th>
					</tr>
					<tr>
						<td>
							<?php
								if($chat_value_data['contact_number'] == "on") {
							?>

								  <input type="checkbox" class="toggleCheck" name="contact_number" checked="checked" style="padding-top:10px !important">
							<?php
								} else {
							?>
								  <input type="checkbox" class="toggleCheck" name="contact_number" style="padding-top:10px !important">
							<?php } ?>
						</td>
					</tr>
					<tr class="user-nickname-wrap">
						<th colspan="2"><label for="nickname" style="font-size: 18px;"><?php _e( 'Chat Setting' ); ?></label></th>
					</tr>
					<tr class="user-nickname-wrap">
						<th style="padding:10px 10px 0px 0px;" ><label for="nickname" style="font-size: 15px;"><?php _e( 'Do you want to Enable Chat' ); ?></label></th>
					</tr>
					<tr>
						<td>
							<?php
								if($chat_value_data['disable_chat'] == "on") {
							?>

								  <input type="checkbox" class="toggleCheck" name="disable_chat" checked="checked" style="padding-top:10px !important">
							<!--<input type="radio" id="Yes" name="disable_chat" checked value="y">
							<label for="Yes">Yes</label>-->
							<?php
								} else {
							?>
								  <input type="checkbox" class="toggleCheck" name="disable_chat" style="padding-top:10px !important">
							<!--<input type="radio" id="Yes" name="disable_chat" value="y">
							<label for="Yes">Yes</label>-->
							<?php } ?>


							<!--<p>
							<?php
								if($chat_value_data['disable_chat'] == "n") {
							?>
							<input type="radio" id="No" name="disable_chat" checked value="n">
							<label for="No">No</label>
							<?php
								} else {
							?>
							<input type="radio" id="No" name="disable_chat" value="n">
							<label for="No">No</label>
							<?php
								} 
							?></p>-->
						</td>
					</tr>
					<br>
					<tr class="user-user-login-wrap">
						<th style="font-size: 15px;">Chat time settings</th>
					</tr>
					<tr>
       
					  <?php
					  $array_of_times = array();
					  for($i=1;$i<=12;$i++){
						  if($i<10) $v='0'.$i.':00'; else $v=$i.':00';
						  $array_of_times[] = array('id'=>$v,'title'=>$v);
						  if($i<10) $k='0'.$i.':30'; else $k=$i.':30';
						  $array_of_times[] = array('id'=>$k,'title'=>$k);
					  }

					 $time_am_pm = array(
					    0 => array(
					      'id' => 'AM',
					      'title' => 'AM',
					    ),
					    1 => array(
					      'id' => 'PM',
					      'title' => 'PM',
					    ));
					  ?>
					       <td colspan="4">Our normal business hours are in (UTC-08:00) Pacific Time (US)</td>
					  </tr>
				        <tr>
				        	<td><b>&nbsp;&nbsp;&nbsp;&nbsp;Monday:	 </b></td>
				        	<td>
							  <?php if(isset($chat_value_data['mon_status']) && $chat_value_data['mon_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="mon_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="mon_status" style="padding-top:10px !important">
								<?php } ?>

								<select name="mon_start" id="mon_start">
									<?php 
										if($chat_value_data['mon_start'] != ''){
											echo '<option value="'.$chat_value_data['mon_start'].'">'.$chat_value_data['mon_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="mon_start_ap" id="mon_start_ap">
									<?php 
										if($chat_value_data['mon_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['mon_start_ap'].'">'.$chat_value_data['mon_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['mon_start_ap'] != '' && $chat_value_data['mon_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['mon_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="mon_end" id="mon_end">
									<?php 
										if($chat_value_data['mon_end'] != ''){
											echo '<option value="'.$chat_value_data['mon_end'].'">'.$chat_value_data['mon_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="mon_end_ap" id="mon_end_ap">
									<?php 
										if($chat_value_data['mon_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['mon_end_ap'].'">'.$chat_value_data['mon_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['mon_end_ap'] != '' && $chat_value_data['mon_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['mon_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
							</td>
					  	</tr>
				  	<tr>
					  	 <td><b>&nbsp;&nbsp;&nbsp;&nbsp;Tuesday: </b></td>
					  	 <td>
							  <?php if(isset($chat_value_data['tue_status']) && $chat_value_data['tue_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="tue_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="tue_status" style="padding-top:10px !important">
								<?php } ?>

								<select name="tue_start" id="tue_start">
									<?php 
										if($chat_value_data['tue_start'] != ''){
											echo '<option value="'.$chat_value_data['tue_start'].'">'.$chat_value_data['tue_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="tue_start_ap" id="tue_start_ap">
									<?php 
										if($chat_value_data['tue_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['tue_start_ap'].'">'.$chat_value_data['tue_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['tue_start_ap'] != '' && $chat_value_data['tue_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['tue_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="tue_end" id="tue_end">
									<?php 
										if($chat_value_data['tue_end'] != ''){
											echo '<option value="'.$chat_value_data['tue_end'].'">'.$chat_value_data['tue_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="tue_end_ap" id="tue_end_ap">
									<?php 
										if($chat_value_data['tue_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['tue_end_ap'].'">'.$chat_value_data['tue_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['tue_end_ap'] != '' && $chat_value_data['tue_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['tue_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
						</td>
			        </tr>
			        <tr>
			         	<td><b>&nbsp;&nbsp;&nbsp;&nbsp;Wednesday: </b></td>
			         	<td>
							  <?php if(isset($chat_value_data['wed_status']) && $chat_value_data['wed_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="wed_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="wed_status" style="padding-top:10px !important">
								<?php } ?>

								<select name="wed_start" id="wed_start">
									<?php 
										if($chat_value_data['wed_start'] != ''){
											echo '<option value="'.$chat_value_data['wed_start'].'">'.$chat_value_data['wed_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="wed_start_ap" id="wed_start_ap">
									<?php 
										if($chat_value_data['wed_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['wed_start_ap'].'">'.$chat_value_data['wed_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['wed_start_ap'] != '' && $chat_value_data['wed_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['wed_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="wed_end" id="wed_end">
									<?php 
										if($chat_value_data['wed_end'] != ''){
											echo '<option value="'.$chat_value_data['wed_end'].'">'.$chat_value_data['wed_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="wed_end_ap" id="wed_end_ap">
									<?php 
										if($chat_value_data['wed_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['wed_end_ap'].'">'.$chat_value_data['wed_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['wed_end_ap'] != '' && $chat_value_data['wed_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['wed_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
						</td>
				  	</tr>
			        <tr>
			         	<td><b>&nbsp;&nbsp;&nbsp;&nbsp;Thursday: </b> </td>
			         	<td>
							  <?php if(isset($chat_value_data['thu_status']) && $chat_value_data['thu_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="thu_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="thu_status" style="padding-top:10px !important">
								<?php } ?>

								<select name="thu_start" id="thu_start">
									<?php 
										if($chat_value_data['thu_start'] != ''){
											echo '<option value="'.$chat_value_data['thu_start'].'">'.$chat_value_data['thu_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="thu_start_ap" id="thu_start_ap">
									<?php 
										if($chat_value_data['thu_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['thu_start_ap'].'">'.$chat_value_data['thu_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['thu_start_ap'] != '' && $chat_value_data['thu_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['thu_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="thu_end" id="thu_end">
									<?php 
										if($chat_value_data['thu_end'] != ''){
											echo '<option value="'.$chat_value_data['thu_end'].'">'.$chat_value_data['thu_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="thu_end_ap" id="thu_end_ap">
									<?php 
										if($chat_value_data['thu_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['thu_end_ap'].'">'.$chat_value_data['thu_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['thu_end_ap'] != '' && $chat_value_data['thu_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['thu_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
						</td>
			        </tr>
			        <tr>
			         	<td><b>&nbsp;&nbsp;&nbsp;&nbsp;Friday: </b> </td>
			         	<td>
							  <?php if(isset($chat_value_data['fri_status']) && $chat_value_data['fri_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="fri_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="fri_status" style="padding-top:10px !important">
								<?php } ?>

								<select name="fri_start" id="fri_start">
									<?php 
										if($chat_value_data['fri_start'] != ''){
											echo '<option value="'.$chat_value_data['fri_start'].'">'.$chat_value_data['fri_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="fri_start_ap" id="fri_start_ap">
									<?php 
										if($chat_value_data['fri_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['fri_start_ap'].'">'.$chat_value_data['fri_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['fri_start_ap'] != '' && $chat_value_data['fri_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['fri_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="fri_end" id="fri_end">
									<?php 
										if($chat_value_data['fri_end'] != ''){
											echo '<option value="'.$chat_value_data['fri_end'].'">'.$chat_value_data['fri_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="fri_end_ap" id="fri_end_ap">
									<?php 
										if($chat_value_data['fri_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['fri_end_ap'].'">'.$chat_value_data['fri_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['fri_end_ap'] != '' && $chat_value_data['fri_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['fri_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
						</td>
			        </tr>
			        <tr>
			        	 <td><b>&nbsp;&nbsp;&nbsp;&nbsp;Saturday: </b></td>
			        	 <td> 
							  <?php if(isset($chat_value_data['sat_status']) && $chat_value_data['sat_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="sat_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="sat_status" style="padding-top:10px !important">
								<?php } ?>


								<select name="sat_start" id="sat_start">
									<?php 
										if($chat_value_data['sat_start'] != ''){
											echo '<option value="'.$chat_value_data['sat_start'].'">'.$chat_value_data['sat_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="sat_start_ap" id="sat_start_ap">
									<?php 
										if($chat_value_data['sat_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['sat_start_ap'].'">'.$chat_value_data['sat_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['sat_start_ap'] != '' && $chat_value_data['sat_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['sat_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="sat_end" id="sat_end">
									<?php 
										if($chat_value_data['sat_end'] != ''){
											echo '<option value="'.$chat_value_data['sat_end'].'">'.$chat_value_data['sat_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="sat_end_ap" id="sat_end_ap">
									<?php 
										if($chat_value_data['sat_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['sat_end_ap'].'">'.$chat_value_data['sat_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['sat_end_ap'] != '' && $chat_value_data['sat_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['sat_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select> 
						</td>
					 </tr>
					 <tr>
					 	<td><b>&nbsp;&nbsp;&nbsp;&nbsp;Sunday: </b> </td>
					 	<td>
							  <?php if(isset($chat_value_data['sun_status']) && $chat_value_data['sun_status']=='on'){ ?>
								  <input type="checkbox" class="toggleCheck" name="sun_status" checked="checked" style="padding-top:10px !important">
								<?php } else { ?>
									<input type="checkbox" class="toggleCheck" name="sun_status" style="padding-top:10px !important">
								<?php } ?>

								<select name="sun_start" id="sun_start">
									<?php 
										if($chat_value_data['sun_start'] != ''){
											echo '<option value="'.$chat_value_data['sun_start'].'">'.$chat_value_data['sun_start'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="sun_start_ap" id="sun_start_ap">
									<?php 
										if($chat_value_data['sun_start_ap'] != ''){
											echo '<option value="'.$chat_value_data['sun_start_ap'].'">'.$chat_value_data['sun_start_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['sun_start_ap'] != '' && $chat_value_data['sun_start_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['sun_start_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
								to
								<select name="sun_end" id="sun_end">
									<?php 
										if($chat_value_data['sun_end'] != ''){
											echo '<option value="'.$chat_value_data['sun_end'].'">'.$chat_value_data['sun_end'].'</option>';
										}
									?>
									<?php
										foreach ($array_of_times as $key_times => $value_times) {
											?>
												<option value="<?php echo $value_times['title']; ?>"><?php echo $value_times['title']; ?></option>
											<?php
										}
									?>
								</select>
								<select name="sun_end_ap" id="sun_end_ap">
									<?php 
										if($chat_value_data['sun_end_ap'] != ''){
											echo '<option value="'.$chat_value_data['sun_end_ap'].'">'.$chat_value_data['sun_end_ap'].'</option>';
										}
									?>
									<?php
										foreach ($time_am_pm as $key_time => $value_time) {
											if($chat_value_data['sun_end_ap'] != '' && $chat_value_data['sun_end_ap'] != $value_time['title']){
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										} if($chat_value_data['sun_end_ap'] == '') {
											?>
												<option value="<?php echo $value_time['title']; ?>"><?php echo $value_time['title']; ?></option>
											<?php
										}
										}
									?>
								</select>
						</td>
					 </tr>

					<tr class="user-first-name-wrap">
						<td colspan="4">
							<label for="first_name"><b style="font-size: 15px;"><?php _e( 'Chat Message' ); ?></b></label>
						
							<p>
								<?php $tooltip_text=($chat_value_data['tooltip_text']) != '' ? $chat_value_data['tooltip_text'] : '';?>
								<label>Tooltip Text</label><br>
								<?php
									echo '<input type="text" class="" name="tooltip_text" disabled="disabled" value="'.$tooltip_text.'">'; 
								?>
								       <br>
								<span><b>[Note: This text will appear as tooltip(title) after Available/Unavailable.]</b></span>
								</p>
						</td>
					</tr>

					<tr class="user-last-name-wrap">
						<td colspan="4">
							<label for="last_name" style="font-size: 15px;font-weight: 600;"><?php _e( 'Message Text' ); ?></label>
							<p>
								<?php if ($chat_value_data['description'] != ''){
									$chat_message = $chat_value_data['description'];
						  		echo '<textarea name="description" id="description" rows="5" cols="30">'.$chat_message.'</textarea>'; 
						} else {
						  		echo '<textarea name="description" id="description" rows="5" cols="30"><p>Balancepro chat is currently unavailable. Our normal business hours are in (UTC-08:00) Pacific Time (US):</p><p> You may call us at 1-888-456-2227 during normal business hours.</p></textarea>'; 
						}
						  		?>
						   <br>
						    <span><b>[Note: This text message will appear in new window when chat is unavailable.]</b></span>
						    </p>
						</td>
					</tr>

					
				</table>
				<input type="submit" class="button button-primary button-large"  value="UPDATE">
			</form>
<?php
require_once ABSPATH . 'wp-admin/admin-footer.php';
