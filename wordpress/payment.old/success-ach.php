<?php
  if(session_id() == '') {
    session_start();
  }

  require_once('dwolla.php');

  $transferRequest = doDwolla('createTransfer', [
    'funding-source' => $_GET['source'],
    'amount' => $_SESSION['dwolla-transfer-amount']
  ]);

  $transfer = doDwolla('getTransferById', [
    'transfer-id' => $transferRequest
  ]);

  if(wasError() !== false) {
    renderError();
  }

  $ach_client_email_title = 'Your Transaction is ' . $transfer->status;
  $ach_client_email_body .= '<table style="width: 100%; font-family: \'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif; color: #737373; border: 1px solid #e4e4e4;" border="1" cellspacing="0" cellpadding="6">';
  $ach_client_email_body .= '  <thead>';
  $ach_client_email_body .= '    <tr>';
  $ach_client_email_body .= '      <th style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;" scope="row">ID:</th>';
  $ach_client_email_body .= '      <th style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;" scope="row">' . $transfer->id . '</th>';
  $ach_client_email_body .= '    </tr>';
  $ach_client_email_body .= '  </thead>';
  $ach_client_email_body .= '  <tbody>';
  $ach_client_email_body .= '    <tr>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Amount:</td>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><strong>' . $transfer->amount->value . ' (' . strtoupper($transfer->amount->currency) . ')' . '</strong></td>';
  $ach_client_email_body .= '    </tr>';
  $ach_client_email_body .= '    <tr>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Created:</td>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><strong>' . $transfer->created->format('Y-m-d H:i:s') . '</strong></td>';
  $ach_client_email_body .= '    </tr>';
  $ach_client_email_body .= '    <tr>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Description:</td>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><strong>' . $transfer->metadata->description . '</strong></td>';
  $ach_client_email_body .= '    </tr>';
  $ach_client_email_body .= '    <tr>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Client Number:</td>';
  $ach_client_email_body .= '      <td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><strong>' . $transfer->metadata->client_number . '</strong></td>';
  $ach_client_email_body .= '    </tr>';
  $ach_client_email_body .= '  </tbody>';
  $ach_client_email_body .= '</table>';

  require_once('email.php');
  $config = require_once('config.php');
  $use_config = $config['DEFAULT'];

  function getAchClientEmail() {
    global $ach_client_email_title, $ach_client_email_body;
    ob_start();
    require_once('email.ach.client.php');
    return ob_get_clean();
  }

  sendEmail(
    array($_SESSION['dwolla-customer-email']),
    sprintf($use_config['PMT_SUBJECT_ACH_CLIENT'],$transfer->id),
    getAchClientEmail()
  );

  unset($_SESSION['dwolla-access-token']);
  unset($_SESSION['dwolla-transfer-amount']);
  unset($_SESSION['dwolla-customer-email']);

?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Balance bank account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
    </head>
    <body>

      <!-- Header -->
      <header id="header">
        <div style="background-image: url(https://www.cccssf.org/bankruptcy/images/header_background.png);background-repeat: no-repeat; background-size: cover; background-position: 50% 0; max-height: 50px; letter-spacing: -4px; text-align: right; padding: 10px 5px;" class="header-t">
          <div class="container">
            <div class="row">
              <!-- search form-->
              <ul class="contact-block" style="text-transform: uppercase; letter-spacing: 0; line-height: 0; font-size: 0; display: inline-block; vertical-align: middle; letter-spacing: 1px; margin-right: 11%;">
                <li><a href="tel:8007777526"><span class="icon-phone"></span>800.777.PLAN (7526)</a></li>
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
              <div class="navbar-header" style="margin:30px 0; margin-left: 11%;">
                <a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo.svg" width="238" height="51" alt="balance"></a>
              </div>
            </div>
          </div>
        </nav>
      </header>

      <!-- Main -->
      <div class="main">
        <h1><?php echo $ach_client_email_title; ?></h1>
        <p>Information below was also emailed to you.</p>
        <p>
          <br><br>
        </p>
        <?php echo $ach_client_email_body; ?>
        <p>
          <br><br>
          <a class="btn" href="https://www.balancepro.org/">Go To Balancepro.org Website</a>
        </p>
      </div>

      <!-- Footer -->
      <footer id="footer" style="background-image: url(https://www.cccssf.org/bankruptcy/images/footer_background.png);">
        <div class="container">
          <div class="row">
            <div class="logo-holder"><a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo-primary.svg" width="198" height="44" alt="Balance"></a></div>

          </div>
        </div>
      </footer>

    </body>
</html>
