<?php
if(session_id() == '') {
  session_start();
}
require_once('security.php');
// Check if a form has been sent
$postedToken = filter_input(INPUT_POST, 'token');
if(!empty($postedToken)) {
  if(!isTokenValid($postedToken)){
    echo '<script>window.history.back();</script>';
    exit;
  }
} else {
  echo '<script>window.history.back();</script>';
  exit;
}
require_once('email.php');
$config = require_once('config.php');

$ach1_email = '';

$email_key_map = array(
  'accountholder-name' => 'Name',
  'accountholder-surname' => 'Surname',
  'accountholder-address1' => 'Address 1',
  'accountholder-address2' => 'Adress 2',
  'accountholder-city' => 'City',
  'accountholder-state' => 'State',
  'accountholder-zip' => 'Zip',
  'accountholder-phone' => 'Phone',
  'email-certificate' => 'Email Certificate',
  'fax-certificate' => 'Fax Certificate',
  'email' => 'Email',
  'recipient-fax' => 'Recipient\'s Fax',
  'recipient-name' => 'Recipient\'s Name',
  'recipient-phone' => 'Recipient\'s Phone',
  'payment' => 'Payment For',
  'accountholder-bankruptcy' => 'Bankruptcy Case #',
  'accountholder-routing' => 'Routing Number',
  'accountholder-account' => 'Account Number',
  'accountholder-type' => 'Type',
  'accountholder-amount' => 'Amount'
);

// check if any additional fields passed (set in config)
if ( !empty( $_POST['additional_field'] ) ) {
  $additional_post_fields = $_POST['additional_field'];
  if ( is_array( $additional_post_fields ) ) {
    foreach ($additional_post_fields as $key => $value) {
      // create and email key map for additional field
      $email_key_map[$key] = ucwords(str_replace('_', ' ', $key));
      // modify the POST so that it contains the additional field value
      $_POST[$key] = $_POST['additional_field'][$key];
    }
  }
  // unset primary additional field from the POST so that it is not evaluated further
  unset($_POST['additional_field']);
}

function getAch1Email() {
  global $email_key_map, $ach1_email;
  $tbody = array();
  foreach ($email_key_map as $key => $value) {
    // Set email content labels and values based on $_GET['preset']
    if( isset($_POST[$key]) ) {
      $label = $_POST[$key] ? $_POST[$key] : 'No';
      $tbody[] = join('', [
        '<th style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;" colspan="2" scope="row">',
        $value,
        '</th>',
        '<td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">',
        $label,
        '</td>'
      ]);
    }
  }
  $ach1_email = '<tr>' . join('</tr><tr>', $tbody) . '</tr>';
  ob_start();
  require_once('email.ach.admin.php');
  return ob_get_clean();
}

require_once('dwolla.php');

$email = $_POST['email'];
$amount = $_POST['accountholder-amount'];
$name = $_POST['accountholder-name'] . ' ' . $_POST['accountholder-surname'];

$description = $_POST['payment'];

$preset_key = @$_POST['use_preset'];
$use_config = $config['DEFAULT'];
if(!empty($preset_key)) {
  $use_config = @$config['presets'][$preset_key];
  if(empty($use_config)) {
    renderError("Invalid request");
  }
  if(!empty($use_config['OPTIONS']) && empty($description))
    $description = $use_config['DESCRIPTION'];
}

if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
  renderError("Invalid email.");
}

if(empty($amount) === true || is_numeric($amount) === false) {
  renderError("Missing params.");
}

if(empty($name) === true) {
  renderError("Missing params.");
}

$amount = floatval($amount);
$_SESSION['dwolla-transfer-amount'] = $amount;

$description = $_POST['payment'];

$preset_key = @$_POST['use_preset'];
$use_config = $config['DEFAULT'];
if(!empty($preset_key)) {
  $use_config = @$config['presets'][$preset_key];
  if(empty($use_config)) {
    renderError("Invalid request");
  }
  if(!empty($use_config['OPTIONS']) && empty($description))
    $description = $use_config['DESCRIPTION'];
}
$_SESSION['dwolla-transfer-description'] = array(
  'description' => $description,
  'client_number' => !empty( $_POST['client_number'] ) ? $_POST['client_number'] : 'not specified'
);

$_SESSION['dwolla-customer-email'] = $email;

$customer = doDwolla('createCustomer', [
  'accountholder-name' => $_POST['accountholder-name'],
  'accountholder-surname' => $_POST['accountholder-surname'],
  'email' => $email,
  'ip' => getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR')
]);

if(wasError() !== false) {
  global $ERROR_HEADERS;
  global $ERROR_BODY;
  // check if the error was that the user already exists, then continue with customer iav token creation
  // the customer id will be in the error response body
  if (
    $ERROR_BODY->code == 'ValidationError' &&
    $ERROR_BODY->_embedded->errors[0]->code == 'Duplicate' &&
    $ERROR_BODY->_embedded->errors[0]->path == '/email'
  ) {
    // var_dump($ERROR_BODY->_embedded->errors[0]->_links->about->href);
    $customer = doDwolla('getCustomer', [ 'customer' => $ERROR_BODY->_embedded->errors[0]->_links->about->href]);
  } else {
    renderError();
  }
}

// TO DO: maybe send email to the client?

sendEmail(
  $use_config['PMT_RECEPIENTS'],
  sprintf($use_config['PMT_SUBJECT_ACH'],$hash),
  getAch1Email()
);

$dwollaconf = include('dwolla.conf.php');

require('process-ach.php');
