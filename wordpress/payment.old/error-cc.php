<?php
if(session_id() == '') {
  session_start();
}
$config = require_once('config.php');
?>
<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Balance credit card</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
  </head>
  <body>
    <!-- Main -->
    <div class="main">
      <h1>There was an error with your payment.</h1>
      <?php
        if ( !empty($_POST) ) {
          $response_message = '';
          $response_message = (!empty($_POST['POSTFPSMSG'])) ? $_POST['POSTFPSMSG'] : $response_message;
          $response_message = (!empty($_POST['RESPMSG'])) ? $_POST['RESPMSG'] : $response_message;
          echo '<p>' . $response_message . ' (Error Code: ' . $_POST['RESULT'] . ').' . '</p>';
        }
      ?>
      <?php
        global $NO_TRY_AGAIN;
        if(!$NO_TRY_AGAIN) {
      ?>
      <p><a onclick='window.history.back()'>Try again</a> or contact us at <a href="tel:8007777526">800.777.PLAN (7526)</a></p>
      <?php } ?>

    </div>
  </body>
</html>
