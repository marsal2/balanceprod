<?php

$USE_LIVE = false;

$conf = [
  'sandbox' => [
    'mode' => 'sandbox',
    'api_key' => '',
    'api_secret' => '',
    'token_endpoint' => 'https://sandbox.dwolla.com/oauth/v2/token',
    'api_endpoint' => 'https://api-sandbox.dwolla.com/'
  ],
  'live' => [
    'mode' => 'live',
    'api_key' => '',
    'api_secret' => '',
    'token_endpoint' => 'https://www.dwolla.com/oauth/v2/token',
    'api_endpoint' => 'https://api.dwolla.com/'
  ]
];

return $conf[$USE_LIVE === true ? 'live' : 'sandbox'];
