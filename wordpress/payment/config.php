<?php
return array(
  // This BASE_URL is used to generate links - for dev set it to your local address
  'BASE_URL' => 'https://www.balancepro.org/payment',
  'HOME_URL' => 'https://www.balancepro.org/',
  // DEFAULT is used for a reguest to cc.php and ach.php without a $_GET['preset'] variable
  'DEFAULT' => [
    'PMT_SUBJECT_ACH' => 'New ACH Request for Bankruptcy payment (ref: %s)',
    'PMT_SUBJECT_ACH_CLIENT' => 'Bankruptcy payment Transaction (id: %s)',
    'PMT_SUBJECT_CARD' => 'New Card Payment for Bankruptcy payment (id: %s)',
    'PMT_RECEPIENTS' => [
//      'BankruptcyCheckPayment@cccssf.org',
//      'jbabida@balancepro.org',
      'mitja+balance@qloud.io'
//        'primoz+balance@qloud.io'
    ],
    'DESCRIPTION' => 'Payment for Bankruptcy Counseling and Education',
  ],
  // presets are used to create diferent forms in cc.php and ach.php,
  // the $_GET['preset'] value is used to use the correct key in the
  // presets array
  'presets' => [
    'mortgage' => [
      'PMT_SUBJECT_ACH' => 'New ACH Request for Reverse Mortgage (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'Reverse Mortgage Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment for Reverse Mortgage (id: %s)',
      'PMT_RECEPIENTS' => [
//        'RMPayments@balancepro.org',
//        'rmpayments@cccssf.org',
        'mitja+balance@qloud.io'
  //      'primoz+balance.mortgage@qloud.io'
      ],
      'STRIPE_DESCRIPTION' => 'Payment for Reverse Mortgage',
      'AMOUNT' => '150.00',
      'CC' => false,
      'ACH' => true
//      'ACH' => false
    ],
    'creditreport' => [
      'PMT_SUBJECT_ACH' => 'New Credit Report request (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'ACH payment Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment (id: %s)',
      'PMT_RECEPIENTS' => [
//        'jbabida@balancepro.org'
        'mitja+balance@qloud.io'
      ],
      'DESCRIPTION' => 'Payment for Credit Reports',
      'AMOUNT' => '99.00',
//      'OPTIONS' => [[
//        'AMOUNT' => '99.00',
//        'DESCRIPTION' => 'Credit Report Review'
//        ],[
//        'AMOUNT' => '199.00',
//        'DESCRIPTION' => 'Credit Report Review (OnDemand)',
//      ]],
      'CC' => true,
      'ACH' => true
    ],
    'studentloan' => [
      'PMT_SUBJECT_ACH' => 'New Student Loan Counseling request (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'ACH payment Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment (id: %s)',
      'PMT_RECEPIENTS' => [
//        'jbabida@balancepro.org'
        'mitja+balance@qloud.io'
      ],
      'DESCRIPTION' => 'Payment for Student Loan Counseling',
      'AMOUNT' => '75.00',
      'CC' => false,
      'ACH' => true
//      'ACH' => false
    ],
    'dmppayment' => [
      'PMT_SUBJECT_ACH' => 'New ACH Request for DMP Payment (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'ACH payment Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment for DMP Payment (id: %s)',
      'PMT_RECEPIENTS' => [
//        'jbabida@balancepro.org'
        'mitja+balance@qloud.io'
      ],
      'DESCRIPTION' => 'Payment for DMP',
      'CC' => true,
      'ACH' => true,
      'ADDITIONAL_FIELDS' => [[
        'TYPE' => 'text',
        'NAME' => 'client_number',
        'CLASS' => 'field',
        'LABEL' => 'Client Number',
        'PLACEHOLDER' => 'Specify Client Number'
      ]],
    ],
    'hoepa' => [
      'PMT_SUBJECT_ACH' => 'New ACH Request for HOEPA (ref: %s)',
      'PMT_SUBJECT_ACH_CLIENT' => 'ACH payment Transaction (id: %s)',
      'PMT_SUBJECT_CARD' => 'New Card Payment for HOEPA (id: %s)',
      'PMT_RECEPIENTS' => [
//        'jbabida@balancepro.org'
        'mitja+balance@qloud.io'
      ],
      'DESCRIPTION' => 'Payment for HOEPA',
      'AMOUNT' => '200.00',
      'CC' => true,
//      'ACH' => false
      'ACH' => true
    ],
  ],
  'PMT_RECEPIENTS' => [
//    'BankruptcyCheckPayment@cccssf.org',
//    'jbabida@balancepro.org',
    'mitja+balance@qloud.io'
  ],
  'STATIC_PMT_RECEPIENTS' => [
//    'RMPayments@balancepro.org',
//    'rmpayments@cccssf.org',
    'mitja+balance@qloud.io'
  ],
  'PAYMENT_FAILURES_RECIPIENTS' => [
//    'jbabida@balancepro.org'
    'mitja+balance@qloud.io'
  ]
);

