<?php
  if(session_id() == '') {
    session_start();
  }
  require_once('security.php');
  $token = getToken();
  $config = require_once('config.php');
  $preset = false;
  $preset_key = false;
  if (isset($_GET['preset'])) {
    $preset = @$config['presets'][$_GET['preset']];
    $preset_valid = !empty( $preset['CC'] ) ? $preset['CC'] : false;
    if(empty($preset)) {
      header("HTTP/1.0 404 Not Found");
      echo "<html><body><h3>404 error</h3></body></html>";
      exit();
    } else if ( !$preset_valid ) { // preset sent is not acceptable
      header("HTTP/1.0 406 Not Acceptable");
      echo "<html><body><h3>406 error</h3></body></html>";
      exit();
    }
    $preset_key = $_GET['preset'];
  }

?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Balance credit card</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
    </head>
    <body>

      <!-- Header -->
      <header id="header">
        <div style="background-image: url(https://www.cccssf.org/bankruptcy/images/header_background.png);background-repeat: no-repeat; background-size: cover; background-position: 50% 0; max-height: 50px; letter-spacing: -4px; text-align: right; padding: 10px 5px;" class="header-t">
          <div class="container">
            <div class="row">
              <!-- search form-->
              <ul class="contact-block" style="text-transform: uppercase; letter-spacing: 0; line-height: 0; font-size: 0; display: inline-block; vertical-align: middle; letter-spacing: 1px; margin-right: 11%;">
                <li><a href="tel:8007777526"><span class="icon-phone"></span>800.777.PLAN (7526)</a></li>
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
              <div class="navbar-header" style="margin:30px 0; margin-left: 11%;">
                <a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo.svg" width="238" height="51" alt="balance"></a>
              </div>
            </div>
          </div>
        </nav>
      </header>

      <!-- Form -->
      <form action="<?=$config['BASE_URL'];?>/docc.php" method="post" id="payment-form">
        <input type="hidden" name="token" value="<?php echo $token;?>"/>
        <?php if ($preset !== false) : ?>
          <div class="group">
            <label style='width: 100%; margin: 0; text-align: center; font-weight: bold;'>
              <?php echo $preset['DESCRIPTION']; ?>
            </label>
          </div>
        <?php endif; ?>

        <div class="group">
          <label>
            <span>First Name</span>
            <input name="cardholder-name" id="cardholder-name" class="field" placeholder="Jane" type="text" required />
          </label>
        </div>

        <div class="group">
          <label>
            <span>Last Name</span>
            <input name="cardholder-surname" id="cardholder-surname" class="field" placeholder="Doe" type="text" required />
          </label>
        </div>

        <input type="hidden" name="cardholder-fullname" id="cardholder-fullname">

        <div class="group">
          <label>
            <span>Address 1</span>
            <input name="cardholder-address1" class="field" placeholder="1655 Grant Street" type="text" required />
          </label>
        </div>

        <div class="group">
          <label>
            <span>Address 2</span>
            <input name="cardholder-address2" class="field" placeholder="1655 Grant Street" type="text" />
          </label>
        </div>

        <div class="group">
          <label>
            <span>City</span>
            <input name="cardholder-city" class="field" placeholder="Concord" type="text" required />
          </label>
        </div>

        <div class="group">
          <label>
            <span>State</span>
            <input name="cardholder-state" class="field" placeholder="CA" type="text" required />
          </label>
        </div>

        <div class="group">
          <label>
            <span>Zip</span>
            <input name="cardholder-zip" class="field" placeholder="94520" type="text" required />
          </label>
        </div>

        <div class="group">
          <label>
            <span>Phone</span>
            <input name="cardholder-phone" class="field" placeholder="8775112272" type="text" required />
          </label>
        </div>

        <?php
        if ( $preset === false ) : ?>
        <div class="group" style="margin-bottom: 0; border-bottom-right-radius: 0; border-bottom-left-radius: 0;">
          <label>
            <span>Certificate</span>
            <div class="field text-left">
              <input type="checkbox" name="email-certificate" id="email-certificate" value="Yes" class="certificate">
              <label for="email-certificate" class="radio-label">Email Certificate</label>
            </div>
          </label>
        </div>

        <div class="group" style="border-top-left-radius: 0; border-top-right-radius: 0;">
          <label>
            <div class="field text-left">
              <input type="checkbox" name="fax-certificate" id="fax-certificate" class="certificate" value="Yes">
              <label for="fax-certificate" class="radio-label">Fax Certificate</label>
            </div>
          </label>
          </div>
        <?php endif; ?>

        <div class="group">
          <label>
            <span>Email</span>
            <input name="cardholder-email" class="field" placeholder="jane@gmail.com" type="email" required />
          </label>
        </div>

        <?php
        if ( $preset === false ) : ?>
        <div class="group">
          <label>
            <span>Recipient's Fax</span>
            <input name="recipient-fax" class="field" placeholder="8663636301" type="text" />
          </label>
        </div>

        <div class="group">
          <label>
            <span>Recipient's Name</span>
            <input name="recipient-name" class="field" placeholder="Jane Doe" type="text" />
          </label>
        </div>

        <div class="group">
          <label>
            <span>Recipient's Phone</span>
            <input name="recipient-phone" class="field" placeholder="8775112272" type="text" />
          </label>
        </div>

        <div class="group" style="margin-bottom: 0; border-bottom-right-radius: 0; border-bottom-left-radius: 0;">
          <label>
            <span>Payment For</span>
            <div class="field text-left">
              <input type="radio" name="payment" id="payment-1" value="Pre-Filing" class="payment" checked>
              <label for="payment-1" class="radio-label">Pre-Filing</label>
            </div>
          </label>
        </div>

        <div class="group" style="border-top-left-radius: 0; border-top-right-radius: 0;">
          <label>
            <div class="field text-left">
              <input type="radio" name="payment" id="payment-2" class="payment" value="Predischarged">
              <label for="payment-2" class="radio-label">Predischarged</label>
            </div>
          </label>
        </div>

        <div class="group">
          <label>
            <span>Bankruptcy Case #</span>
            <input name="cardholder-bankruptcy" class="field" placeholder="55555" type="number" required />
          </label>
        </div>

        <?php elseif(!empty($preset['OPTIONS'])) :
          $lbl_shown = false;
          foreach($preset['OPTIONS'] as $key=>$opt) {
        ?>
          <input type="hidden" name="use_preset" value="<?=$preset_key;?>" />
          <?php if(!$lbl_shown): ?>
          <div class="group" style="margin-bottom: 0; border-bottom-right-radius: 0; border-bottom-left-radius: 0;">
          <?php else: ?>
          <div class="group">
          <?php endif; ?>
            <label>
              <?php if(!$lbl_shown): ?>
              <span>Payment For</span>
              <?php endif; ?>
              <div class="field text-left">
                <input
                  type="radio"
                  name="payment"
                  id="payment-<?=$key?>"
                  value="<?=$opt['DESCRIPTION']?>"
                  class="payment"
                  <?php if(!$lbl_shown) echo "checked"; ?>
                  onchange="amt4opt(<?=$key?>)" />
                <label for="payment-<?=$key?>" class="radio-label">
                  <?=$opt['DESCRIPTION']?>
                </label>
              </div>
            </label>
          </div>
        <?php
          $lbl_shown = true;
        }
        else :
        ?>
          <input type="checkbox" name="payment" id="payment-1" value="<?=$preset['DESCRIPTION'];?>" class="payment" style="display:none;" checked>
          <input type="hidden" name="use_preset" value="<?=$preset_key;?>" />
        <?php endif; ?>

        <?php if($preset !== false) :
          $amount = '';
          $disabled = 'disabled readonly';
          $type = 'text';
          $step = '';
          $min = '';
          if ( !empty( $preset['OPTIONS'] ) ) { // read amount from options array (first option amount)
            $amount = $preset['OPTIONS'][0]['AMOUNT'];
          } else if ( !empty( $preset['AMOUNT'] ) && $preset['AMOUNT'] != false ) { // read amount from amount field if not set to false
            $amount = $preset['AMOUNT'];
          } else {
            $amount = '0';
            $disabled = '';
            $type = 'number';
            $step = 'step="0.01"';
            $min = 'min="0"';
          }
          ?>
          <input name="cardholder-amount"
            class="field _set_amt_ hidden"
            type="hidden"
            step="0.01"
            value="<?=$amount;?>"
            required
            readonly />
          <div class="group">
            <label>
              <span>Amount</span>
                <input name="disp-cardholder-amount"
                  class="field _set_amt_ actual"
                  type="<?=$type;?>"
                  value="<?=$amount;?>"
                  required
                  <?=$step;?>
                  <?=$min;?>
                  <?=$disabled;?> />
              </span>
            </label>
          </div>
        <?php else: ?>
          <div class="group">
            <label>
              <span>Amount</span>
                <input name="cardholder-amount"
                  class="field"
                  placeholder="0.00"
                  type="number"
                  step="0.01"
                  value=""
                  required />
              </span>
            </label>
          </div>
        <?php endif; ?>

        <?php if ( $preset !== false && !empty( $preset['ADDITIONAL_FIELDS'] ) ) : ?>

        <?php
          foreach ( $preset['ADDITIONAL_FIELDS'] as $additional_field ) {
            ?>
            <div class="group">
              <label>
                <span><?=$additional_field['LABEL'];?></span>
                <input name="additional_field[<?=$additional_field['NAME'];?>]" class="<?=$additional_field['CLASS'];?>" placeholder="<?=$additional_field['PLACEHOLDER'];?>" type="<?=$additional_field['TYPE'];?>" />
              </label>
            </div>
            <?php
          }
        ?>

        <?php endif; ?>

        <button type="submit">Proceed to Payment</button>
      </form>

      <!-- Footer -->
      <footer id="footer" style="background-image: url(https://www.cccssf.org/bankruptcy/images/footer_background.png);">
        <div class="container">
          <div class="row">
            <div class="logo-holder"><a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo-primary.svg" width="198" height="44" alt="Balance"></a></div>

          </div>
        </div>
      </footer>


    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script>
      window.amt4opt = function (pos) {
        let amts = <?=!empty($preset['OPTIONS'])
          ? json_encode($preset['OPTIONS'])
          : '[]' ?>;
        let amt = amts[pos];
        if(!amt) return;
        if (typeof amt.AMOUNT === 'undefined' || amt.AMOUNT == false) { // set field to be a text field
          $('._set_amt_.actual')
            .removeAttr('disabled')
            .removeAttr('readonly')
            .attr('type', 'number')
            .attr('step', '0.01')
            .attr('min', '0')
            .val('0');
          $('._set_amt_.hidden').val('0');
        } else {
          $('._set_amt_.actual')
            .attr('disabled', 'disabled')
            .attr('readonly', 'readonly')
            .attr('type', 'text')
            .removeAttr('step')
            .removeAttr('min')
            .val(amt.AMOUNT);
          $('._set_amt_.hidden').val(amt.AMOUNT);
        }
      }

      $('._set_amt_.actual').on('change', function() {
        $('._set_amt_.hidden').val($(this).val());
      });

      $('._set_amt_.hidden').val($('._set_amt_.actual').val());

    </script>
    </body>
</html>
