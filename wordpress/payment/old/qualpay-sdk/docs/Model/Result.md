# Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | [**\qpPlatform\Model\HttpEntity**](HttpEntity.md) |  | 
**flash** | **map[string,string]** |  | 
**session** | **map[string,string]** |  | 
**cookies** | [**\qpPlatform\Model\Cookies**](Cookies.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


