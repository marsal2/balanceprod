# qpPlatform\InvoicingApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addInvoicePayment**](InvoicingApi.md#addInvoicePayment) | **POST** /invoice/{invoice_id}/payments | Add Payment to an Invoice
[**browseBouncedInvoices**](InvoicingApi.md#browseBouncedInvoices) | **GET** /invoice/bounced | Get Undelivered Invoices
[**browseInvoicePayments**](InvoicingApi.md#browseInvoicePayments) | **GET** /invoice/payments | Get Invoice Payments
[**browseInvoicePaymentsById**](InvoicingApi.md#browseInvoicePaymentsById) | **GET** /invoice/{invoice_id}/payments | Get invoice payments by id
[**browseInvoices**](InvoicingApi.md#browseInvoices) | **GET** /invoice | Get all Invoices
[**cancelInvoice**](InvoicingApi.md#cancelInvoice) | **DELETE** /invoice/{invoice_id}/cancel | Cancel an Invoice
[**copyInvoice**](InvoicingApi.md#copyInvoice) | **POST** /invoice/{invoice_id}/copy | Copy an Invoice
[**createInvoice**](InvoicingApi.md#createInvoice) | **POST** /invoice | Create an invoice
[**getInvoice**](InvoicingApi.md#getInvoice) | **GET** /invoice/{invoice_id}/detail | Get by Invoice ID
[**removeInvoicePayment**](InvoicingApi.md#removeInvoicePayment) | **DELETE** /invoice/{invoice_id}/payments/{payment_id} | Remove an Invoice Payment
[**resendInvoice**](InvoicingApi.md#resendInvoice) | **POST** /invoice/{invoice_id}/resend | Resend an Invoice
[**sendInvoice**](InvoicingApi.md#sendInvoice) | **POST** /invoice/{invoice_id}/send | Send an Invoice
[**updateDraftInvoice**](InvoicingApi.md#updateDraftInvoice) | **PUT** /invoice/{invoice_id}/draft | Update a Draft Invoice
[**updateInvoicePayment**](InvoicingApi.md#updateInvoicePayment) | **PUT** /invoice/{invoice_id}/payments/{payment_id} | Update an Invoice Payment
[**updateOutstandingInvoice**](InvoicingApi.md#updateOutstandingInvoice) | **PUT** /invoice/{invoice_id}/outstanding | Update an Outstanding Invoice


# **addInvoicePayment**
> \qpPlatform\Model\InvoicePaymentResponse addInvoicePayment($invoice_id, $body)

Add Payment to an Invoice

Adds a payment to an invoice. A check or cash payment can be added to a saved or outstanding invoice. Credit card payments cannot be added manually to an invoice.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$body = new \qpPlatform\Model\InvoicePaymentRequest(); // \qpPlatform\Model\InvoicePaymentRequest | Invoice Payment

try {
    $result = $apiInstance->addInvoicePayment($invoice_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->addInvoicePayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **body** | [**\qpPlatform\Model\InvoicePaymentRequest**](../Model/InvoicePaymentRequest.md)| Invoice Payment |

### Return type

[**\qpPlatform\Model\InvoicePaymentResponse**](../Model/InvoicePaymentResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseBouncedInvoices**
> \qpPlatform\Model\InvoiceBouncedResponse browseBouncedInvoices($count, $order_on, $order_by, $page, $filter)

Get Undelivered Invoices

Browse all undelivered invoices. Optional query parameters determines, size and sort order of returned array.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "db_timestamp"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.

try {
    $result = $apiInstance->browseBouncedInvoices($count, $order_on, $order_by, $page, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->browseBouncedInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to db_timestamp]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]

### Return type

[**\qpPlatform\Model\InvoiceBouncedResponse**](../Model/InvoiceBouncedResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseInvoicePayments**
> \qpPlatform\Model\InvoicePaymentListResponse browseInvoicePayments($count, $order_on, $order_by, $page, $filter)

Get Invoice Payments

Browse all invoice payments. Optional query parameters determines, size and sort order of returned array.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "date_payment"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.

try {
    $result = $apiInstance->browseInvoicePayments($count, $order_on, $order_by, $page, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->browseInvoicePayments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_payment]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]

### Return type

[**\qpPlatform\Model\InvoicePaymentListResponse**](../Model/InvoicePaymentListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseInvoicePaymentsById**
> \qpPlatform\Model\InvoicePaymentListResponse browseInvoicePaymentsById($invoice_id, $count, $order_on, $order_by, $page, $filter)

Get invoice payments by id

Browse all invoice payments made to an invoice. Optional query parameters determines, size and sort order of returned array.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$count = 10; // int | The number of records in the result.
$order_on = "date_payment"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.

try {
    $result = $apiInstance->browseInvoicePaymentsById($invoice_id, $count, $order_on, $order_by, $page, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->browseInvoicePaymentsById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_payment]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]

### Return type

[**\qpPlatform\Model\InvoicePaymentListResponse**](../Model/InvoicePaymentListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseInvoices**
> \qpPlatform\Model\InvoiceListResponse browseInvoices($count, $order_on, $order_by, $page, $filter)

Get all Invoices

Gets an array of invoice objects. Optional query parameters determines, size and sort order of returned array.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "invoice_number"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.

try {
    $result = $apiInstance->browseInvoices($count, $order_on, $order_by, $page, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->browseInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to invoice_number]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]

### Return type

[**\qpPlatform\Model\InvoiceListResponse**](../Model/InvoiceListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelInvoice**
> \qpPlatform\Model\InvoiceResponse cancelInvoice($invoice_id)

Cancel an Invoice

Cancels an invoice. A canceled invoice cannot be edited. If your customer clicks on the pay now button in the invoice e-mail an error message will be displayed.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 

try {
    $result = $apiInstance->cancelInvoice($invoice_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->cancelInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **copyInvoice**
> \qpPlatform\Model\InvoiceResponse copyInvoice($invoice_id, $body)

Copy an Invoice

Makes a copy of an invoice. The invoice date will be set to today's date and due date will be adjusted based on the invoice date and the payment terms. Optionally, include an invoice_number in the POST body to make a copy of the invoice with a different invoice number. Invoice payments from the original invoice will not be copied.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$body = new \qpPlatform\Model\CopyInvoiceRequest(); // \qpPlatform\Model\CopyInvoiceRequest | Invoice

try {
    $result = $apiInstance->copyInvoice($invoice_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->copyInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **body** | [**\qpPlatform\Model\CopyInvoiceRequest**](../Model/CopyInvoiceRequest.md)| Invoice | [optional]

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createInvoice**
> \qpPlatform\Model\InvoiceResponse createInvoice($body)

Create an invoice

Creates a draft invoice that you can send later using the Send Invoice API.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \qpPlatform\Model\CreateInvoiceRequest(); // \qpPlatform\Model\CreateInvoiceRequest | Invoice

try {
    $result = $apiInstance->createInvoice($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->createInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\qpPlatform\Model\CreateInvoiceRequest**](../Model/CreateInvoiceRequest.md)| Invoice |

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInvoice**
> \qpPlatform\Model\InvoiceResponse getInvoice($invoice_id)

Get by Invoice ID

Gets an invoice by invoice_id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | Invoice ID

try {
    $result = $apiInstance->getInvoice($invoice_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->getInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**| Invoice ID |

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeInvoicePayment**
> \qpPlatform\Model\QPApiResponse removeInvoicePayment($invoice_id, $payment_id)

Remove an Invoice Payment

Removes an invoice payment. A payment can be removed on a saved or an outstanding invoice. CARD type payments cannot be removed. Payments made via a credit card cannot be removed. Payments can be deleted only from SAVED or OUTSTANDING invoices.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$payment_id = 789; // int | 

try {
    $result = $apiInstance->removeInvoicePayment($invoice_id, $payment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->removeInvoicePayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **payment_id** | **int**|  |

### Return type

[**\qpPlatform\Model\QPApiResponse**](../Model/QPApiResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resendInvoice**
> \qpPlatform\Model\QPApiResponse resendInvoice($invoice_id, $body)

Resend an Invoice

Resends an invoice to the customer. An outstanding or paid invoice can be resent.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$body = new \qpPlatform\Model\ResendInvoiceRequest(); // \qpPlatform\Model\ResendInvoiceRequest | Email Addresses

try {
    $result = $apiInstance->resendInvoice($invoice_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->resendInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **body** | [**\qpPlatform\Model\ResendInvoiceRequest**](../Model/ResendInvoiceRequest.md)| Email Addresses | [optional]

### Return type

[**\qpPlatform\Model\QPApiResponse**](../Model/QPApiResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendInvoice**
> \qpPlatform\Model\InvoiceResponse sendInvoice($invoice_id, $body)

Send an Invoice

Sends an invoice to the customer. Sending an invoice changes the status of the invoice to outstanding. Once sent, only the from_contact and business_contact of the invoice can be updated.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$body = new \qpPlatform\Model\SendInvoiceRequest(); // \qpPlatform\Model\SendInvoiceRequest | Email Addresses

try {
    $result = $apiInstance->sendInvoice($invoice_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->sendInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **body** | [**\qpPlatform\Model\SendInvoiceRequest**](../Model/SendInvoiceRequest.md)| Email Addresses | [optional]

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDraftInvoice**
> \qpPlatform\Model\InvoiceResponse updateDraftInvoice($invoice_id, $body)

Update a Draft Invoice

Updates a draft invoice. Only the fields that need updating can be sent in the request body. If updating JSON object fields, the complete JSON should be sent in the request body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$body = new \qpPlatform\Model\UpdateDraftRequest(); // \qpPlatform\Model\UpdateDraftRequest | Invoice

try {
    $result = $apiInstance->updateDraftInvoice($invoice_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->updateDraftInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **body** | [**\qpPlatform\Model\UpdateDraftRequest**](../Model/UpdateDraftRequest.md)| Invoice |

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateInvoicePayment**
> \qpPlatform\Model\InvoicePaymentResponse updateInvoicePayment($invoice_id, $payment_id, $body)

Update an Invoice Payment

Updates an invoice payment. A payment can be updated on a saved or an outstanding invoice. Payments made via credit card using the “Pay Now” button cannot be updated.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$payment_id = 789; // int | 
$body = new \qpPlatform\Model\InvoicePaymentRequest(); // \qpPlatform\Model\InvoicePaymentRequest | Invoice Payment

try {
    $result = $apiInstance->updateInvoicePayment($invoice_id, $payment_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->updateInvoicePayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **payment_id** | **int**|  |
 **body** | [**\qpPlatform\Model\InvoicePaymentRequest**](../Model/InvoicePaymentRequest.md)| Invoice Payment |

### Return type

[**\qpPlatform\Model\InvoicePaymentResponse**](../Model/InvoicePaymentResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOutstandingInvoice**
> \qpPlatform\Model\InvoiceResponse updateOutstandingInvoice($invoice_id, $body)

Update an Outstanding Invoice

Updates an outstanding invoice. Only the from_contact and business_contact fields can be updated on an outstanding invoice. Only the fields that need updating can be sent in the request body. If updating JSON object fields, the complete JSON should be sent in the request body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\InvoicingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoice_id = 789; // int | 
$body = new \qpPlatform\Model\UpdateOutstandingRequest(); // \qpPlatform\Model\UpdateOutstandingRequest | Invoice

try {
    $result = $apiInstance->updateOutstandingInvoice($invoice_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoicingApi->updateOutstandingInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **int**|  |
 **body** | [**\qpPlatform\Model\UpdateOutstandingRequest**](../Model/UpdateOutstandingRequest.md)| Invoice |

### Return type

[**\qpPlatform\Model\InvoiceResponse**](../Model/InvoiceResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

