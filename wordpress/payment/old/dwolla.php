<?php
if(session_id() == '') {
  session_start();
}

require_once('email.php');
require('dwolla-sdk/autoload.php');

$WAS_ERROR = false;
$WAS_ERROR_ADMIN = false;
$SHOW_ERROR = '';
$ERROR_BODY = array();
$ERROR_HEADERS = array();

function getAccessToken() {
  $dwollaAccessToken = null;
  if ( empty( $_SESSION['dwolla-access-token'] ) ) {
    $dwollaAccessToken = require('dwolla-auth-token.php');
    $_SESSION['dwolla-access-token'] = json_encode($dwollaAccessToken);
  } else {
    $dwollaAccessToken = json_decode($_SESSION['dwolla-access-token']);
  }
  return $dwollaAccessToken;
}

function getApiClient() {
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $dwollaconf = include('dwolla.conf.php');
  $apiClient = new DwollaSwagger\ApiClient($dwollaconf['api_endpoint']);
  return $apiClient;
}

function getCustomersApi() {
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $apiClient = getApiClient();
  $customersApi = new DwollaSwagger\CustomersApi($apiClient);
  return $customersApi;
}

function createCustomer($params) {
  if ( empty( $params['accountholder-name'] ) ) {
    setError( null, $use_message = 'Customer Name is not specified (Error Code #1).');
    return;
  }
  if ( empty( $params['accountholder-surname'] ) ) {
    setError( null, $use_message = 'Customer Surname is not specified (Error Code #2).');
    return;
  }
  if ( empty( $params['email'] ) ) {
    setError( null, $use_message = 'Email is not specified (Error Code #3).');
    return;
  }
  if ( empty( $params['ip'] ) ) {
    setError( null, $use_message = 'Customer IP Address is not specified (Error Code #4).');
    return;
  }
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $customersApi = getCustomersApi();
  $new_customer = $customersApi->create([
    'firstName' => $params['accountholder-name'],
    'lastName' => $params['accountholder-surname'],
    'email' => $params['email'],
    'ipAddress' => $params['ip'],
    'address1' => $params['accountholder-address1'],
    'address2' => $params['accountholder-address2'],
    'city' => $params['accountholder-city'],
    'state' => $params['accountholder-state'],
    'postalCode' => $params['accountholder-zip'],
    'phone' => $params['accountholder-phone']
  ]);
  $iavToken = createCustomerIavToken( array( 'customer' => $new_customer ) );
  return array( 'customer' => $new_customer, 'iav_token' => $iavToken);
}

function createCustomerIavToken($params) {
  if ( empty( $params['customer'] ) ) {
    setError( null, $use_message = 'Customer ID is not specified (Error Code #5).');
    return;
  }
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $customersApi = getCustomersApi();
  $fsToken = $customersApi->getCustomerIavToken($params['customer']);
  $iavToken = $fsToken->token; # => "lr0Ax1zwIpeXXt8sJDiVXjPbwEeGO6QKFWBIaKvnFG0Sm2j7vL"
  return $iavToken;
}

function getCustomer($params) {
  if ( empty( $params['customer'] ) ) {
    setError( null, $use_message = 'Customer ID is not specified (Error Code #6).');
    return;
  }
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $customersApi = getCustomersApi();
  $customer = $customersApi->getCustomer($params['customer']);
  $iavToken = createCustomerIavToken( array( 'customer' => $customer->_links['self']->href ) );
  return array( 'customer' => $customer->_links['self']->href, 'iav_token' => $iavToken);
}

function getBalanceAccount() {
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $apiClient = getApiClient();
  $rootApi = new DwollaSwagger\RootApi($apiClient);
  $root = $rootApi->root();
  return $root;
}

function getBalanceAccountID() {
  $balanceAccountID = null;
  if ( empty( $_SESSION['dwolla-account-id'] ) ) {
    $balanceAccount = getBalanceAccount();
    $balanceAccountID = $balanceAccount->_links['account']->href;
    $_SESSION['dwolla-account-id'] = $balanceAccountID;
  } else {
    $balanceAccountID = $_SESSION['dwolla-account-id'];
  }
  return $balanceAccountID;
}

function createTransfer($params) {
  if ( empty( $params['funding-source'] ) ) {
    setError( null, $use_message = 'Transfer Funding Source is not specified (Error Code #7).');
    return;
  }
  if ( empty( $params['amount'] ) ) {
    setError( null, $use_message = 'Transfer Amount is not specified (Error Code #8).');
    return;
  }
  if ( empty( $_SESSION['dwolla-transfer-description'] ) ) {
    $params['metadata'] = array(
      'description' => 'not specified',
      'client_number' => 'not specified'
    );
  } else {
    $params['metadata'] = $_SESSION['dwolla-transfer-description'];
  }

  $accessToken = getAccessToken();
  //var_dump($accessToken);
  DwollaSwagger\Configuration::$access_token = $accessToken;
  $apiClient = getApiClient();
  //var_dump($apiClient);
  $transferData = array (
    '_links' =>  array (
      'source' => array (
        'href' => $params['funding-source'],
      ),
      'destination' => array (
        'href' => getBalanceAccountID(),
      ),
    ),
    'amount' => array (
      'currency' => 'USD',
      'value' => $params['amount'],
    ),
    'metadata' => $params['metadata']
  );
  //var_dump($transferData);
  $transferApi = new DwollaSwagger\TransfersApi($apiClient);
  //var_dump($transferApi);
  $transfer = $transferApi->create($transferData);
  return $transfer;
}

function getTransferById($params) {
  if ( empty( $params['transfer-id'] ) ) {
    setError( null, $use_message = 'Transfer ID is not specified (Error Code #9).');
    return;
  }
  DwollaSwagger\Configuration::$access_token = getAccessToken();
  $apiClient = getApiClient();
  $transfersApi = new DwollaSwagger\TransfersApi($apiClient);
  $transfer = $transfersApi->byId($params['transfer-id']);
  return $transfer;
}

// Using this method to call others
// as same err handling would otherwise
// need to be done
function doDwolla( $method, $params ) {
  global $WAS_ERROR;
  global $WAS_ERROR_ADMIN;
  try {
    return call_user_func($method, $params);
  } catch ( Exception $e ) {
    setError($e);
  }
}

function wasError() {
  global $WAS_ERROR;
  return $WAS_ERROR;
}

function setError( $e, $use_message = false ) {
  global $WAS_ERROR;
  global $WAS_ERROR_ADMIN;
  global $ERROR_BODY;
  global $ERROR_HEADERS;

  if ( empty( $e ) ) {
    $e = new Exception();
  }

  $trace = explode("\n", $e->getTraceAsString());
  // reverse array to make steps line up chronologically
  $trace = array_reverse($trace);
  array_shift($trace); // remove {main}
  array_pop($trace); // remove call to this method
  $length = count($trace);
  $result = array();
  
  for ($i = 0; $i < $length; $i++) {
    $result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
  }
  
  $trace_output = "<br>Stack Trace:<br>" . implode("<br>", $result);
  
  if(empty($use_message) === false) {
    $WAS_ERROR = $use_message;
    $WAS_ERROR_ADMIN = $use_message . $trace_output;
    return;
  }

  $headers = $e->getResponseHeaders();
  $body = $e->getResponseBody();

  $ERROR_HEADERS = $headers;
  $ERROR_BODY = json_decode($body);
  $WAS_ERROR = 'Error Description: 500';
  $WAS_ERROR_ADMIN = "$headers $body $trace_output";

  $config = include('config.php');

  sendEmail(
    $config['PAYMENT_FAILURES_RECIPIENTS'],
    'There was an error with the payment process (Dwolla)',
    getAchFailureEmail()
  );
}

function renderError( $use_message = false ) {
  global $SHOW_ERROR;
  global $ERROR_BODY;
  global $ERROR_HEADERS;

  $SHOW_ERROR = wasError();
  if(empty($use_message) === false) {
    $SHOW_ERROR = $use_message;
  }
  include('error.php');
  exit();
}

function getAchFailureEmail() {
  global $WAS_ERROR_ADMIN;
  ob_start();
  require_once('email.error.admin.php');
  return ob_get_clean();
}
